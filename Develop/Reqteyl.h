//==========================================================================
// レクティル[Reqteyl.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Reqteyl_H_
#define _Reqteyl_H_

//==========================================================================
//
// class  : CReqteyl
// Content: レクティル
//
//==========================================================================
class CReqteyl : private CTemplate
{
public:
	CReqteyl();
	~CReqteyl();
	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static void Update(void);
	// 描画
	static void Draw(bool * Key);
private:
	static C2DPolygon *m_ReqteylData; // レクティルの画像
	static C2DObject *m_ReqteylPos; // レクティルの座標
};

#endif // !_Reqteyl_H_
