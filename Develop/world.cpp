//==========================================================================
// ワールド[world.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "world.h"

//==========================================================================
// 実体 
//==========================================================================
CCamera *CWorld::m_WorldCamera;

CWorld::CWorld()
{
}

CWorld::~CWorld()
{
}

bool CWorld::Init(void)
{
	D3DXVECTOR3 Eye = D3DXVECTOR3(20.0f, 8.0f, -45.0f); // 注視点
	D3DXVECTOR3 At = D3DXVECTOR3(0.0f, 0.0f, 0.0f); // カメラ座標 

	// メモリ確保
	New(m_WorldCamera);

	// ワールドカメラ初期化
	m_WorldCamera->Init();
	m_WorldCamera->Init(&Eye, &At);

	return false;
}

void CWorld::Uninit(void)
{
	// カメラ解放
	m_WorldCamera->Uninit();

	// メモリ解放
	Delete(m_WorldCamera);
}

void CWorld::Update(void)
{
}

void CWorld::Draw(void)
{
}
