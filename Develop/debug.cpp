//==========================================================================
// デバッグ[debug.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "GunIncludeData.h"
#include "SceneChange.h"
#include "debug.h"
#include "player.h"
#include "main.h"

#if defined(_DEBUG) || defined(DEBUG)
//==========================================================================
// 実体
//==========================================================================
CSceneManager::SceneName CDebug::m_Name;
int CDebug::m_FontPosY;
//==========================================================================
// マクロ定義
//==========================================================================
#define _DEBUGNAME_(a,...) CDirectXDebug::PosAndColor(1, m_FontPosY);CDirectXDebug::DPrintf((a),__VA_ARGS__);
#define _DEBUG_Printf_(a,...) m_FontPosY += CDirectXDebug::GetFontSize();CDirectXDebug::PosAndColor(1, m_FontPosY);CDirectXDebug::DPrintf((a),__VA_ARGS__);
#endif

CDebug::CDebug()
{
}

CDebug::~CDebug()
{
}

// シーンセット
void CDebug::Set(CSceneManager::SceneName Input)
{
#if defined(_DEBUG) || defined(DEBUG)
	m_Name = Input;
#else
	Input;
#endif
}

// 描画
void CDebug::Draw(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	m_FontPosY = 3;
	switch (m_Name)
	{
	case CSceneManager::SceneName::TITLE:
		Title();
		break;
	case CSceneManager::SceneName::HOME:
		Home();
		break;
	case CSceneManager::SceneName::GAME:
		Game();
		break;
	case CSceneManager::SceneName::RESULT:
		Result();
		break;
	case CSceneManager::SceneName::SCREEN_SAVER:
		Screen_Saver();
		break;
	case CSceneManager::SceneName::PRACTICE:
		Practice();
		break;
	case CSceneManager::SceneName::LOAD:
		Loat();
		break;
	default:
		break;
	}
#endif
}

void CDebug::Title(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	_DEBUGNAME_("Title");
#endif
}

void CDebug::Home(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	_DEBUGNAME_("Home");
#endif
}

void CDebug::Game(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	_DEBUGNAME_("Game");
	_DEBUG_Printf_("");
	_DEBUG_Printf_("カメラ");
	_DEBUG_Printf_("TPS視点 >> X=%.2f,Y=%.2f,Z=%.2f", CPlayer::TPSCamera()->GetVECTOR(CCamera::VectorList::VAT).x, CPlayer::TPSCamera()->GetVECTOR(CCamera::VectorList::VAT).y, CPlayer::TPSCamera()->GetVECTOR(CCamera::VectorList::VAT).z);
	_DEBUG_Printf_("TPS座標 >> X=%.2f,Y=%.2f,Z=%.2f", CPlayer::TPSCamera()->GetVECTOR(CCamera::VectorList::VEYE).x, CPlayer::TPSCamera()->GetVECTOR(CCamera::VectorList::VEYE).y, CPlayer::TPSCamera()->GetVECTOR(CCamera::VectorList::VEYE).z);
	_DEBUG_Printf_("FPS視点 >> X=%.2f,Y=%.2f,Z=%.2f", CPlayer::FPSCamera()->GetVECTOR(CCamera::VectorList::VAT).x, CPlayer::FPSCamera()->GetVECTOR(CCamera::VectorList::VAT).y, CPlayer::FPSCamera()->GetVECTOR(CCamera::VectorList::VAT).z);
	_DEBUG_Printf_("FPS座標 >> X=%.2f,Y=%.2f,Z=%.2f", CPlayer::FPSCamera()->GetVECTOR(CCamera::VectorList::VEYE).x, CPlayer::FPSCamera()->GetVECTOR(CCamera::VectorList::VEYE).y, CPlayer::FPSCamera()->GetVECTOR(CCamera::VectorList::VEYE).z);
	_DEBUG_Printf_("");
	_DEBUG_Printf_("マウス");
	_DEBUG_Printf_("座標 >> X=%d,Y=%d", CMouse::WIN32Cursor().x, CMouse::WIN32Cursor().y);
	_DEBUG_Printf_("速度 >> X=%d,Y=%d,Z=%d", CMouse::Speed().m_lX, CMouse::Speed().m_lY, CMouse::Speed().m_lZ);
	_DEBUG_Printf_("クリック >> 左=%s, 右=%s", BoolFont(CMouse::Trigger(CMouse::ButtonKey::Left)), BoolFont(CMouse::Trigger(CMouse::ButtonKey::Right)));
	_DEBUG_Printf_("");
	_DEBUG_Printf_("プレイヤー");
	_DEBUG_Printf_("座標 >> X=%.2f,Y=%.2f,Z=%.2f", CPlayer::GetPos()->Info.pos.x, CPlayer::GetPos()->Info.pos.y, CPlayer::GetPos()->Info.pos.z);
	_DEBUG_Printf_("視点 >> X=%.2f,Y=%.2f,Z=%.2f", CPlayer::GetPos()->Look.Eye.x, CPlayer::GetPos()->Look.Eye.y, CPlayer::GetPos()->Look.Eye.z);

#endif
}

void CDebug::Result(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	_DEBUGNAME_("Result");
#endif
}

void CDebug::Screen_Saver(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	_DEBUGNAME_("Screen_Saver");
#endif
}

void CDebug::Practice(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	_DEBUGNAME_("Practice");
#endif
}

void CDebug::Loat(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	_DEBUGNAME_("Loat");
#endif
}
