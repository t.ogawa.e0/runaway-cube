//==========================================================================
// ランクリスト[ContentsResult.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "GunIncludeData.h"
#include "ContentsResult.h"
#include "Time.h"
#include "WillPower.h"
#include "enemy.h"

//==========================================================================
// 実体
//==========================================================================
bool CContentsResult::m_EndKey; // 終了キー
int CContentsResult::m_DrawSerect; // 描画セレクト
C2DObject * CContentsResult::m_Pos; // 座標
C2DPolygon * CContentsResult::m_poly; // ポリゴン
int CContentsResult::m_NumData; // データ数
CSound * CContentsResult::m_Sound; // サウンド

CContentsResult::CContentsResult()
{
}

CContentsResult::~CContentsResult()
{
}

//==========================================================================
// 初期化
bool CContentsResult::Init(void)
{
	const char * pTexLink[] =
	{
		"resource/texture/ランク.png",
		"resource/texture/C.png",
		"resource/texture/B.png",
		"resource/texture/A.png",
		"resource/texture/S.png",
	};

	CSound::SoundLabel pSundList[] =
	{
		{ "resource/sound/RPG_SE[232].wav",0,1.0f }
	};

	m_NumData = Sizeof(pTexLink);

	New(m_Pos, m_NumData);
	New(m_poly);
	New(m_Sound);

	for (int i = 0; i < m_NumData; i++)
	{
		m_Pos[i].Init(i);
		m_Pos[i].SetCentralCoordinatesMood(true);
	}

	if (m_poly->Init(pTexLink, m_NumData))
	{
		return true;
	}

	m_DrawSerect = 0;
	m_EndKey = false;

	m_Pos[0].SetX((float)(CDirectXDevice::GetWindowsSize().m_Width *0.4f));
	m_Pos[0].SetY((float)(CDirectXDevice::GetWindowsSize().m_Height *0.5f));

	m_Pos[1].SetX((m_Pos[0].GetPos()->x + m_poly->GetTexPalam(0)->Widht / 2) + 150);
	m_Pos[1].SetY(m_Pos[0].GetPos()->y + 30);
	m_Pos[1].SetColor(0, 255, 150, 255);
	m_Pos[1].Scale(0.5f);

	m_Pos[2].SetX(m_Pos[1].GetPos()->x);
	m_Pos[2].SetY(m_Pos[1].GetPos()->y);
	m_Pos[2].SetColor(42, 102, 255, 255);
	m_Pos[2].Scale(0.5f);

	m_Pos[3].SetX(m_Pos[1].GetPos()->x);
	m_Pos[3].SetY(m_Pos[1].GetPos()->y);
	m_Pos[3].SetColor(255, 43, 103, 255);
	m_Pos[3].Scale(0.5f);

	m_Pos[4].SetX(m_Pos[1].GetPos()->x);
	m_Pos[4].SetY(m_Pos[1].GetPos()->y);
	m_Pos[4].SetColor(251, 236, 0, 255);
	m_Pos[4].Scale(0.5f);

	if (m_Sound->Init(pSundList, Sizeof(pSundList)))
	{
		return true;
	}

	return false;
}

//==========================================================================
// 解放
void CContentsResult::Uninit(void)
{
	m_poly->Uninit();
	m_Sound->Uninit();

	Delete(m_Pos);
	Delete(m_poly);
	Delete(m_Sound);
}

//==========================================================================
// 更新
bool CContentsResult::Update(void)
{
	int nScorCount = 0;

	// エネミーが全滅した場合
	if (CEnemy::GetDesEnemyCount() == CEnemy::LimitNumEnemy())
	{
		nScorCount++;
	}

	// エネミーが半分以上倒されていた場合
	if ((CEnemy::LimitNumEnemy() / 2)<CEnemy::GetDesEnemyCount())
	{
		nScorCount++;
	}

	// 気力が半分以上あるとき
	if (50.0f<CWillPower::GetResultPercentage())
	{
		nScorCount++;
	}

	// 時間切れではない場合と気力が0でなければ
	if (CTime::GetResultTime() != 0 && CWillPower::GetResultPercentage() != 0)
	{
		nScorCount++;
	}

	if (nScorCount == 0)
	{
		nScorCount++;
	}

	m_DrawSerect = nScorCount;

	if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_RETURN))
	{
		m_Sound->Play(0);
		m_EndKey = true;
	}

	if (CMouse::Trigger(CMouse::ButtonKey::Left) || CMouse::Trigger(CMouse::ButtonKey::Right))
	{
		m_Sound->Play(0);
		m_EndKey = true;
	}

	if (CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton1)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton2)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton3)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton4)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::L1Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::R1Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::L2Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::R2Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::SHAREButton)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::SOPTIONSButton)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::L3Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::R3Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::PSButton)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::TouchPad)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUp)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUpRight)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonRight)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUnderRight)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUnder)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUnderLeft)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonLeft)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUpLeft)
		)
	{
		m_Sound->Play(0);
		m_EndKey = true;
	}

	return m_EndKey;
}

//==========================================================================
// 描画
void CContentsResult::Draw(void)
{
	m_poly->Draw(&m_Pos[0]);
	m_poly->Draw(&m_Pos[m_DrawSerect]);
}
