//==========================================================================
// 銃[gun.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "GunIncludeData.h"

CGun::CGun()
{
}

CGun::~CGun()
{
}

//==========================================================================
// 初期化
bool CGun::Init(const char ** InputGun)
{
	// アドレス初期化
	this->m_ModelData = nullptr;
	this->m_Barrett = nullptr;

	// メモリ確保
	this->New(this->m_ModelData);

	// 描画フラグ
	this->m_Draw = false;

	// モデルデータ格納
	return this->m_ModelData->Init(InputGun, sizeof(**InputGun));
}

//==========================================================================
// 弾の初期化
bool CGun::InitBarrett(const char ** pFile, int size)
{
	// 銃がないとき
	if (this->m_ModelData == nullptr)
	{
		ErrorMessage(nullptr, "銃が存在しないため、弾をセットできませんでした");
		return true;
	}

	// メモリ確保
	this->New(this->m_Barrett);

	// モデル格納
	return this->m_Barrett->Init(pFile, size);
}

//==========================================================================
// 使用するエフェクトの初期化
bool CGun::InitEffect(const char * InputExplosion, int UpdateFrame, int Pattern, int Direction)
{
	// 弾が込められていない
	if (this->m_Barrett == nullptr)
	{
		ErrorMessage(nullptr, "弾がセットされていないため、エフェクトを作れませんでした");
		return true;
	}

	// エフェクト格納
	return this->m_Barrett->SetEffect(InputExplosion, UpdateFrame, Pattern, Direction);
}

//==========================================================================
// 解放
void CGun::Uninit(void)
{
	// モデルの解放
	if (this->m_ModelData != nullptr)
	{
		this->m_ModelData->Uninit();
		this->Delete(this->m_ModelData);
	}

	// 弾の解放
	if (this->m_Barrett != nullptr)
	{
		this->m_Barrett->Uninit();
		this->Delete(this->m_Barrett);
	}
}

//==========================================================================
// 更新
void CGun::Update(void)
{
	if (this->m_Barrett != nullptr)
	{
		this->m_Barrett->Update();
	}
}

//==========================================================================
// 描画
void CGun::Draw(D3DXMATRIX *MtxView)
{
	if (this->m_ModelData != nullptr)
	{
		if (this->m_Draw == true)
		{
			this->m_ModelData->Draw(this->m_Pos);
		}
	}

	if (this->m_Barrett != nullptr)
	{
		this->m_Barrett->Draw(MtxView);
	}
}

//==========================================================================
// 銃の座標セット
void CGun::SetPos(C3DObject * Input)
{
	this->m_Pos = Input;
}

//==========================================================================
// 銃のトリガー
void CGun::Trigger(bool Input, const float *Spped)
{
	if (this->m_Barrett != nullptr)
	{
		if (Input == true)
		{
			this->m_Barrett->Create(this->m_Pos, Spped);
		}
	}
}
