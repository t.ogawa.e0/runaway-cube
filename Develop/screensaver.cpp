//==========================================================================
// スクリーンセーバー[screensaver.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "screensaver.h"

CScreenSaverScene::CScreenSaverScene()
{
}

CScreenSaverScene::~CScreenSaverScene()
{
}

//==========================================================================
// 初期化
bool CScreenSaverScene::Init(void)
{
	CLight light;

	light.Init({ 1,0,0 });

	return false;
}

//==========================================================================
// 解放
void CScreenSaverScene::Uninit(void)
{
}

//==========================================================================
// 更新
void CScreenSaverScene::Update(void)
{
}

//==========================================================================
// 描画
void CScreenSaverScene::Draw(void)
{
}
