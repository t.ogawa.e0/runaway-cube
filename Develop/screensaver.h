//==========================================================================
// スクリーンセーバー[screensaver.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _SCREEN_SAVER_H_
#define _SCREEN_SAVER_H_

#include"SceneChange.h"

//==========================================================================
//
// class  : CScreenSaverScene
// Content: スクリーンセーバーシーン
//
//==========================================================================
class CScreenSaverScene : public CBaseScene
{
public:
	CScreenSaverScene();
	~CScreenSaverScene();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
private:
};

#endif // !_SCREEN_SAVER_H_
