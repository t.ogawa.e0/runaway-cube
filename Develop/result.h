//==========================================================================
// リザルト[result.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _RESULT_H_
#define _RESULT_H_

#include"SceneChange.h"

//==========================================================================
//
// class  : CResultScene
// Content: リザルトシーン
//
//==========================================================================
class CResultScene : public CBaseScene
{
public:
	CResultScene();
	~CResultScene();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
private:
	CSound m_Sound;
};

#endif // !_RESULT_H_
