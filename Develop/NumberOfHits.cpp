//==========================================================================
// íe[NumberOfHits.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "NumberOfHits.h"

//==========================================================================
// ÀÌ
//==========================================================================
int CNumberOfHits::m_DrawCount; // `æI¹ÜÅ
int CNumberOfHits::m_HitsCount; // íe
C2DObject * CNumberOfHits::m_Pos; // ÀW
C2DPolygon * CNumberOfHits::m_Poly; // |S
CNumber * CNumberOfHits::m_Number; // 

CNumberOfHits::CNumberOfHits()
{
}

CNumberOfHits::~CNumberOfHits()
{
}

//==========================================================================
// ú»
bool CNumberOfHits::Init(void)
{
	const char * pTexData[] =
	{
		"resource/texture/íetHg.png",
		"resource/texture/Number.png",
	};
	C2DObject * pPos;

	m_DrawCount = m_DrawLimit;
	m_HitsCount = 0;
	New(m_Pos, Sizeof(pTexData));
	New(m_Poly);
	New(m_Number);

	if (m_Poly->Init(pTexData, Sizeof(pTexData))) { return true; }

	pPos = &m_Pos[0];
	pPos->Init(0);
	pPos->SetCentralCoordinatesMood(true);
	pPos->Scale(-0.85f);
	pPos->SetX((float)CDirectXDevice::GetWindowsSize().m_Width*0.05f);
	pPos->SetY((float)CDirectXDevice::GetWindowsSize().m_Height / 2);

	pPos = &m_Pos[1];
	pPos->Init(1, 1, 10, 10);
	pPos->SetCentralCoordinatesMood(true);
	pPos->SetPos(m_Pos[0].GetPos()->x, m_Pos[0].GetPos()->y);
	pPos->SetXPlus(90);
	m_Poly->GetTexPalam(pPos->m_ID)->Widht = (int)(m_Poly->GetTexPalam(pPos->m_ID)->Widht*0.33f);
	m_Poly->GetTexPalam(pPos->m_ID)->Height = (int)(m_Poly->GetTexPalam(pPos->m_ID)->Height*0.33f);

	m_Number->Init(5, true, false);

	return false;
}

//==========================================================================
// ðú
void CNumberOfHits::Uninit(void)
{
	m_Poly->Uninit();
	m_Number->Uninit();

	Delete(m_Pos);
	Delete(m_Poly);
	Delete(m_Number);
}

//==========================================================================
// XV
void CNumberOfHits::Update(void)
{
	m_DrawCount++;
	m_Number->Update();
}

//==========================================================================
// `æ
void CNumberOfHits::Draw(void)
{
	if (m_DrawCount <= m_DrawLimit)
	{
		m_Poly->Draw(&m_Pos[0]);
		m_Number->Draw(m_Poly, &m_Pos[1], m_HitsCount);
	}
}

//==========================================================================
// íecount
void CNumberOfHits::HitCount(void)
{
	m_DrawCount = 0;
	m_HitsCount++;
}
