//==========================================================================
// バレット[barrett.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _BARRETT_H_
#define _BARRETT_H_

//==========================================================================
//
// class  : CBarrett
// Content: バレット
//
//==========================================================================
class CBarrett : private CTemplate
{
private:
	// 弾のデータ管理
	class CData
	{
	public:
		int m_ID; // 管理ID
		float m_Speed; // 速度
		C3DObject m_Pos; // 座標
		C3DObject m_ObjPos; // オブジェクト座標
		C3DObject m_OldPos; // 古い座標
		CData *m_pNext; // 次のアドレス
		CData *m_pBack; // 前のアドレス
	};
public:
	CBarrett();
	~CBarrett();
	// 初期化
	bool Init(const char ** pFile, int size);
	// 使用するエフェクトの初期化
	bool SetEffect(const char * InputExplosion, int UpdateFrame, int Pattern, int Direction);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(D3DXMATRIX *MtxView);
	// 弾生成
	void Create(const C3DObject *Input, const float *Spped);
	// 特定のデータ消去
	void PinpointDelete(const int * ID);
	// 弾データのゲッター データがない場合null
	CBarrett::CData *GetData(const int *Count);
private:
	CXmodel *m_pBarrett = nullptr; // 弾
	CData *m_pStartAddress = nullptr; // 先頭アドレス
	CEffect *m_EffectData = nullptr; // エフェクトデータ
	int m_NumData; // データ数
	int m_IDCount; // IDカウンタ
};

#endif // !_BARRETT_H_
