//==========================================================================
// エンター押そう処理[PressEnter.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _PressEnter_H_
#define _PressEnter_H_

//==========================================================================
//
// class  : CPressEnter
// Content: エンター
//
//==========================================================================
class CPressEnter : private CTemplate
{
public:
	CPressEnter();
	~CPressEnter();
	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static void Update(void);
	// 描画
	static void Draw(void);
private:
	static C2DObject * m_Pos; // 座標
	static C2DPolygon * m_Poly; // ポリゴン
	static int * m_αCountLimit; // α値の制限
	static int * m_αCount; // カウント
	static bool * m_αChange; // チェンジ
};
#endif // !_PressEnter_H_
