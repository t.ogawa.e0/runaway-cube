//==========================================================================
// スクリーン[Screen.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "main.h"
#include "Screen.h"
#include "LoadScreen.h"
#include "UserCamera.h"
#include "debug.h"
#include "Fade.h"

#include "ThankYouForPlaying.h"
#include "EndGame.h"
#include "OperationExplanation.h"
#include "TitleName.h"

//==========================================================================
// 実体
//==========================================================================
bool CScreen::m_Change; // 切り替えフラグ
int CScreen::m_Count; // カウンタ
bool CScreen::m_Start;
bool CScreen::m_DebugKey;

CScreen::CScreen()
{
}

CScreen::~CScreen()
{
}

//==========================================================================
// 初期化
bool CScreen::Init(void)
{
	srand((unsigned)time(nullptr));
	if (CDirectXDevice::CreateDevice(&CMain::m_hWnd)) { return true; }
	if (CKeyboard::Init(&CMain::m_hInstance, &CMain::m_hWnd)) { return true; }
	if (CMouse::Init(&CMain::m_hInstance, &CMain::m_hWnd)) { return true; }
	if (CSoundDevice::Init(&CMain::m_hWnd)) { return true; }
	if (CController::Init(&CMain::m_hInstance, &CMain::m_hWnd)) { return true; }
#if defined(_DEBUG) || defined(DEBUG)
	CDirectXDebug::Init(CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height);
#endif
	m_Change = false;
	m_Count = 0;
	m_Start = false;
	m_DebugKey = false;
	if (CFade::Init()) { return true; }

	return CLoadScreen::Init();
}

//==========================================================================
// 解放
void CScreen::Uninit(void)
{
	CSceneManager::Uninit();
	CFade::Uninit();
	CLoadScreen::Uninit();
	CDirectXDevice::Uninit();
	CKeyboard::Uninit();
	CMouse::Uninit();
	CController::Uninit();
	CSoundDevice::Uninit();
#if defined(_DEBUG) || defined(DEBUG)
	CDirectXDebug::Uninit();
#endif
}

//==========================================================================
// 更新処理
bool CScreen::Update(void)
{
	if (StartInit()) { return true; }
	CKeyboard::Update();
	CMouse::Update();
	CController::Update();
	CFade::Update();
	Fade();

	// ロードが終わっているとき
	if (m_Change == true)
	{
		CSceneManager::Update();
		CUserCamera::Update();
	}
	else
	{
		CLoadScreen::Update();
	}

	return false;
}

//==========================================================================
// 描画処理
void CScreen::Draw(void)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();	//デバイス渡し	

	pDevice->Clear(0, nullptr, (D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER), D3DCOLOR_RGBA(50, 50, 50, 255), 1.0f, 0);

	//Direct3Dによる描画の開始
	if (SUCCEEDED(pDevice->BeginScene()))
	{
		// 画面切り替えが終わっているとき
		if (m_Change == true)
		{
			CSceneManager::Draw();
			CDebug::Draw();
			CFade::Draw();
		}
		else
		{
			CLoadScreen::Draw();
		}
		pDevice->EndScene();//Direct3Dによる描画の終了
	}

	pDevice->Present(nullptr, nullptr, nullptr, nullptr);//第3　対象のウィンドウハンドル　hWnd
}

//==========================================================================
// スクリーンの切り替え
bool CScreen::Change(CSceneManager::SceneName Name)
{
	// 各シーンのカメラ切り替え
	CUserCamera::Set(Name);

	// デバッグの切り替え
	CDebug::Set(Name);

	// すべての初期化
	return CSceneManager::ChangeScene(Name);
}

//==========================================================================
// 初期化start
bool CScreen::StartInit(void)
{
	//==========================================================================
	// デバッグ用
#if defined(_DEBUG) || defined(DEBUG)
	if (m_Count == 50 && m_Change == false && m_Start == true&& m_DebugKey == true)
	{
		if (Change(CSceneManager::SceneName::RESULT)) { return true; }
		m_DebugKey = false;
	}
#endif
	//==========================================================================
	// 最初の画面
	if (m_Count == 50 && m_Change == false && m_Start == false)
	{
		m_Start = true;
		if (Change(CSceneManager::SceneName::TITLE)) { return true; }
	}

	//==========================================================================
	// タイトル画面終了時
	if (m_Count == 50 && m_Change == false && CTitleName::GetKey())
	{
		if (*CTitleName::GetKey())
		{
			if (Change(CSceneManager::SceneName::PRACTICE)) { return true; }
		}
	}

	//==========================================================================
	// 操作説明画面終了時
	if (m_Count == 50 && m_Change == false && COperationExplanation::GetKey())
	{
		if (*COperationExplanation::GetKey())
		{
			if (Change(CSceneManager::SceneName::GAME)) { return true; }
		}
	}

	//==========================================================================
	// ゲーム終了時
	if (m_Count == 50 && m_Change == false && CEndGame::GetEndKey())
	{
		if (*CEndGame::GetEndKey())
		{
			if (Change(CSceneManager::SceneName::RESULT)) { return true; }
		}
	}

	//==========================================================================
	// リザルト終了時
	if (m_Count == 50 && m_Change == false && CThankYouForPlaying::GetKey())
	{
		if (*CThankYouForPlaying::GetKey())
		{
			if (Change(CSceneManager::SceneName::TITLE)) { return true; }
		}
	}

	//==========================================================================
	// ここから下カウンタ
	if (m_Count == 100 && m_Change == false)
	{
		CFade::Out();
		m_Change = true;
	}

	if (m_Change == false)
	{
		m_Count++;
	}

	return false;
}

//==========================================================================
// フェード
void CScreen::Fade(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	// フェード処理がされていないとき
	if (CFade::GetDraw() == false)
	{
		// 制作用
		if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_1))
		{
			// フェード処理開始フラグ
			m_DebugKey = true;
			CFade::In();
		}
	}
#endif

	// フェードが終わりスクリーンがロード画面ではない場合
	if (CFade::FeadInEnd() && m_Change == true)
	{
		m_Change = false;
		m_Count = 0;
	}
}
