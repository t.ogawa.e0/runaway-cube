//==========================================================================
// オブジェクト[3DObject.cpp]
// author: tatuya ogawa
//==========================================================================
#include "3DObject.h"

C3DObject::C3DObject()
{
}

C3DObject::~C3DObject()
{
}

//==========================================================================
// 初期化
void C3DObject::Init(void)
{
	this->ID = 0;

	this->Info.pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	this->Info.rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	this->Info.sca = D3DXVECTOR3(1.0f, 1.0f, 1.0f);

	this->Look.Eye = D3DXVECTOR3(0.0f, 0.0f, -2.0f); // 視点
	this->Look.At = D3DXVECTOR3(0.0f, 0.0f, 0.0f); // 座標
	this->Look.Up = D3DXVECTOR3(0.0f, 1.0f, 0.0f); // ベクター

	this->Vec.Up = D3DXVECTOR3(0, 1, 0); // 上ベクトル
	this->Vec.Front = D3DXVECTOR3(0, 0, 1); // 前ベクトル
	this->Vec.Right = D3DXVECTOR3(1, 0, 1); //  右ベクトル

	this->Collar = nullptr;

	// ベクトルの正規化
	D3DXVec3Normalize(&this->Vec.Front, &this->Vec.Front);
	D3DXVec3Normalize(&this->Vec.Up, &this->Vec.Up);
	D3DXVec3Normalize(&this->Vec.Right, &this->Vec.Right);
}

//==========================================================================
// X軸回転
void C3DObject::RotX(float Rot)
{
	D3DXMATRIX RotX; //X回転行列
	D3DXVECTOR3 Direction;

	D3DXMatrixRotationY(&RotX, Rot); // 回転
	D3DXVec3Cross(&this->Vec.Right, &this->Vec.Up, &this->Vec.Front); // 外積
	Direction = (this->Look.Eye) - (this->Look.At); // 向きベクトル	
	D3DXVec3TransformNormal(&Direction, &Direction, &RotX);
	D3DXVec3TransformNormal(&this->Vec.Front, &this->Vec.Front, &RotX);
	D3DXVec3TransformNormal(&this->Vec.Right, &this->Vec.Right, &RotX);
	this->Look.Eye = (this->Look.At) + (Direction);
}

//==========================================================================
// Y軸回転
void C3DObject::RotY(float Rot)
{
	D3DXMATRIX RotY; //X回転行列
	D3DXVECTOR3 Direction;

	if (this->Restriction(RotY, &Rot))
	{
		D3DXVec3Cross(&this->Vec.Right, &this->Vec.Up, &this->Vec.Front); // 外積
		D3DXMatrixRotationAxis(&RotY, &this->Vec.Right, Rot); // 回転
		Direction = (this->Look.Eye) - (this->Look.At); // 向きベクトル
		D3DXVec3TransformNormal(&Direction, &Direction, &RotY);
		D3DXVec3TransformNormal(&this->Vec.Front, &this->Vec.Front, &RotY);
		D3DXVec3TransformNormal(&this->Vec.Right, &this->Vec.Right, &RotY);
		this->Look.Eye = (this->Look.At) + (Direction);
	}
}

//==========================================================================
// 平行移動
void C3DObject::MoveZ(float Speed)
{
	D3DXVec3Normalize(&this->Vec.Front, &this->Vec.Front);
	this->Info.pos += this->Vec.Front*Speed;
}

//==========================================================================
// 平行移動
void C3DObject::MoveX(float Speed)
{
	D3DXVec3Normalize(&this->Vec.Right, &this->Vec.Right);
	this->Info.pos += this->Vec.Right*Speed;
}

//==========================================================================
// 平行移動
void C3DObject::MoveY(float Speed)
{
	D3DXVec3Normalize(&this->Vec.Up, &this->Vec.Up);
	this->Info.pos += this->Vec.Up*Speed;
}

//==========================================================================
// スケール
void C3DObject::Scale(float Scale)
{
	this->Info.sca += D3DXVECTOR3(Scale, Scale, Scale);
}

//==========================================================================
// ベクトルを使った行列
D3DXMATRIX * C3DObject::CreateMtxWorld1(D3DXMATRIX * pOut)
{
	D3DXMATRIX aMtxRot;
	D3DXMATRIX aMtxTra;
	D3DXMATRIX aMtxSca;

	// 平行
	D3DXMatrixTranslation(&aMtxTra, this->Info.pos.x, this->Info.pos.y, this->Info.pos.z);

	// スケール
	D3DXMatrixScaling(&aMtxSca, this->Info.sca.x, this->Info.sca.y, this->Info.sca.z);

	// 回転
	this->CalcLookAtMatrix(&aMtxRot);

	D3DXMatrixIdentity(pOut);

	// 平行の合成
	D3DXMatrixMultiply(pOut, &aMtxTra, pOut);

	// スケールの合成
	D3DXMatrixMultiply(pOut, &aMtxSca, pOut);

	// 回転の合成
	D3DXMatrixMultiply(pOut, &aMtxRot, pOut);

	return pOut;
}

//==========================================================================
// 直接値を入れる行列
D3DXMATRIX * C3DObject::CreateMtxWorld2(D3DXMATRIX * pOut)
{
	D3DXMATRIX aMtxRot;
	D3DXMATRIX aMtxTra;
	D3DXMATRIX aMtxSca;

	// 平行
	D3DXMatrixTranslation(&aMtxTra, this->Info.pos.x, this->Info.pos.y, this->Info.pos.z);

	// スケール
	D3DXMatrixScaling(&aMtxSca, this->Info.sca.x, this->Info.sca.y, this->Info.sca.z);

	// 回転
	D3DXMatrixRotationYawPitchRoll(&aMtxRot, this->Info.rot.y, this->Info.rot.x, this->Info.rot.z);

	D3DXMatrixIdentity(pOut);

	// 平行の合成
	D3DXMatrixMultiply(pOut, &aMtxTra, pOut);

	// スケールの合成
	D3DXMatrixMultiply(pOut, &aMtxSca, pOut);

	// 回転の合成
	D3DXMatrixMultiply(pOut, &aMtxRot, pOut);

	return pOut;
}

//==========================================================================
// ラジコン回転
void C3DObject::RadioControl(D3DXVECTOR3 vecRight, float Spped)
{
	D3DXVECTOR3 vecCross;

	D3DXVec3Cross(&vecCross, &this->Vec.Front, &vecRight);
	this->RotX((vecCross.y < 0.0f ? -0.01f : 0.01f)*Spped);
}

//==========================================================================
// 前ベクトル
void C3DObject::SetVecFront(D3DXVECTOR3 Input)
{
	this->Vec.Front = Input;
}

//==========================================================================
// 上ベクトル
void C3DObject::SetVecUp(D3DXVECTOR3 Input)
{
	this->Vec.Up = Input;
}

//==========================================================================
//  右ベクトル
void C3DObject::SetVecRight(D3DXVECTOR3 Input)
{
	this->Vec.Right = Input;
}

//==========================================================================
// 回転の制限
bool C3DObject::Restriction(D3DXMATRIX pRot, const float * pRang)
{
	D3DXVECTOR3 dir = D3DXVECTOR3(0, 1.0f, 0); // 単位ベクトル
	float Limit = 0.75f;
	D3DXVECTOR3 pVecRight, pVecUp, pVecFront;

	pVecRight = this->Vec.Right;
	pVecUp = this->Vec.Up;
	pVecFront = this->Vec.Front;

	// ベクトルの座標変換
	D3DXVec3Cross(&pVecRight, &pVecUp, &pVecFront); // 外積
	D3DXMatrixRotationAxis(&pRot, &pVecRight, *pRang); // 回転
	D3DXVec3TransformNormal(&pVecFront, &pVecFront, &pRot);
	float fVec3Dot = atanf(D3DXVec3Dot(&pVecFront, &dir));

	// 内積
	if (-Limit<fVec3Dot && Limit>fVec3Dot) { return true; }

	return false;
}

//==========================================================================
// 回転行列
D3DXMATRIX* C3DObject::CalcLookAtMatrix(D3DXMATRIX* pOut)
{
	D3DXVECTOR3 X, Y, Z;

	Z = this->Look.Eye - this->Look.At;
	D3DXVec3Normalize(&Z, &Z);
	D3DXVec3Cross(&X, D3DXVec3Normalize(&Y, &this->Look.Up), &Z);
	D3DXVec3Normalize(&X, &X);
	D3DXVec3Normalize(&Y, D3DXVec3Cross(&Y, &Z, &X));

	pOut->_11 = X.x; pOut->_12 = X.y; pOut->_13 = X.z; pOut->_14 = 0;
	pOut->_21 = Y.x; pOut->_22 = Y.y; pOut->_23 = Y.z; pOut->_24 = 0;
	pOut->_31 = Z.x; pOut->_32 = Z.y; pOut->_33 = Z.z; pOut->_34 = 0;
	pOut->_41 = 0.0f; pOut->_42 = 0.0f; pOut->_43 = 0.0f; pOut->_44 = 1.0f;

	return pOut;
}

//==========================================================================
// 回転行列
D3DXMATRIX* C3DObject::CalcLookAtMatrixAxisFix(D3DXMATRIX* pOut)
{
	D3DXVECTOR3 X, Y, Z, D;
	D = this->Look.Eye - this->Look.At;
	D3DXVec3Normalize(&D, &D);
	D3DXVec3Cross(&X, D3DXVec3Normalize(&Y, &this->Look.Up), &D);
	D3DXVec3Normalize(&X, &X);
	D3DXVec3Normalize(&Z, D3DXVec3Cross(&Z, &X, &Y));

	pOut->_11 = X.x; pOut->_12 = X.y; pOut->_13 = X.z; pOut->_14 = 0;
	pOut->_21 = Y.x; pOut->_22 = Y.y; pOut->_23 = Y.z; pOut->_24 = 0;
	pOut->_31 = Z.x; pOut->_32 = Z.y; pOut->_33 = Z.z; pOut->_34 = 0;
	pOut->_41 = 0.0f; pOut->_42 = 0.0f; pOut->_43 = 0.0f; pOut->_44 = 1.0f;

	return pOut;
}

void C3DObject::CCollar::Set(int _r, int _g, int _b, int _α)
{
	this->r = _r;
	this->g = _g;
	this->b = _b;
	this->α = _α;
}
