//==========================================================================
// プレイヤー[player.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "GunIncludeData.h"
#include "player.h"
#include "SceneChange.h"
#include "UserCamera.h"

//==========================================================================
// 実体
//==========================================================================
CPlayer::CPlayerData *CPlayer::m_Player; // プレイヤーのデータ
CXmodel *CPlayer::m_ModelData; // モデルデータ
CShadow *CPlayer::m_Shadow; // 影
CGun *CPlayer::m_MosinData; // 銃のデータ
int * CPlayer::m_FrameSkip;

CPlayer::CPlayer()
{
}

CPlayer::~CPlayer()
{
}

//==========================================================================
// 初期化
bool CPlayer::Init(void)
{
	const char* LinkData[] =
	{
		"resource/3DModel/Player.x"
	};
	const char* MosinLinkData[] =
	{
		"resource/3DModel/MosinNagantM1891-30SniperTHEEND.x"
	};
	const char* BarrettLinkData[] =
	{
		"resource/3DModel/BarrettEfectPlayer.x"
	};

	CSound::SoundLabel pSundList[] =
	{
		{ "resource/sound/RPG_SE[146].wav",0,1.0f },
		{ "resource/sound/RPG_SE[232].wav",0,1.0f }
	};

	// メモリ確保
	New(m_Player);
	New(m_ModelData);
	New(m_Shadow);
	New(m_MosinData);
	New(m_FrameSkip);

	(*m_FrameSkip) = 0;

	// カメラアドレス初期化
	m_Player->m_CurrentCamera = nullptr;

	// 各座標初期化
	m_Player->m_Pos.Init();
	m_Player->m_OldPos.Init();
	m_Player->m_Body.Init();
	m_Player->m_Head.Init();
	m_Player->m_FPSPos.Init();
	m_Player->m_MosinPos.Init();
	m_Player->m_MosinRot.Init();

	// ビューカメラ設定
	m_Player->m_ViewCameraKey = false;

	// カメラ初期化
	SetCamera();
	ChangeCanera();

	// プレイヤーの座標変更
	m_Player->m_Pos.MoveZ(-35);
	m_Player->m_Body.MoveY(2);
	m_Player->m_Head.MoveY(3.9f);

	// 各モデルデータ格納
	if (m_MosinData->Init(MosinLinkData)) { return true; }
	if (m_MosinData->InitBarrett(BarrettLinkData, Sizeof(BarrettLinkData))) { return true; }
	if (m_MosinData->InitEffect("resource/texture/explosionA-color_10F_xp.png", 2, 10, 5)) { return true; }
	if (m_Shadow->Init("resource/texture/shadow000.jpg")) { return true; }

	// 描画判定
	m_MosinData->DrawJudgment(true);
	if (m_Player->m_Sound.Init(pSundList, Sizeof(pSundList)))
	{
		return true;
	}

	// モデル確保
	return m_ModelData->Init(LinkData, Sizeof(LinkData));
}

//==========================================================================
// 解放
void CPlayer::Uninit(void)
{
	// データ解放
	m_ModelData->Uninit();
	m_MosinData->Uninit();
	m_Shadow->Uninit();
	m_Player->m_Sound.Uninit();

	// メモリ解放
	Delete(m_Player);
	Delete(m_ModelData);
	Delete(m_MosinData);
	Delete(m_Shadow);
	Delete(m_FrameSkip);
}

//==========================================================================
// 更新
void CPlayer::Update(void)
{
	(*m_FrameSkip)++;

	if (10 <= (*m_FrameSkip))
	{
		Move();
		Attack();
		PlayerCamera();
		MosinUpdate();
		(*m_FrameSkip) = 10;
	}
}

//==========================================================================
// 描画
void CPlayer::Draw(void)
{
	if (10 <= (*m_FrameSkip))
	{
		m_ModelData->Draw(&m_Player->m_Pos);
		m_MosinData->Draw(CUserCamera::GetView());
		m_Shadow->Draw(&m_Player->m_Pos);
	}
}

//==========================================================================
// プレイヤーの動く処理
void CPlayer::Move(void)
{
	// FPSモードの座標セット
	m_Player->m_FPSPos.RotX((float)CController::CPS4::RightStick().m_LeftRight*0.0001f);

	m_Player->m_FPSPos.RotX((float)CMouse::Speed().m_lX*m_ViewRollingPower);

	// TPSモード
	if (m_Player->m_ViewCameraKey == false)
	{
		// キーボード
		if (CKeyboard::Press(CKeyboard::KeyList::KEY_W))
		{
			m_Player->m_Pos.MoveZ(m_MoveSpeed);
		}
		if (CKeyboard::Press(CKeyboard::KeyList::KEY_A))
		{
			m_Player->m_Pos.RotX(-m_RollingPower);
		}
		if (CKeyboard::Press(CKeyboard::KeyList::KEY_S))
		{
			m_Player->m_Pos.MoveZ(-m_MoveSpeed);
		}
		if (CKeyboard::Press(CKeyboard::KeyList::KEY_D))
		{
			m_Player->m_Pos.RotX(m_RollingPower);
		}

		// PS4
		m_Player->m_Pos.MoveZ(-(float)CController::CPS4::LeftStick().m_UpUnder*0.0005f);
		m_Player->m_Pos.RotX((float)CController::CPS4::LeftStick().m_LeftRight*0.0001f);
	}
	// FPSモード
	else if (m_Player->m_ViewCameraKey == true)
	{
		// キーボード
		if (CKeyboard::Press(CKeyboard::KeyList::KEY_W))
		{
			m_Player->m_Pos.MoveZ(m_MoveSpeed);
		}
		if (CKeyboard::Press(CKeyboard::KeyList::KEY_A))
		{
			m_Player->m_Pos.MoveX(-m_MoveSpeed);
		}
		if (CKeyboard::Press(CKeyboard::KeyList::KEY_S))
		{
			m_Player->m_Pos.MoveZ(-m_MoveSpeed);
		}
		if (CKeyboard::Press(CKeyboard::KeyList::KEY_D))
		{
			m_Player->m_Pos.MoveX(m_MoveSpeed);
		}

		// PS4
		m_Player->m_Pos.MoveZ(-(float)CController::CPS4::LeftStick().m_UpUnder*0.0005f);
		m_Player->m_Pos.MoveX((float)CController::CPS4::LeftStick().m_LeftRight*0.0005f);

		// ベクトル関係を移動
		m_Player->m_Pos.Vec.Front = m_Player->m_FPSPos.Vec.Front;
		m_Player->m_Pos.Vec.Right = m_Player->m_FPSPos.Vec.Right;
		m_Player->m_Pos.Look.Eye = m_Player->m_FPSPos.Look.Eye;
		m_Player->m_Pos.Look.At = m_Player->m_FPSPos.Look.At;
	}

	m_Player->m_Body.Info.pos.x = m_Player->m_Pos.Info.pos.x;
	m_Player->m_Body.Info.pos.z = m_Player->m_Pos.Info.pos.z;
	m_Player->m_Head.Info.pos.x = m_Player->m_Pos.Info.pos.x;
	m_Player->m_Head.Info.pos.z = m_Player->m_Pos.Info.pos.z;
}

//==========================================================================
// プレイヤーのカメラ
void CPlayer::SetCamera(void)
{
	C3DObject Eye = m_Player->m_Pos;
	C3DObject At = m_Player->m_Pos;

	// 各カメラの初期化
	m_Player->m_TPSCamera.Init();
	m_Player->m_FPSCamera.Init();

	{// TPSカメラ
		Eye.Info.pos.y += 4;
		At = Eye;
		Eye.MoveZ(-5);
		m_Player->m_TPSCamera.Init(&Eye.Info.pos, &At.Info.pos);
	}

	Eye = m_Player->m_Pos;
	At = m_Player->m_Pos;

	{// FPSカメラ
		Eye.Info.pos.y += 4;
		At = Eye;
		At.MoveZ(2);
		m_Player->m_FPSCamera.Init(&Eye.Info.pos, &At.Info.pos);
	}
}

//==========================================================================
// カメラの動く処理
void CPlayer::MoveCamera(void)
{
	// 各カメラ情報に座標データをセット
	m_Player->m_TPSCamera.SetEye(&m_Player->m_Pos.Info.pos);
	m_Player->m_TPSCamera.SetAt(&m_Player->m_Pos.Info.pos);
	m_Player->m_FPSCamera.SetEye(&m_Player->m_Pos.Info.pos);
	m_Player->m_FPSCamera.SetAt(&m_Player->m_Pos.Info.pos);
}

//==========================================================================
// ビューカメラ
void CPlayer::ViewCamera(void)
{
	// PS4
	// TPSカメラ処理
	m_Player->m_TPSCamera.RotViewX((float)CController::CPS4::RightStick().m_LeftRight*0.0001f);
	m_Player->m_TPSCamera.RotViewY((float)CController::CPS4::RightStick().m_UpUnder*0.0001f);

	// マウス
	// TPSカメラ処理
	m_Player->m_TPSCamera.RotViewX((float)CMouse::Speed().m_lX*m_ViewRollingPower);
	m_Player->m_TPSCamera.RotViewY((float)CMouse::Speed().m_lY*m_ViewRollingPower);
}

//==========================================================================
// 視点カメラ
void CPlayer::LookCamera(void)
{
	// PS4
	// FPSカメラ処理
	m_Player->m_FPSCamera.RotCameraX((float)CController::CPS4::RightStick().m_LeftRight*0.0001f);
	m_Player->m_FPSCamera.RotCameraY((float)CController::CPS4::RightStick().m_UpUnder*0.0001f);

	// マウス
	// FPSカメラ処理
	m_Player->m_FPSCamera.RotCameraX((float)CMouse::Speed().m_lX*m_ViewRollingPower);
	m_Player->m_FPSCamera.RotCameraY((float)CMouse::Speed().m_lY*m_ViewRollingPower);
}

//==========================================================================
// プレイヤーのカメラ
void CPlayer::PlayerCamera(void)
{
	// PS4
	// カメラ切り替え
	if (CMouse::Trigger(CMouse::ButtonKey::Right))
	{
		// bool切り替え
		Bool(&m_Player->m_ViewCameraKey);
		ChangeCanera();
		m_Player->m_Sound.Play(1);
	}

	// マウス
	// カメラ切り替え
	if (CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::R1Button))
	{
		// bool切り替え
		Bool(&m_Player->m_ViewCameraKey);
		ChangeCanera();
		m_Player->m_Sound.Play(1);
	}


	// 各カメラ処理
	MoveCamera();
	ViewCamera();
	LookCamera();
}

//==========================================================================
// 攻撃
void CPlayer::Attack(void)
{
	// PS4
	// プレイヤーの攻撃処理
	if (CMouse::Press(CMouse::ButtonKey::Left))
	{
		m_MosinData->Trigger(CMouse::Press(CMouse::ButtonKey::Left), &m_MosinBarrettSpped);
		m_Player->m_Sound.Play(0);
	}

	// マウス
	// プレイヤーの攻撃処理
	if (CController::CPS4::CButton::Press(CController::CPS4::CButton::Ckey::R2Button))
	{
		m_MosinData->Trigger(CController::CPS4::CButton::Press(CController::CPS4::CButton::Ckey::R2Button), &m_MosinBarrettSpped);
		m_Player->m_Sound.Play(0);
	}
}

//==========================================================================
// カメラ切り替え
void CPlayer::ChangeCanera(void)
{
	// TPSセット
	if (m_Player->m_ViewCameraKey == false)
	{
		m_Player->m_CurrentCamera = &m_Player->m_TPSCamera;
	}
	// FPSセット
	else if (m_Player->m_ViewCameraKey == true)
	{
		m_Player->m_CurrentCamera = &m_Player->m_FPSCamera;
	}
}

void CPlayer::MosinUpdate(void)
{
	// PS4
	// FPS銃の回転設定
	m_Player->m_MosinRot.RotX((float)CController::CPS4::RightStick().m_LeftRight*0.0001f);
	m_Player->m_MosinRot.RotY((float)CController::CPS4::RightStick().m_UpUnder*0.0001f);

	// マウス
	// FPS銃の回転設定
	m_Player->m_MosinRot.RotY((float)CMouse::Speed().m_lY*m_ViewRollingPower);
	m_Player->m_MosinRot.RotX((float)CMouse::Speed().m_lX*m_ViewRollingPower);

	// TPS銃の処理
	if(m_Player->m_ViewCameraKey == false)
	{
		m_Player->m_MosinPos = m_Player->m_Pos;
		m_Player->m_MosinPos.Info.pos.y += 3.1f;
		m_Player->m_MosinPos.Info.pos += m_Player->m_MosinPos.Vec.Right * 0.8f;
		m_Player->m_MosinPos.MoveZ(2.f);
		m_Player->m_MosinPos.Scale(2);
		m_MosinData->DrawJudgment(true);
	}
	// FPS銃の処理
	else if (m_Player->m_ViewCameraKey == true)
	{
		m_Player->m_MosinPos.Init();
		m_Player->m_MosinPos.Info.pos = m_Player->m_FPSCamera.GetVECTOR(CCamera::VectorList::VAT);
		m_Player->m_MosinPos.Vec.Front = m_Player->m_MosinRot.Vec.Front;
		m_Player->m_MosinPos.Vec.Right = m_Player->m_MosinRot.Vec.Right;
		m_Player->m_MosinPos.Look.Eye = m_Player->m_MosinRot.Look.Eye;
		m_Player->m_MosinPos.Look.At = m_Player->m_MosinRot.Look.At;
		m_Player->m_MosinPos.Scale(2);
		m_MosinData->DrawJudgment(false);
	}

	// 銃データに座標をセット
	m_MosinData->SetPos(&m_Player->m_MosinPos);

	// 銃の更新
	m_MosinData->Update();
}
