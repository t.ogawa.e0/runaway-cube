//==========================================================================
// jσ¦[DestructionRate.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _DestructionRate_H_
#define _DestructionRate_H_

//==========================================================================
//
// class  : CDestructionRate
// Content: jσ¦
//
//==========================================================================
class CDestructionRate : private CTemplate
{
private:
	static constexpr int m_DrawLimit = 60; // `ζIΉframe
	static constexpr float m_LimitPercentage = 100; // %γΐ
public:
	CDestructionRate();
	~CDestructionRate();
	// ϊ»
	static bool Init(int Numenemy);
	// πϊ
	static void Uninit(void);
	// XV
	static void Update(void);
	// `ζ
	static void Draw(void);
	// %JE^
	static void Count(void);
private:
	static int m_DrawCount; // `ζIΉάΕ
	static float m_1Percentage; // 1%
	static float m_NowPercentage; // ‘Μ%
	static C2DObject * m_Pos; // ΐW
	static C2DPolygon * m_Poly; // |S
	static CNumber * m_Number; // 
};

#endif // !_DestructionRate_H_
