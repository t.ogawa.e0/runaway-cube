//==========================================================================
// ワールド[world.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _WORLD_H_
#define _WORLD_H_

//==========================================================================
//
// class  : CWorld
// Content: ワールド
//
//==========================================================================
class CWorld : private CTemplate
{
public:
	CWorld();
	~CWorld();
	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static void Update(void);
	// 描画
	static void Draw(void);
	// ビューカメラのゲッター
	static D3DXMATRIX *GetCamera(void) { return m_WorldCamera->CreateView(); }
private:
	static CCamera *m_WorldCamera; // ワールドビューカメラ
};

#endif // !_WORLD_H_
