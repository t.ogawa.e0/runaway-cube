//==========================================================================
// DirectXライブラリ[DirectXLibrary.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _DirectXLibrary_H_
#define _DirectXLibrary_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// 独自lib include
//==========================================================================
#include "Lib\input.h"
#include "Lib\Mouse.h"
#include "Lib\Controller.h"
#include "Lib\Template.h"
#include "Lib\DXDevice.h"
#include "Lib\Sound.h"
#include "Lib\DXDebug.h"
#include "Lib\Object.h"
#include "Lib\ExcelOpen.h"
#include "Lib\Rectangle.h"
#include "Lib\SetRender.h"
#include "Lib\HitDetermination.h"
#include "Lib\Vertex3D.h"
#include "Lib\2DObject.h"
#include "Lib\3DObject.h"
#include "Lib\2DPolygon.h"
#include "Lib\Billboard.h"
#include "Lib\Camera.h"
#include "Lib\Cube.h"
#include "Lib\Grid.h"
#include "Lib\Light.h"
#include "Lib\Meshfield.h"
#include "Lib\Shadow.h"
#include "Lib\Xmodel.h"
#include "Lib\Number.h"
#include "Lib\Timer.h"

//==========================================================================
// マクロ定義
//==========================================================================
#ifndef ErrorMessage
#define ErrorMessage(p,c) MessageBox((p), (c), "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING)
#endif // !ErrorMessage

#endif // !_DirectXLibrary_H_
