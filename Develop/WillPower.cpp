//==========================================================================
// 気力[WillPower.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "WillPower.h"

//==========================================================================
// 実体
//==========================================================================
float CWillPower::m_1Percentage; // 1%
float CWillPower::m_NowPercentage; // 今の%
bool CWillPower::m_0Percentage;
C2DObject * CWillPower::m_Pos; // 座標
C2DPolygon * CWillPower::m_Poly; // ポリゴン
CNumber * CWillPower::m_Number; // 数字

CWillPower::CWillPower()
{
}

CWillPower::~CWillPower()
{
}

//==========================================================================
// 初期化
bool CWillPower::Init(int nα)
{
	C2DObject * pPos = nullptr;
	const char * pTexData[] =
	{
		"resource/texture/気力フォント.png",
		"resource/texture/Number.png",
		"resource/texture/%フォント.png",
	};

	m_1Percentage = (float)(m_LimitPercentage / nα);
	m_NowPercentage = 100.0f;
	m_0Percentage = false;
	New(m_Pos, Sizeof(pTexData));
	New(m_Poly);
	New(m_Number);

	if (m_Poly->Init(pTexData, Sizeof(pTexData))) { return true; }
	pPos = &m_Pos[0];
	pPos->Init(0);
	pPos->Scale(-0.7f);
	pPos->SetY((float)CDirectXDevice::GetWindowsSize().m_Height *0.2f);

	pPos = &m_Pos[1];
	pPos->Init(1, 1, 10, 10);
	pPos->Scale(-0.7f);
	pPos->SetY((float)CDirectXDevice::GetWindowsSize().m_Height *0.21f);
	pPos->SetX((float)m_Poly->GetTexPalam(m_Pos[0].m_ID)->Widht);

	pPos = &m_Pos[2];
	pPos->Init(2);
	pPos->Scale(-0.7f);
	pPos->SetY((float)CDirectXDevice::GetWindowsSize().m_Height *0.21f);

	m_Number->Init(3, true, false);

	return false;
}

//==========================================================================
// 解放
void CWillPower::Uninit(void)
{
	m_Poly->Uninit();
	m_Number->Uninit();

	Delete(m_Pos);
	Delete(m_Poly);
	Delete(m_Number);
}

//==========================================================================
// 更新
void CWillPower::Update(int nα)
{
	m_NowPercentage = m_LimitPercentage - (m_1Percentage*nα);
	if (m_NowPercentage <= 0)
	{
		m_NowPercentage = 0.0f;
		m_0Percentage = true;
	}
}

//==========================================================================
// 描画
void CWillPower::Draw(void)
{
	float PosX = 0.0f;

	m_Poly->Draw(&m_Pos[0]);
	PosX = m_Number->Draw(m_Poly, &m_Pos[1], (int)(m_NowPercentage + 0.5f));

	m_Pos[2].SetX(PosX);
	m_Poly->Draw(&m_Pos[2]);
}
