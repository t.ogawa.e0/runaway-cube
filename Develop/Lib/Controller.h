//==========================================================================
// コントローラー[Controller.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Controller_H_
#define _Controller_H_

//==========================================================================
// Include
//==========================================================================
#include <Windows.h>
#define DIRECTINPUT_VERSION (0x0800) // DirectInputのバージョン指定
#include<dinput.h>

//==========================================================================
//
// class  : CController
// Content: コントローラー
//
//==========================================================================
class CController
{
public:
	//==========================================================================
	//
	// class  : CPS4
	// Content: PS4コントローラー
	//
	//==========================================================================
	class CPS4
	{
	private:
		class CStick
		{
		public:
			LONG m_LeftRight;
			LONG m_UpUnder;
		};
	public:
		// 方向キー
		enum class CDirecyionkey
		{
			LeftButtonUp = 1,
			LeftButtonUpRight,
			LeftButtonRight,
			LeftButtonUnderRight,
			LeftButtonUnder,
			LeftButtonUnderLeft,
			LeftButtonLeft,
			LeftButtonUpLeft,
		};
	public:
		// 左スティック
		static CStick LeftStick(void);

		// 右スティック
		static CStick RightStick(void);

		// L2
		static LONG L2(void);

		// R2
		static LONG R2(void);

		// PS4 方向キー Num=Max3
		static bool DirectionKey(CDirecyionkey Key);

		// 方向キー 
		class CDirection
		{
		public:
			enum class Ckey
			{
				LeftButtonUp,
				LeftButtonUpRight,
				LeftButtonRight,
				LeftButtonUnderRight,
				LeftButtonUnder,
				LeftButtonUnderLeft,
				LeftButtonLeft,
				LeftButtonUpLeft,
			};
		public:
			// プレス
			static bool Press(Ckey key);

			// トリガー
			static bool Trigger(Ckey key);

			// リピート
			static bool Repeat(Ckey key);

			// リリ−ス
			static bool Release(Ckey key);
		};

		// ボタン
		class CButton
		{
		public:
			// 有効ボタン
			enum class Ckey
			{
				RightButton1,	// □ シカク
				RightButton2,	// Ｘ バツ
				RightButton3,	// 〇 マル
				RightButton4,	// △ サンカク
				L1Button,		// L1
				R1Button,		// R1
				L2Button,		// L2
				R2Button,		// R2
				SHAREButton,	// SHARE
				SOPTIONSButton,	// OPTIONS
				L3Button,		// L3
				R3Button,		// R3
				PSButton,		// PS
				TouchPad,		// TouchPad
			};
		public:
			// プレス
			static bool Press(Ckey key);

			// トリガー
			static bool Trigger(Ckey key);

			// リピート
			static bool Repeat(Ckey key);

			// リリ−ス
			static bool Release(Ckey key);
		};
	private:
		static LONG Adjustment(LONG Set);
		static CStick Stick(LONG Stick1, LONG Stick2);
	};
public:
	CController();
	~CController();
	static bool Init(HINSTANCE * hInstance, HWND * hWnd);
	static bool Update(void);
	static void Uninit(void);
private:
	static BOOL CALLBACK EnumJoysticksCallback(const DIDEVICEINSTANCE *pdidInstance, VOID *pContext);
	static BOOL CALLBACK EnumAxesCallback(const DIDEVICEOBJECTINSTANCE *pdidoi, VOID *pContext);
private:
	static constexpr int m_LimitCounter = 20;
private:
	static LPDIRECTINPUT8 m_pDInput; // DirectInputオブジェクト
	static LPDIRECTINPUTDEVICE8 m_pDIDevice; // 入力デバイス(コントローラー)へのポインタ
	static DIJOYSTATE m_State; // 入力情報ワーク
	static BYTE m_StateTrigger[(int)sizeof(DIJOYSTATE::rgbButtons)]; // トリガー情報ワーク
	static BYTE m_StateRelease[(int)sizeof(DIJOYSTATE::rgbButtons)]; // リリース情報ワーク
	static BYTE m_StateRepeat[(int)sizeof(DIJOYSTATE::rgbButtons)]; // リピート情報ワーク
	static int m_StateRepeatCnt[(int)sizeof(DIJOYSTATE::rgbButtons)]; // リピートカウンタ

	static BYTE m_POVState[8]; // POV入力情報ワーク
	static BYTE m_POVTrigger[8]; // トリガー情報ワーク
	static BYTE m_POVRelease[8]; // リリース情報ワーク
	static BYTE m_POVRepeat[8]; // リピート情報ワーク
	static int m_POVRepeatCnt[8]; // リピートカウンタ
};

#endif // !_Controller_H_
