//==========================================================================
// オブジェクト[3DObject.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _3DObject_H_
#define _3DObject_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
//
// class  : C3DObject
// Content: オブジェクト
//
//==========================================================================
class C3DObject
{
private:
	class CMatLook // 視覚情報
	{
	public:
		D3DXVECTOR3 Eye; // 視点
		D3DXVECTOR3 At; // 座標
		D3DXVECTOR3 Up; // ベクター
	};

	class CVec3 // ベクトル
	{
	public:
		D3DXVECTOR3 Up; // 上ベクトル
		D3DXVECTOR3 Front; // 前ベクトル
		D3DXVECTOR3 Right; //  右ベクトル
	};

	class CObjInfo // オブジェクトの情報
	{
	public:
		D3DXVECTOR3 pos; // 平行
		D3DXVECTOR3 rot; // 回転
		D3DXVECTOR3 sca; // スケール
	};

	class CCollar
	{
	public:
		int r, g, b, α;
	public:
		void Set(int _r, int _g, int _b, int _α);
	};
private:
	struct ANGLE
	{
		static constexpr float	_ANGLE_000 = 0.00000000f;
		static constexpr float	_ANGLE_045 = 0.785398185f;
		static constexpr float	_ANGLE_090 = 1.57079637f;
		static constexpr float	_ANGLE_135 = 2.35619450f;
		static constexpr float	_ANGLE_180 = 3.14159274f;
		static constexpr float	_ANGLE_225 = -2.35619450f;
		static constexpr float	_ANGLE_270 = -1.57079637f;
		static constexpr float	_ANGLE_315 = -0.785398185f;

	}m_ANGLE;
public:
	C3DObject();
	~C3DObject();
	// 初期化
	void Init(void);

	// X軸回転
	void RotX(float Rot);

	// Y軸回転
	void RotY(float Rot);

	// 平行移動
	void MoveZ(float Speed);

	// 平行移動
	void MoveX(float Speed);

	// 平行移動
	void MoveY(float Speed);

	// スケール
	void Scale(float Scale);

	// ベクトルを使った行列
	D3DXMATRIX *CreateMtxWorld1(D3DXMATRIX * pOut);

	// 直接値を入れる行列
	D3DXMATRIX *CreateMtxWorld2(D3DXMATRIX * pOut);

	// 角度テンプレート
	ANGLE GetAngle(void) { return this->m_ANGLE; }

	// ラジコン回転
	void RadioControl(D3DXVECTOR3 vecRight, float Spped);

	// 前ベクトル
	void SetVecFront(D3DXVECTOR3 Input);

	// 上ベクトル
	void SetVecUp(D3DXVECTOR3 Input);

	// 右ベクトル
	void SetVecRight(D3DXVECTOR3 Input);
private:
	// 回転の制限
	bool Restriction(D3DXMATRIX pRot, const float * pRang);

	// 回転行列
	D3DXMATRIX* CalcLookAtMatrix(D3DXMATRIX* pOut);

	// 回転行列
	D3DXMATRIX* CalcLookAtMatrixAxisFix(D3DXMATRIX* pOut);
public:
	CMatLook Look; // 視覚情報
	CVec3 Vec; // ベクトル
	CObjInfo Info; // オブジェクトの情報
	CCollar * Collar = nullptr; // 色
	int ID; // データ
};

#endif // !_3DObject_H_
