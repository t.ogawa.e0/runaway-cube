//==========================================================================
// 当たり判定[HitDetermination.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _HitDetermination_H_
#define _HitDetermination_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "3DObject.h"

//==========================================================================
//
// class  : CHitDetermination
// Content: 当たり判定専用クラス
//
//==========================================================================
class CHitDetermination
{
private:
	static constexpr float m_Null = 0.0f;
public:
	CHitDetermination();
	~CHitDetermination();
	// 球当たり判定 当たった場合true
	static bool Ball(C3DObject * TargetA, C3DObject * TargetB, float Scale);
	// シンプルな当たり判定 当たった場合true
	static bool Simple(C3DObject * TargetA, C3DObject * TargetB, float Scale);
private:
	// シンプルな当たり判定の内部 
	static bool Sinple2(float * TargetA, float * TargetB, const float * TargetC, float Scale);
};

#endif // !_HitDetermination_H_
