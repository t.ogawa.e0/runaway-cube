//==========================================================================
// メッシュ[Meshfield.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Meshfield.h"
#include "DXDevice.h"

//==========================================================================
// マクロ定義
//==========================================================================
#ifndef ErrorMessage
#define ErrorMessage(p,c) MessageBox((p), (c), "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING)
#endif // !ErrorMessage

CMeshfield::CMeshfield()
{
}

CMeshfield::~CMeshfield()
{
}

//==========================================================================
// 初期化 Excel 失敗時true
bool CMeshfield::Init(const char * pFile)
{
	// ファイルオープン
	if (this->OpenFile(pFile, this->m_Link, &this->m_NumData)) { return true; }
	if (this->InitMeshfield()) { return true; }

	return false;
}

//==========================================================================
// 初期化 失敗時false
bool CMeshfield::Init(const MESHLINK * pFile, int Size)
{
	this->m_NumData = Size;
	this->InputLink(pFile);
	if (this->InitMeshfield()) { return true; }

	return false;
}

//==========================================================================
// 解放
void CMeshfield::Uninit(void)
{
	if (this->m_MeshData != nullptr)
	{
		for (int i = 0; i < this->m_NumData; i++)
		{
			// バッファ解放
			this->Release(this->m_MeshData[i].pVertexBuffer);

			// インデックスバッファ解放
			this->Release(this->m_MeshData[i].pIndexBuffer);

			// テクスチャ解放
			this->Release(this->m_MeshData[i].pTexture);
		}
		this->Delete(this->m_MeshData);
		this->m_MeshData = nullptr;
		this->m_NumData = 0;
	}
}

//==========================================================================
// 描画
void CMeshfield::Draw(C3DObject * Poly)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	D3DXMATRIX aMtxWorld; // ワールド行列

	if (this->m_MeshData != nullptr)
	{
		// サイズ
		pDevice->SetStreamSource(0, this->m_MeshData[Poly->ID].pVertexBuffer, 0, sizeof(VERTEX_4));	// パイプライン

		pDevice->SetIndices(this->m_MeshData[Poly->ID].pIndexBuffer);

		// FVFの設定
		pDevice->SetFVF(FVF_VERTEX_4);

		// ライト設定
		pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

		pDevice->SetMaterial(&this->m_MeshData->pMatMesh);	// マテリアル情報をセット
		pDevice->SetTexture(0, nullptr);
		pDevice->SetTexture(0, this->m_MeshData[Poly->ID].pTexture);

		// 各種行列の設定
		pDevice->SetTransform(D3DTS_WORLD, Poly->CreateMtxWorld2(&aMtxWorld));

		pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, this->m_MeshData[Poly->ID].Info.MaxVertex, 0, this->m_MeshData[Poly->ID].Info.MaxPrimitive);
	}
}

//==========================================================================
// リンク入力
void CMeshfield::InputLink(const MESHLINK * pInput)
{
	this->m_Link = nullptr;
	this->New(this->m_Link, this->m_NumData);

	for (int i = 0; i < this->m_NumData; i++)
	{
		this->m_Link[i].m_Data[0] = 0;
		this->m_Link[i].m_NumMeshX = pInput[i].m_x;
		this->m_Link[i].m_NumMeshZ = pInput[i].m_z;
		strcpy(this->m_Link[i].m_Data, pInput[i].m_Data);
	}
}

//==========================================================================
// メッシュの初期化
bool CMeshfield::InitMeshfield(void)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	VERTEX_4* pPseudo;// 頂点バッファのロック
	LPWORD* pIndex;

	this->New(this->m_MeshData, this->m_NumData);

	for (int i = 0; i < this->m_NumData; i++)
	{
		this->m_MeshData[i].pVertexBuffer = nullptr;
		this->m_MeshData[i].pIndexBuffer = nullptr;
		this->m_MeshData[i].pTexture = nullptr;
		pIndex = nullptr;
		pPseudo = nullptr;

		// メッシュフィールドの情報算出
		this->MeshFieldInfo(&this->m_MeshData[i].Info, &this->m_Link[i].m_NumMeshX, &this->m_Link[i].m_NumMeshZ);

		if (this->CreateVertexBuffer(pDevice, &this->m_MeshData[i])) { return true; }
		if (this->CreateIndexBuffer(pDevice, &this->m_MeshData[i])) { return true; }
		if (this->Read(pDevice, &this->m_Link[i], &this->m_MeshData[i])) { return true; }

		this->m_MeshData[i].pVertexBuffer->Lock(0, 0, (void**)&pPseudo, D3DLOCK_DISCARD);
		this->CreateVertex(pPseudo, &this->m_MeshData[i].Info);
		this->m_MeshData[i].pVertexBuffer->Unlock();	// ロック解除

		this->m_MeshData[i].pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
		this->CreateIndex(pIndex, &this->m_MeshData[i].Info);
		this->m_MeshData[i].pIndexBuffer->Unlock();	// ロック解除

		this->MaterialSetting(&this->m_MeshData[i]);
	}

	this->Delete(this->m_Link);

	return false;
}

//==========================================================================
// メッシュ情報の生成
void CMeshfield::MeshFieldInfo(DEGENERATEPOLYGONINFORMATION * Output, const int * numX, const int * numZ)
{
	Output->NumMeshX = (*numX);
	Output->NumMeshZ = (*numZ);
	Output->NumXVertex = (*numX) + 1; // 基礎頂点数
	Output->NumZVertex = (*numZ) + 1; // 基礎頂点数
	Output->MaxVertex = Output->NumXVertex * Output->NumZVertex; // 最大頂点数
	Output->NumXVertexWey = x_2 * Output->NumXVertex; // 視覚化されている1列の頂点数
	Output->VertexOverlap = x_2 * ((*numZ) - 1); // 重複する頂点数
	Output->NumMeshVertex = Output->NumXVertexWey * (*numZ); // 視覚化されている全体の頂点数
	Output->MaxIndex = Output->NumMeshVertex + Output->VertexOverlap; // 最大Index数
	Output->MaxPrimitive = (((*numX) * (*numZ)) * x_2) + (Output->VertexOverlap * x_2); // プリミティブ数
}

//==========================================================================
// インデックス情報の生成
void CMeshfield::CreateIndex(LPWORD * Output, const DEGENERATEPOLYGONINFORMATION * Input)
{
	for (int i = 0, Index1 = 0, Index2 = Input->NumXVertex, Overlap = 0; i < Input->MaxIndex; Index1++, Index2++)
	{
		// 通常頂点
		Output[i] = (LPWORD)Index2;
		i++;

		// 重複点
		if (Overlap == Input->NumXVertexWey&&i < Input->MaxIndex)
		{
			Output[i] = (LPWORD)Index2;
			i++;
			Overlap = 0;
		}

		// 通常頂点
		Output[i] = (LPWORD)Index1;
		i++;

		Overlap += x_2;

		// 重複点
		if (Overlap == Input->NumXVertexWey&&i < Input->MaxIndex)
		{
			Output[i] = (LPWORD)Index1;
			i++;
		}
	}
}

//==========================================================================
// バーテックス情報の生成
void CMeshfield::CreateVertex(VERTEX_4 * Output, const DEGENERATEPOLYGONINFORMATION * Input)
{
	bool bZ = false;
	bool bX = false;
	float fPosZ = Input->NumMeshZ*CorrectionValue;
	float fPosX = 0.0f;

	for (int iZ = 0, Counter = 0; iZ < Input->NumZVertex; iZ++, fPosZ--)
	{
		bX = true;
		fPosX = -(Input->NumMeshX*CorrectionValue);
		for (int iX = 0; iX < Input->NumXVertex; iX++, Counter++, fPosX++, Output++)
		{
			Output->pos = D3DXVECTOR3(fPosX, 0.0f, fPosZ);
			Output->color = D3DCOLOR_RGBA(xRGBA, xRGBA, xRGBA, xRGBA);
			Output->Tex = D3DXVECTOR2((FLOAT)bX, (FLOAT)bZ);
			Output->Normal = D3DXVECTOR3(0, 1, 0);
			bX = bX ^ 1;
		}
		bZ = bZ ^ 1;
	}
}

//==========================================================================
// 頂点バッファの生成
bool CMeshfield::CreateVertexBuffer(LPDIRECT3DDEVICE9 pDevice, DEGENERATEPOLYGON *Output)
{
	if (FAILED(pDevice->CreateVertexBuffer(sizeof(VERTEX_4) * Output->Info.MaxVertex, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &Output->pVertexBuffer, nullptr)))
	{
		ErrorMessage(nullptr, "頂点バッファが作れませんでした。");
		return true;
	}
	return false;
}

//==========================================================================
// インデックスバッファの生成
bool CMeshfield::CreateIndexBuffer(LPDIRECT3DDEVICE9 pDevice, DEGENERATEPOLYGON *Output)
{
	if (FAILED(pDevice->CreateIndexBuffer(sizeof(LPWORD) * Output->Info.MaxIndex, D3DUSAGE_WRITEONLY, D3DFMT_INDEX32, D3DPOOL_MANAGED, &Output->pIndexBuffer, nullptr)))
	{
		ErrorMessage(nullptr, "インデックスバッファが作れませんでした。");
		return true;
	}
	return false;
}

//==========================================================================
// 読込み
bool CMeshfield::Read(LPDIRECT3DDEVICE9 pDevice, FILELINK2* pLink, DEGENERATEPOLYGON *Output)
{
	if (FAILED(D3DXCreateTextureFromFile(pDevice, pLink->m_Data, &Output->pTexture)))
	{
		ErrorMessage(nullptr, "テクスチャの読み込み失敗");
		return true;
	}
	return false;
}

//==========================================================================
// マテリアルの設定
void CMeshfield::MaterialSetting(DEGENERATEPOLYGON *Output)
{
	ZeroMemory(&Output->pMatMesh, sizeof(Output->pMatMesh));
	Output->pMatMesh.Diffuse.r = 1.0f; // ディレクショナルライト
	Output->pMatMesh.Diffuse.g = 1.0f; // ディレクショナルライト
	Output->pMatMesh.Diffuse.b = 1.0f; // ディレクショナルライト
	Output->pMatMesh.Diffuse.a = 1.0f; // ディレクショナルライト
	Output->pMatMesh.Ambient.r = 1.0f; // アンビエントライト
	Output->pMatMesh.Ambient.g = 1.0f; // アンビエントライト
	Output->pMatMesh.Ambient.b = 1.0f; // アンビエントライト
	Output->pMatMesh.Ambient.a = 1.0f; // アンビエントライト
}
