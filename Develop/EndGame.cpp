//==========================================================================
// ゲーム終了[EndGame.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "EndGame.h"

//==========================================================================
// 実体
//==========================================================================
bool * CEndGame::m_EndKey;
bool * CEndGame::m_GameClear; // ゲームクリア
bool * CEndGame::m_GameOver; // ゲームオーバー
bool * CEndGame::m_GameTimeUp; // たいむあっぷ
int * CEndGame::m_Count;
int * CEndGame::m_αCount;
float * CEndGame::m_Scale;
C2DObject * CEndGame::m_Pos; // 座標
C2DPolygon * CEndGame::m_Poly; // ポリゴン

CEndGame::CEndGame()
{
}

CEndGame::~CEndGame()
{
}

//==========================================================================
// 初期化
bool CEndGame::Init(void)
{
	const char * pTexList[] =
	{
		"resource/texture/ColorTex.png",
		"resource/texture/GameOverフォント.png",
		"resource/texture/Clearフォント.png",
		"resource/texture/Timeupフォント.png",
	};

	New(m_Pos, Sizeof(pTexList));
	New(m_Poly);
	New(m_Scale);
	New(m_αCount);
	New(m_Count);
	New(m_GameOver);
	New(m_GameClear);
	New(m_GameTimeUp);
	New(m_EndKey);

	if (m_Poly->Init(pTexList, Sizeof(pTexList))) { return true; }

	for (int i = 0; i < (int)Sizeof(pTexList); i++)
	{
		m_Pos[i].Init(i);
	}

	// ゲームオーバーのredフェード
	(*m_GameOver) = false;
	(*m_GameClear) = false;
	(*m_GameTimeUp) = false;
	(*m_EndKey) = false;
	m_Pos[0].SetColor(0, 0, 0, 0);
	m_Poly->GetTexPalam(m_Pos[0].m_ID)->Widht = CDirectXDevice::GetWindowsSize().m_Width;
	m_Poly->GetTexPalam(m_Pos[0].m_ID)->Height = CDirectXDevice::GetWindowsSize().m_Height;

	m_Pos[1].SetCentralCoordinatesMood(true);
	m_Pos[1].Scale(-1);
	m_Pos[1].SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
	m_Pos[1].SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 2));

	m_Pos[2].SetCentralCoordinatesMood(true);
	m_Pos[2].SetColor(255, 255, 0, 255);
	m_Pos[2].Scale(-1);
	m_Pos[2].SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
	m_Pos[2].SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 2));

	m_Pos[3].SetCentralCoordinatesMood(true);
	m_Pos[3].SetColor(255, 255, 0, 255);
	m_Pos[3].Scale(-1);
	m_Pos[3].SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
	m_Pos[3].SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 2));

	(*m_Scale) = 0.0f;
	(*m_Count) = 0;
	(*m_αCount) = 255;

	return false;
}

//==========================================================================
// 解放
void CEndGame::Uninit(void)
{
	m_Poly->Uninit();

	Delete(m_Count);
	Delete(m_αCount);
	Delete(m_GameClear);
	Delete(m_GameOver);
	Delete(m_GameTimeUp);
	Delete(m_Scale);
	Delete(m_Pos);
	Delete(m_Poly);
	Delete(m_EndKey);
}

//==========================================================================
// 更新
bool CEndGame::Update(void)
{
	if ((*m_GameOver))
	{
		m_Pos[0].SetColor(0, 0, 0, 200);
		if ((*m_Count) != 0)
		{
			(*m_Count)++;
		}

		if (60<(*m_Count))
		{
			m_Pos[1].Scale(0.05f);
			(*m_Scale) += 0.05f;
			(*m_αCount) -= 5;
			if ((*m_αCount) <= 0)
			{
				(*m_αCount) = 0;
				(*m_EndKey) = true;
			}
			m_Pos[1].SetColor(255, 255, 255, (*m_αCount));
		}

		if ((*m_Scale)<1.0f && (*m_Count) == 0)
		{
			m_Pos[1].Scale(0.05f);
			(*m_Scale) += 0.05f;
		}
		else if (1.0f<(*m_Scale) && (*m_Count) == 0)
		{
			(*m_Count)++;
			(*m_αCount) = 255;
		}
		return true;
	}

	if ((*m_GameClear))
	{
		m_Pos[0].SetColor(0, 0, 0, 255 - (*m_αCount));
		if ((*m_Count) != 0)
		{
			(*m_Count)++;
		}

		if (60<(*m_Count))
		{
			m_Pos[2].Scale(0.05f);
			(*m_Scale) += 0.05f;
			(*m_αCount) -= 5;
			if ((*m_αCount) <= 0)
			{
				(*m_αCount) = 0;
				(*m_EndKey) = true;
			}
			m_Pos[2].SetColor(255, 255, 0, (*m_αCount));
		}

		if ((*m_Scale)<1.0f && (*m_Count) == 0)
		{
			m_Pos[2].Scale(0.05f);
			(*m_Scale) += 0.05f;
		}
		else if (1.0f<(*m_Scale) && (*m_Count) == 0)
		{
			(*m_Count)++;
			(*m_αCount) = 255;
		}
		return true;
	}

	if ((*m_GameTimeUp))
	{
		m_Pos[0].SetColor(0, 0, 0, 255 - (*m_αCount));
		if ((*m_Count) != 0)
		{
			(*m_Count)++;
		}

		if (60<(*m_Count))
		{
			m_Pos[3].Scale(0.05f);
			(*m_Scale) += 0.05f;
			(*m_αCount) -= 5;
			if ((*m_αCount) <= 0)
			{
				(*m_αCount) = 0;
				(*m_EndKey) = true;
			}
			m_Pos[3].SetColor(255, 255, 0, (*m_αCount));
		}

		if ((*m_Scale)<1.0f && (*m_Count) == 0)
		{
			m_Pos[3].Scale(0.05f);
			(*m_Scale) += 0.05f;
		}
		else if (1.0f<(*m_Scale) && (*m_Count) == 0)
		{
			(*m_Count)++;
			(*m_αCount) = 255;
		}
		return true;
	}

	return false;
}

//==========================================================================
// 描画
void CEndGame::Draw(void)
{
	if ((*m_GameOver))
	{
		m_Poly->Draw(&m_Pos[0]);
		m_Poly->Draw(&m_Pos[1]);
	}
	if ((*m_GameClear))
	{
		m_Poly->Draw(&m_Pos[0]);
		m_Poly->Draw(&m_Pos[2]);
	}
	if ((*m_GameTimeUp))
	{
		m_Poly->Draw(&m_Pos[0]);
		m_Poly->Draw(&m_Pos[3]);
	}
}
