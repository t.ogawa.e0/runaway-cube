//==========================================================================
// �_���[�W���o[DamageEffect.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "DamageEffect.h"

//==========================================================================
// ����
//==========================================================================
CDamageEffect::CParam * CDamageEffect::m_Param;

CDamageEffect::CDamageEffect()
{
}

CDamageEffect::~CDamageEffect()
{
}

//==========================================================================
// ������
bool CDamageEffect::Init(void)
{
	const char * pTexData[] =
	{
		"resource/texture/DamageEffect.png",
	};
	New(m_Param);

	if (m_Param->m_Poly.Init(pTexData, Sizeof(pTexData))) { return true; }

	m_Param->m_Count = 0;
	m_Param->m_��Count = 0;
	m_Param->m_Pos.Init(0);
	m_Param->m_Poly.GetTexPalam(m_Param->m_Pos.m_ID)->Widht = CDirectXDevice::GetWindowsSize().m_Width;
	m_Param->m_Poly.GetTexPalam(m_Param->m_Pos.m_ID)->Height = CDirectXDevice::GetWindowsSize().m_Height;
	m_Param->m_Pos.SetColor(255, 0, 0, m_Param->m_��Count);

	return false;
}

//==========================================================================
// ���
void CDamageEffect::Uninit(void)
{
	m_Param->m_Poly.Uninit();
	Delete(m_Param);
}

//==========================================================================
// �X�V
void CDamageEffect::Update(void)
{
	if (m_Param != nullptr)
	{
		m_Param->m_Count++;

		if (5 <= m_Param->m_Count)
		{
			m_Param->m_��Count--;
			m_Param->m_Count = 0;
			if (m_Param->m_��Count <= 0)
			{
				m_Param->m_��Count = 0;
			}
		}
		m_Param->m_Pos.SetColor(255, 0, 0, m_Param->m_��Count);
	}
}

//==========================================================================
// �`��
void CDamageEffect::Draw(void)
{
	if (m_Param != nullptr)
	{
		m_Param->m_Poly.Draw(&m_Param->m_Pos);
	}
}

//==========================================================================
// �_���[�W���o
void CDamageEffect::Damage(void)
{
	m_Param->m_��Count += 3;
	if (255 <= m_Param->m_��Count)
	{
		m_Param->m_��Count = 255;
	}
}

int * CDamageEffect::Get��(void)
{
	if (m_Param == nullptr)
	{
		return nullptr;
	}

	return &m_Param->m_��Count;
}
