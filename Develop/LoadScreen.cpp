//==========================================================================
// ロードスクリーン[LoadScreen.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "LoadScreen.h"

//==========================================================================
// 実体
//==========================================================================
C2DObject *CLoadScreen::m_Pos; // 座標
C2DPolygon *CLoadScreen::m_Poly; // ポリゴン
CLoadScreen::CParam *CLoadScreen::m_Param; // パラメータ
int CLoadScreen::m_NumData; // データ数

CLoadScreen::CLoadScreen()
{
}

CLoadScreen::~CLoadScreen()
{
}

//==========================================================================
// 初期化
bool CLoadScreen::Init(void)
{
	CLight light;
	C2DObject * pPos;
	float PosX = 0.0f;
	float PosY = 0.0f;
	static const char * cLinkData[] =
	{
		"resource/texture/ColorTex.png",
		"resource/texture/LoadTex.png",
		"resource/texture/NowLoading.png",
	};

	// データ数記録
	m_NumData = Sizeof(cLinkData);
	light.Init({ 1,0,0 });

	// メモリ確保
	New(m_Pos, Sizeof(cLinkData));
	New(m_Param, Sizeof(cLinkData));
	New(m_Poly, 1);

	// テクスチャの確保
	if (m_Poly->Init(cLinkData, m_NumData)) { return true; }

	// データの数回す
	for (int i = 0; i < m_NumData; i++)
	{
		// 初期化の共通部
		pPos = &m_Pos[i];
		pPos->Init(i);
		pPos->m_ID = i;
		m_Param[i].m_α = 255;
		m_Param[i].m_Change = false;

		// 種類に合わせて
		switch (i)
		{
		case 0: // ただの黒背景
			m_Poly->GetTexPalam(pPos->m_ID)->Height = CDirectXDevice::GetWindowsSize().m_Height;
			m_Poly->GetTexPalam(pPos->m_ID)->Widht = CDirectXDevice::GetWindowsSize().m_Width;
			PosX = 0.0f;
			PosY = 0.0f;
			pPos->SetColor(0, 0, 0, m_Param[i].m_α);
			break;
		case 1: // ロードを表す輪
			pPos->SetCentralCoordinatesMood(true);
			pPos->Scale(-0.9f);
			PosX = (float)CDirectXDevice::GetWindowsSize().m_Width*0.9f;
			PosY = (float)CDirectXDevice::GetWindowsSize().m_Height*0.9f;
			m_Param[i].m_α = 100;
			pPos->SetColor(255, 255, 0, m_Param[i].m_α);
			break;
		case 2: // 読み込み中フォンと
			m_Pos[i] = m_Pos[i - 1];
			pPos->SetCentralCoordinatesMood(true);
			pPos->m_ID = i;
			pPos->SetColor(255, 255, 0, m_Param[i].m_α);
			break;
		default:
			break;
		}

		// 初期座標
		pPos->SetPos(PosX, PosY);
	}

	return false;
}

//==========================================================================
// 解放
void CLoadScreen::Uninit(void)
{
	// テクスチャ解放
	m_Poly->Uninit();
	
	// メモリ確保
	Delete(m_Pos);
	Delete(m_Param);
	Delete(m_Poly);
}

//==========================================================================
// 更新
void CLoadScreen::Update(void)
{
	m_Pos[1].Angle(0.05f);

	αChange(2, 5);
	m_Pos[2].SetColor(255, 255, 0, m_Param[2].m_α);
}

//==========================================================================
// 描画
void CLoadScreen::Draw(void)
{
	for (int i = 0; i < m_NumData; i++)
	{
		m_Poly->Draw(&m_Pos[i]);
	}
}

//==========================================================================
// αチェンジ
void CLoadScreen::αChange(int ID, int Speed)
{
	// チェンジフラグ
	if (m_Param[ID].m_Change)
	{
		m_Param[ID].m_α -= Speed;
		if (m_Param[ID].m_α <= 0)
		{
			m_Param[ID].m_α = 0;
			m_Param[ID].m_Change = false;
		}
	}
	else
	{
		m_Param[ID].m_α += Speed;
		if (255 <= m_Param[ID].m_α)
		{
			m_Param[ID].m_α = 255;
			m_Param[ID].m_Change = true;
		}
	}
}
