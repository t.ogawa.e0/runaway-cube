//==========================================================================
// ゲーム終了[EndGame.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _EndGame_H_
#define _EndGame_H_

//==========================================================================
//
// class  : CEndGame
// Content: ゲーム終了
//
//==========================================================================
class CEndGame : private CTemplate
{
public:
	CEndGame();
	~CEndGame();
	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static bool Update(void);
	// 描画
	static void Draw(void);
	// ゲームオーバー
	static void GameOver(void) { (*m_GameOver) = true; }
	// ゲームクリア
	static void GameClear(void) { (*m_GameClear) = true; }
	// タイムアップ
	static void GameTimeUp(void) { (*m_GameTimeUp) = true; }
	// クリア処理終了キー
	static bool *GetClearKey(void) { return m_GameClear; }
	// ゲームオーバー処理終了キー
	static bool *GetOverKey(void) { return m_GameOver; }
	// タイムアップ処理終了キー
	static bool *GetTimeUpKey(void) { return m_GameTimeUp; }
	// 処理終了キー
	static bool *GetEndKey(void) { return m_EndKey; }
private:
	static bool * m_EndKey;
	static int * m_Count;
	static int * m_αCount;
	static float * m_Scale;
	static bool * m_GameTimeUp; // たいむあっぷ
	static bool * m_GameClear; // ゲームクリア
	static bool * m_GameOver; // ゲームオーバー
	static C2DObject * m_Pos; // 座標
	static C2DPolygon * m_Poly; // ポリゴン
};

#endif // !_EndGame_H_
