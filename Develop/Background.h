//==========================================================================
// バックグラウンド[Background.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Background_H_
#define _Background_H_

//==========================================================================
//
// class  : CBackground
// Content: バックグラウンド
//
//==========================================================================
class CBackground : private CTemplate
{
private:
	static constexpr int m_NumBrockX = 12;
	static constexpr int m_NumBrockY = 12;
public:
	CBackground();
	~CBackground();
	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static void Update(void);
	// 描画
	static void Draw(void);
	// ビューカメラのゲッター
	static D3DXMATRIX *GetCamera(void) { return m_Camera->CreateView(); }
private:
	static C2DObject * m_Pos; // 座標
	static C2DPolygon * m_Poly; // ポリゴン
	static CCamera *m_Camera; // ワールドビューカメラ
};

#endif // !_Background_H_
