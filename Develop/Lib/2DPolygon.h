//==========================================================================
// 2Dポリゴン[2DPolygon.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _2DPolygon_H_
#define _2DPolygon_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Vertex3D.h"
#include "SetRender.h"
#include "2DObject.h"
#include "Template.h"

//==========================================================================
//
// class  : C2DPolygon
// Content: 2Dポリゴン 
//
//==========================================================================
class C2DPolygon : private VERTEX_3D, private CSetRender, private CTemplate
{
private:
	class TEXTURE
	{
	public:
		LPDIRECT3DTEXTURE9 Texture; // テクスチャポインタ
		int Widht; // 幅
		int Height; // 高さ
	};
public:
	C2DPolygon();
	~C2DPolygon();

	// 初期化 失敗時true
	bool Init(const char ** Input, int NumData);

	// 解放
	void Uninit(void);

	// 描画
	void Draw(C2DObject * Input, bool ADD = false);

	// テクスチャの情報
	TEXTURE * GetTexPalam(int Num) { return &this->m_Texture[Num]; }

	// テクスチャの枚数
	int GetNumTex(void) { return this->m_NumTexture; }
private:
	LPDIRECT3DVERTEXBUFFER9 m_pVertexBuffer; // バッファ
	TEXTURE *m_Texture; // テクスチャの格納
	int m_NumTexture; // テクスチャの枚数
};

#endif // !_2DPolygon_H_
