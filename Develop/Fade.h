//==========================================================================
// フェード[Fade.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Fade_H_
#define _Fade_H_

//==========================================================================
//
// class  : CFade
// Content: フェード
//
//==========================================================================
class CFade : private CTemplate
{
private:
	// パラメータ
	class CParam
	{
	public:
		int m_α; // α
		bool m_Change; // change
		bool m_Key; // 鍵
		bool m_Draw; // 描画判定
		bool m_In; // フェードイン判定
	};
public:
	CFade();
	~CFade();
	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static bool Update(void);
	// 描画
	static void Draw(void);
	// フェードイン
	static void In(void);
	// フェードアウト
	static void Out(void);
	// フェードイン終了判定
	static bool FeadInEnd(void);
	static bool GetDraw(void) { return m_Param->m_Draw; };
private:
	static C2DObject * m_Pos; // 座標
	static C2DPolygon * m_Poly; // ポリゴン
	static CParam * m_Param; // パラメータ
};

#endif // !_Fade_H_
