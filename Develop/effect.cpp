//==========================================================================
// エフェクト[effect.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "effect.h"

CEffect::CEffect()
{
}

CEffect::~CEffect()
{
}

//==========================================================================
// 初期化
bool CEffect::Init(const char * InputExplosion, int UpdateFrame, int Pattern, int Direction)
{
	// アドレスの初期化
	this->m_EffectData = nullptr;
	this->m_pStartAddress = nullptr;

	// メモリ確保
	this->New(this->m_EffectData);

	// 管理用パラメータの初期化
	this->m_IDCount = 0;
	this->m_NumData = 0;
	this->m_EfectType = EffectType::Anim;
	this->m_Add = false;

	// モデルの初期化
	return this->m_EffectData->Init(InputExplosion, UpdateFrame, Pattern, Direction);
}

bool CEffect::Init(const char * InputExplosion)
{
	// アドレスの初期化
	this->m_EffectData = nullptr;
	this->m_pStartAddress = nullptr;

	// メモリ確保
	this->New(this->m_EffectData);

	// 管理用パラメータの初期化
	this->m_IDCount = 0;
	this->m_NumData = 0;
	this->m_EfectType = EffectType::α;
	this->m_Add = true;

	// モデルの初期化
	return this->m_EffectData->Init(InputExplosion);
}

//==========================================================================
// 解放
void CEffect::Uninit(void)
{
	CData *pData = this->m_pStartAddress; // 初期アドレスを記憶
	CData *pNextDelete = nullptr; // 次のアドレス

	// エフェクトの解放
	if (this->m_EffectData != nullptr)
	{
		this->m_EffectData->Uninit();
		this->Delete(this->m_EffectData);
	}

	// 先頭アドレスを初期化
	this->m_pStartAddress = nullptr;

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// 次のアドレスを記憶
		pNextDelete = pData->m_pNext;

		// メモリ解放
		this->Delete(pData->m_Pos.Collar);
		this->Delete(pData->m_AnimCounter);
		this->Delete(pData);
		this->m_NumData--;

		pData = pNextDelete;

		// データ数が0の時
		if (this->m_NumData == 0)
		{
			this->m_pStartAddress = nullptr;
			this->m_IDCount = 0;
		}
	}
}

//==========================================================================
// 更新
void CEffect::Update(void)
{
	// 先頭アドレスを記憶
	CData *pData = this->m_pStartAddress;
	CData *pDeleteData = nullptr; // 消去アドレス

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// エフェクトの終了判定が出ている場合アドレスを記憶
		if (this->m_EfectType == EffectType::α&&pData->m_Pos.Collar != nullptr)
		{
			pData->m_Pos.Collar->α -= 10;
			if (pData->m_Pos.Collar->α <= 0)
			{
				pDeleteData = pData;
			}
		}

		// エフェクトの終了判定が出ている場合アドレスを記憶
		if (this->m_EfectType == EffectType::Anim)
		{
			if (this->m_EffectData->GetPattanNum(pData->m_AnimCounter))
			{
				pDeleteData = pData;
			}
		}

		// 次のアドレスを記憶
		pData = pData->m_pNext;

		// 消去対象のアドレスが入っている場合解放
		if (pDeleteData != nullptr)
		{
			this->PinpointDelete(&pDeleteData->m_ID);
			pDeleteData = nullptr;
		}
	}
}

//==========================================================================
// 描画
void CEffect::Draw(D3DXMATRIX *MtxView)
{
	// 先頭アドレスを記憶
	CData *pData = this->m_pStartAddress;

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		this->m_EffectData->Draw(&pData->m_Pos, MtxView, pData->m_AnimCounter, this->m_Add);

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}
}

//==========================================================================
// エフェクトセット
void CEffect::SetEfect(const C3DObject * Input)
{
	CData *pMain = nullptr; // 生成時のアドレス
	CData *pData = nullptr; // 次のアドレス
	this->m_NumData++;
	this->m_IDCount++;

	// メモリを確保
	this->New(pMain);
	pMain->m_pNext = nullptr;
	pMain->m_pBack = nullptr;
	pMain->m_AnimCounter = nullptr;
	pMain->m_Pos.Collar = nullptr;
	pMain->m_ID = this->m_IDCount;
	pMain->m_Pos = *Input;

	if (this->m_EfectType == EffectType::Anim)
	{
		this->New(pMain->m_AnimCounter);
		this->m_EffectData->AnimationCountInit(pMain->m_AnimCounter);
	}
	if (this->m_EfectType == EffectType::α&&Input->Collar != nullptr)
	{
		this->New(pMain->m_Pos.Collar);
		*pMain->m_Pos.Collar = *Input->Collar;
	}

	// 先頭アドレスがなかった場合
	if (this->m_pStartAddress == nullptr)
	{
		this->m_pStartAddress = pMain;
	}
	else if (this->m_pStartAddress != nullptr)
	{
		// 先頭アドレスを記憶
		pData = this->m_pStartAddress;

		// アドレスがnullではない限り検索を繰り返す
		for (;;)
		{
			// 次のアドレスがnullの場合
			if (pData->m_pNext == nullptr)
			{
				break;
			}

			// 検索をするアドレスを記憶
			pData = pData->m_pNext;
		}

		// 次のアドレスを記憶
		pData->m_pNext = pMain;

		// ひとつ前のアドレスに記憶
		pMain->m_pBack = pData;
	}
}

//==========================================================================
// 特定のデータ消去
void CEffect::PinpointDelete(const int * ID)
{
	CData *pData = this->m_pStartAddress; // 先頭アドレスを記憶
	CData *pBack = nullptr; // 前のデータ
	CData *pNext = nullptr; // 次のデータ

	// 解放対象が見つかるまで検索を続ける
	for (;;)
	{
		// アドレスが無ければ終了
		if (pData == nullptr)
		{
			break;
		}

		// 検索IDとヒット
		if (pData->m_ID == (*ID))
		{
			// 先頭アドレスと検索IDが同じ場合先頭アドレスを書き換える
			if (this->m_pStartAddress->m_ID == pData->m_ID)
			{
				this->m_pStartAddress = pData->m_pNext;
			}

			pBack = pData->m_pBack; // ひとつ前のアドレスを記憶
			pNext = pData->m_pNext; // 次のアドレスを記憶

			// アドレスがある場合次のアドレスをつなぎなおす
			if (pBack != nullptr)
			{
				pBack->m_pNext = pNext;
			}

			// アドレスがある場合ひとつ前のアドレスをつなぎなおす
			if (pNext != nullptr)
			{
				pNext->m_pBack = pBack;
			}

			// メモリ解放
			this->Delete(pData->m_Pos.Collar);
			this->Delete(pData->m_AnimCounter);
			this->Delete(pData);
			this->m_NumData--;

			// データ数が0の時
			if (this->m_NumData == 0)
			{
				this->m_pStartAddress = nullptr;
				this->m_IDCount = 0;
			}

			break; // 終了
		}

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}
}
