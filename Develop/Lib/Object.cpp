//==========================================================================
// オブジェクト管理[Object.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Object.h"

//==========================================================================
// 実体
//==========================================================================
CObject *CObject::m_pStart = nullptr;
int CObject::m_NumData = 0; // データ数
int CObject::m_IDCount = 0; // IDカウンタ

//==========================================================================
// コンストラクタ
CObject::CObject()
{
	CObject *pMain; // 生成時のアドレス
	CObject *pData; // 次のアドレス

	m_NumData++;
	m_IDCount++;

	pMain = this;
	pData = nullptr;

	pMain->m_pNext = nullptr;
	pMain->m_pBack = nullptr;
	pMain->m_ID = m_IDCount;

	// 先頭アドレスがなかった場合
	if (m_pStart == nullptr)
	{
		m_pStart = pMain;
	}
	else if (m_pStart != nullptr)
	{
		// 先頭アドレスを記憶
		pData = m_pStart;

		// アドレスがnullではない限り検索を繰り返す
		for (;;)
		{
			// 次のアドレスがnullの場合
			if (pData->m_pNext == nullptr)
			{
				break;
			}

			// 検索をするアドレスを記憶
			pData = pData->m_pNext;
		}

		// 次のアドレスを記憶
		pData->m_pNext = pMain;

		// ひとつ前のアドレスに記憶
		pMain->m_pBack = pData;
	}
}

CObject::~CObject()
{
}

//==========================================================================
// 登録済みオブジェクトの初期化
bool CObject::InitAll(void)
{
	// 先頭アドレスを記憶
	CObject *pData = m_pStart;

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// 初期化
		if (pData->Init())
		{
			return true;
		}

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}

	return false;
}

//==========================================================================
// 登録済みオブジェクトの解放
void CObject::UninitAll(void)
{
	CObject *pData = m_pStart; // 初期アドレスを記憶
	CObject *pNextDelete = nullptr; // 次のアドレス

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// 次のアドレスを記憶
		pNextDelete = pData->m_pNext;

		// 解放
		pData->Uninit();

		// メモリ解放
		if (pData != nullptr)
		{
			delete[] pData;
			pData = nullptr;
		}

		m_NumData--;

		pData = pNextDelete;

		// データ数が0の時
		if (m_NumData == 0)
		{
			m_pStart = nullptr;
			m_IDCount = 0;
		}
	}
}

//==========================================================================
// 登録済みオブジェクトの更新
void CObject::UpdateAll(void)
{
	// 先頭アドレスを記憶
	CObject *pData = m_pStart;

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// 更新
		pData->Update();

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}
}

//==========================================================================
// 登録済みオブジェクトの描画
void CObject::DrawAll(void)
{
	// 先頭アドレスを記憶
	CObject *pData = m_pStart;

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// 描画
		pData->Draw();

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}
}
