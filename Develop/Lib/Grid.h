//==========================================================================
// グリッド[Grid.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Grid_H_
#define _Grid_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Vertex3D.h"
#include "Template.h"

//==========================================================================
//
// class  : CGrid
// Content: グリッド
//
//==========================================================================
class CGrid : private VERTEX_3D, private CTemplate // グリッド
{
private:
	static constexpr int nNumGrid = 44;
private:
	static D3DXMATRIX *m_aMtxWorld; // ワールド行列
	static VERTEX_2 *m_vPos; // 頂点の作成
public:
	CGrid();
	~CGrid();
	// 初期化
	void Init(void);
	// 解放
	void Uninit(void);
	// 描画
	void Draw(void);
};

#endif // !_Grid_H_
