//==========================================================================
// �G���^�[����������[PressEnter.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "PressEnter.h"

//==========================================================================
// ����
//==========================================================================
C2DObject * CPressEnter::m_Pos; // ���W
C2DPolygon * CPressEnter::m_Poly; // �|���S��
int * CPressEnter::m_��CountLimit; // ���l�̐���
int * CPressEnter::m_��Count; // �J�E���g
bool * CPressEnter::m_��Change; // �`�F���W

CPressEnter::CPressEnter()
{
}

CPressEnter::~CPressEnter()
{
}

//==========================================================================
// ������
bool CPressEnter::Init(void)
{
	const char * pTexLink[] =
	{
		"resource/texture/PressEnter�t�H���g.png",
	};

	New(m_Pos);
	New(m_Poly);
	New(m_��CountLimit);
	New(m_��Count);
	New(m_��Change);

	m_Pos->Init(0);
	m_Pos->SetCentralCoordinatesMood(true);
	m_Pos->SetColor(255, 255, 0, 255);
	m_Pos->SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
	m_Pos->SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 1.1f));

	(*m_��Change) = true;
	(*m_��Count) = 0;
	(*m_��CountLimit) = 60;

	return m_Poly->Init(pTexLink, Sizeof(pTexLink));
}

//==========================================================================
// ���
void CPressEnter::Uninit(void)
{
	m_Poly->Uninit();

	Delete(m_Pos);
	Delete(m_Poly);
	Delete(m_��CountLimit);
	Delete(m_��Count);
	Delete(m_��Change);
}

//==========================================================================
// �X�V
void CPressEnter::Update(void)
{
	if ((*m_��Change))
	{
		m_Pos->SetColor(255, 255, 0, 255);
	}
	else
	{
		m_Pos->SetColor(255, 255, 0, 0);
	}

	if ((*m_��CountLimit) <= (*m_��Count))
	{
		(*m_��Count) = 0;
		Bool(m_��Change);
	}

	(*m_��Count)++;
}

//==========================================================================
// �`��
void CPressEnter::Draw(void)
{
	m_Poly->Draw(m_Pos);
}
