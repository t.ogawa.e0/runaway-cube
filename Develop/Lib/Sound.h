//==========================================================================
// サウンド[Sound.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Sound_H_
#define _Sound_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <XAudio2.h>
#include "Template.h"

//==========================================================================
//
// class  : CSoundDevice
// Content: サウンドのデバイス
//
//==========================================================================
class CSoundDevice : private CTemplate
{
public:
	CSoundDevice();
	~CSoundDevice();
	// 初期化
	static HRESULT Init(HWND * hWnd);
	// 解放
	static void Uninit(void);
	// ゲッター
	static HWND *GetWnd(void) { return &m_hWnd; }
	// ゲッター
	static IXAudio2 *GetAudio2(void) { return m_pXAudio2; }
private:
	// ボイスデータの破棄
	static void DestroyVoice(void);
private:
	static IXAudio2 *m_pXAudio2; // XAudio2オブジェクトへのインターフェイス
	static IXAudio2MasteringVoice *m_pMasteringVoice; // マスターボイス
	static HWND m_hWnd; // ウィンドハンドル
};

//==========================================================================
//
// class  : CSound
// Content: サウンド
//
//==========================================================================
class CSound : private CTemplate
{
private:
	//==========================================================================
	//
	// class  : CParam
	// Content: データ
	//
	//==========================================================================
	class CParam
	{
	public:
		IXAudio2SourceVoice *m_pSourceVoice; // サウンド
		BYTE *m_pDataAudio; // オーディオデーター
		DWORD m_SizeAudio; // オーディオデータサイズ
		int m_CntLoop; // ループカウント
		float m_Volume; // ボリューム
	public:
		// ボイスデータの破棄
		void DestroyVoice(void);
	};
	//==========================================================================
	//
	// class  : CLabel
	// Content: サウンドのリンク
	//
	//==========================================================================
	struct CLabel
	{
	public:
		char m_pFilename[256]; // ファイル名
		int m_CntLoop; // ループカウント -1=ループ 0 一回
		float m_Volume; // ボリューム 最大=1.0f 無音=0.0f
	};
public:
	typedef CLabel SoundLabel;
public:
	CSound();
	~CSound();
	// 初期化
	HRESULT Init(const SoundLabel *cLnk, int size);
	// 解放
	void Uninit(void);
	// 再生
	HRESULT Play(int label);
	// 特定のサウンド停止
	void Stop(int label);
	// 格納してある全てのサウンド停止
	void Stop(void);
	// 特定のサウンドのボリューム設定
	// volume 最大=1.0f
	// volume 無音=0.0f
	void Volume(int label, float volume);
	// 全てのサウンドのボリューム設定
	// volume 最大=1.0f
	// volume 無音=0.0f
	void Volume(float volume);
	// 全てのサウンドのボリューム自動設定
	void Volume(void);
private:
	// チャンクのチェック
	HRESULT CheckChunk(HANDLE hFile, DWORD format, DWORD *pChunkSize, DWORD *pChunkDataPosition);
	// チャンクデータの読み込み
	HRESULT ReadChunkData(HANDLE hFile, void *pBuffer, DWORD dwBuffersize, DWORD dwBufferoffset);
private:
	int m_nNumSound; // サウンド数
	CParam *m_pSound; // サウンドデータ
};

#endif // !_Sound_H_
