//==========================================================================
// 影[Shadow.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Shadow_H_
#define _Shadow_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Rectangle.h"
#include "Vertex3D.h"
#include "SetRender.h"
#include "3DObject.h"
#include "Template.h"

//==========================================================================
//
// class  : CShadow
// Content: 影
//
//==========================================================================
class CShadow : private CRectangle, private VERTEX_3D, private CSetRender, private CTemplate
{
private:
	class CUV // UV
	{
	public:
		float u0;
		float v0;
		float u1;
		float v1;
	};
private:
	static constexpr float VCRCT = 0.5f;
	static constexpr int DefaltColor = 80;
	static constexpr int ColorBuf = 8;
private:
	VERTEX_4* m_pPseudo;// 頂点バッファのロック
public:
	CShadow();
	~CShadow();

	// 初期化 失敗時true
	bool Init(const char * Input);

	// 解放
	void Uninit(void);

	// 描画
	void Draw(const C3DObject * pInput);
private:
	// 頂点バッファの生成
	void CreateVertexBuffer(VERTEX_4 * Output, CUV * uv);
private:
	LPDIRECT3DTEXTURE9 m_pTexture;	// テクスチャを張り付けるためのポインタ
	LPDIRECT3DVERTEXBUFFER9 m_pVertexBuffer;	// バッファ
	LPDIRECT3DINDEXBUFFER9 m_pIndexBuffer; // インデックスバッファ
};

#endif // !_Shadow_H_
