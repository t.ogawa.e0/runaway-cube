//==========================================================================
// シーン遷移[SceneChange.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "SceneChange.h"
#include "title.h"
#include "home.h"
#include "game.h"
#include "result.h"
#include "load.h"
#include "practice.h"
#include "screensaver.h"

//==========================================================================
// 実体
//==========================================================================
CBaseScene * CSceneManager::m_pScene = nullptr;

CSceneManager::CSceneManager()
{
	m_pScene = nullptr;
}

CSceneManager::~CSceneManager()
{
	if (m_pScene != nullptr)
	{
		delete[] m_pScene;
		m_pScene = nullptr;
	}
}

//==========================================================================
// シーンの切り替え
bool CSceneManager::ChangeScene(SceneName Name)
{
	// 解放
	Uninit();

	switch (Name)
	{
	case SceneName::NOME:
		ErrorMessage(nullptr, "Scene Ellor : -1");
		return true;
		break;
	case SceneName::TITLE:
		m_pScene = new CTitleScene();
		break;
	case SceneName::HOME:
		m_pScene = new CHomeScene();
		break;
	case SceneName::GAME:
		m_pScene = new CGameScene();
		break;
	case SceneName::RESULT:
		m_pScene = new CResultScene();
		break;
	case SceneName::SCREEN_SAVER:
		m_pScene = new CScreenSaverScene();
		break;
	case SceneName::PRACTICE:
		m_pScene = new CPracticeScene();
		break;
	case SceneName::LOAD:
		m_pScene = new CLoadScene();
		break;
	default:
		ErrorMessage(nullptr, "Scene Ellor : -1");
		return true;
		break;
	}

	// 初期化
	return Init();
}

//==========================================================================
// 初期化
bool CSceneManager::Init(void)
{
	return m_pScene->Init();
}

//==========================================================================
// 解放
void CSceneManager::Uninit(void)
{
	if (m_pScene != nullptr)
	{
		m_pScene->Uninit();
		delete[] m_pScene;
		m_pScene = nullptr;
	}
}

//==========================================================================
// 更新
void CSceneManager::Update(void)
{
	m_pScene->Update();
}

//==========================================================================
// 描画
void CSceneManager::Draw(void)
{
	m_pScene->Draw();
}