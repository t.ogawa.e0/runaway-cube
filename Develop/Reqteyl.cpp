//==========================================================================
// レクティル[Reqteyl.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "Reqteyl.h"

//==========================================================================
// 実体
//==========================================================================
C2DPolygon *CReqteyl::m_ReqteylData; // レクティルの画像
C2DObject *CReqteyl::m_ReqteylPos; // レクティルの座標

CReqteyl::CReqteyl()
{
}

CReqteyl::~CReqteyl()
{
}

//==========================================================================
// 初期化
bool CReqteyl::Init(void)
{
	const char *pTexture2D[] =
	{
		"resource/texture/Reqteyl.png",
	};

	// メモリ確保
	New(m_ReqteylData);
	New(m_ReqteylPos);

	// レクティル座標初期化
	m_ReqteylPos->Init(0);

	// 座標のセット
	m_ReqteylPos->SetPos((float)CDirectXDevice::GetWindowsSize().m_Width / 2, (float)CDirectXDevice::GetWindowsSize().m_Height / 2);
	m_ReqteylPos->SetCentralCoordinatesMood(true);

	// 画像確保
	return m_ReqteylData->Init(pTexture2D, (int)Sizeof(pTexture2D));
}

//==========================================================================
// 解放
void CReqteyl::Uninit(void)
{
	// データ解放
	m_ReqteylData->Uninit();

	// メモリ
	Delete(m_ReqteylData);
	Delete(m_ReqteylPos);
}

//==========================================================================
// 更新
void CReqteyl::Update(void)
{
}

//==========================================================================
// 描画
void CReqteyl::Draw(bool * Key)
{
	if ((*Key) == true)
	{
		m_ReqteylData->Draw(m_ReqteylPos);
	}
}
