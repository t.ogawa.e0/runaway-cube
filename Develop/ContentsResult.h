//==========================================================================
// ランクリスト[ContentsResult.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _ContentsResult_H_
#define _ContentsResult_H_

//==========================================================================
//
// class  : CContentsResult
// Content: ランクリスト
//
//==========================================================================
class CContentsResult : private CTemplate
{
public:
	CContentsResult();
	~CContentsResult();
	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static bool Update(void);
	// 描画
	static void Draw(void);
private:
	static bool m_EndKey; // 終了キー
	static int m_DrawSerect; // 描画セレクト
	static C2DObject * m_Pos; // 座標
	static C2DPolygon * m_poly; // ポリゴン
	static int m_NumData; // データ数
	static CSound * m_Sound; // サウンド
};

#endif // !_ContentsResult_H_
