//==========================================================================
// 時間[Time.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Time_H_
#define _Time_H_

//==========================================================================
//
// class  : CTime
// Content: 時間
//
//==========================================================================
class CTime : private CTemplate
{
public:
	CTime();
	~CTime();
	// 初期化
	static bool Init(int time);
	// 解放
	static void Uninit(void);
	// 更新
	static void Update(void);
	// 描画
	static void Draw(void);

	static CTimer* GetTime(void) { return m_Time; }

	static int GetResultTime(void) { return m_GetTime; }

	static bool TimeUpKey(void) { return m_TimeUpKey; }
private:
	static int m_GetTime; // 時間記憶
	static bool m_TimeUpKey;
	static C2DObject *m_TimeFontPos; // 座標
	static C2DObject *m_NumBerPos; // 座標
	static C2DPolygon *m_TimeFontPoly; // ポリゴン
	static CTimer *m_Time; // タイム
	static CNumber * m_NumBer; // 数字の処理
};

#endif // !_Time_H_
