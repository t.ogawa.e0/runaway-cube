//==========================================================================
// �^�C�g���̖��O[TitleName.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "TitleName.h"

//==========================================================================
// ����
//==========================================================================
C2DObject * CTitleName::m_Pos; // ���W
C2DPolygon * CTitleName::m_Poly; // �|���S��
int * CTitleName::m_NumFont;
bool * CTitleName::m_��Change;
int * CTitleName::m_��Count;
int * CTitleName::m_��CountLimit;
bool * CTitleName::m_Input;
bool * CTitleName::m_Key;
CSound * CTitleName::m_Sound; // �T�E���h

CTitleName::CTitleName()
{
}

CTitleName::~CTitleName()
{
}

//==========================================================================
// ������
bool CTitleName::Init(void)
{
	const char * pTexLink[] =
	{
		"resource/texture/RunawayCube.png",
		"resource/texture/PressEnter�t�H���g.png"
	};

	CSound::SoundLabel pSundList[] =
	{
		{ "resource/sound/RPG_SE[232].wav",0,1.0f }
	};

	New(m_Pos, (int)Sizeof(pTexLink));
	New(m_Poly);
	New(m_NumFont);
	New(m_��Change);
	New(m_��Count);
	New(m_��CountLimit);
	New(m_Input);
	New(m_Key);
	New(m_Sound);

	(*m_NumFont) = (int)Sizeof(pTexLink);

	for (int i = 0; i < (*m_NumFont); i++)
	{
		m_Pos[i].Init(i);
	}

	m_Pos[0].SetCentralCoordinatesMood(true);
	m_Pos[0].SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
	m_Pos[0].SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 3));

	m_Pos[1].SetCentralCoordinatesMood(true);
	m_Pos[1].SetColor(255, 255, 0, 255);
	m_Pos[1].SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
	m_Pos[1].SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 1.1f));

	(*m_Input) = false;
	(*m_��Change) = true;
	(*m_Key) = false;
	(*m_��Count) = 0;
	(*m_��CountLimit) = 60;

	if (m_Sound->Init(pSundList, Sizeof(pSundList)))
	{
		return true;
	}

	return m_Poly->Init(pTexLink, (*m_NumFont));
}

//==========================================================================
// ���
void CTitleName::Uninit(void)
{
	m_Poly->Uninit();
	m_Sound->Uninit();

	Delete(m_Pos);
	Delete(m_Poly);
	Delete(m_NumFont);
	Delete(m_��Change);
	Delete(m_��Count);
	Delete(m_��CountLimit);
	Delete(m_Input);
	Delete(m_Key);
	Delete(m_Sound);
}

//==========================================================================
// �X�V
bool CTitleName::Update(void)
{
	if ((*m_��Change))
	{
		m_Pos[1].SetColor(255, 255, 0, 255);
	}
	else
	{
		m_Pos[1].SetColor(255, 255, 0, 0);
	}

	if ((*m_��CountLimit) <= (*m_��Count))
	{
		(*m_��Count) = 0;
		Bool(m_��Change);
	}

	(*m_��Count)++;

	if ((CMouse::Trigger(CMouse::ButtonKey::Left) || CMouse::Trigger(CMouse::ButtonKey::Right)) && !(*m_Input))
	{
		m_Sound->Play(0);
		(*m_��CountLimit) = 5;
		(*m_Input) = true;
		(*m_Key) = true;
	}

	if (CKeyboard::Trigger(CKeyboard::KeyList::KEY_RETURN) && !(*m_Input))
	{
		m_Sound->Play(0);
		(*m_��CountLimit) = 5;
		(*m_Input) = true;
		(*m_Key) = true;
	}

	if ((CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton1)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton2)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton3)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton4)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::L1Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::R1Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::L2Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::R2Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::SHAREButton)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::SOPTIONSButton)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::L3Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::R3Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::PSButton)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::TouchPad)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUp)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUpRight)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonRight)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUnderRight)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUnder)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUnderLeft)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonLeft)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUpLeft)
		) && !(*m_Input))
	{
		m_Sound->Play(0);
		(*m_��CountLimit) = 5;
		(*m_Input) = true;
		(*m_Key) = true;

	}

	return (*m_Key);
}

//==========================================================================
// �`��
void CTitleName::Draw(void)
{
	for (int i = 0; i < (*m_NumFont); i++)
	{
		m_Poly->Draw(&m_Pos[i]);
	}
}
