//==========================================================================
// 時間[Time.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "Time.h"

//==========================================================================
// 実体
//==========================================================================
bool CTime::m_TimeUpKey;
C2DObject *CTime::m_TimeFontPos; // 座標
C2DPolygon *CTime::m_TimeFontPoly; // ポリゴン
CNumber * CTime::m_NumBer; // 数字の処理
C2DObject *CTime::m_NumBerPos; // 座標
CTimer *CTime::m_Time; // タイム
int CTime::m_GetTime; // 時間記憶

CTime::CTime()
{
}

CTime::~CTime()
{
}

//==========================================================================
// 初期化
bool CTime::Init(int time)
{
	const char *pTexture[] =
	{
		"resource/texture/TimeFont.png",
		"resource/texture/Number.png",
	};

	// タイマーフォント
	New(m_TimeFontPos);
	New(m_TimeFontPoly);

	if (m_TimeFontPoly->Init(pTexture, Sizeof(pTexture))) { return true; }
	m_TimeFontPos->Init(0);
	m_TimeFontPos->Scale(-0.8f);

	// 数字フォント
	New(m_Time);
	New(m_NumBer);
	New(m_NumBerPos);

	m_NumBerPos->Init(1, 1, 10, 10);
	m_NumBer->Init(6, true, false);
	m_NumBerPos->Scale(-0.7f);
	m_NumBerPos->SetPos((float)m_TimeFontPoly->GetTexPalam(m_TimeFontPos->m_ID)->Widht*0.7f, 10.0f);

	m_Time->Init(time, 60);
	m_GetTime = m_Time->GetTime();

	m_TimeUpKey = false;

	return false;
}

//==========================================================================
// 解放
void CTime::Uninit(void)
{
	m_TimeFontPoly->Uninit();

	Delete(m_TimeFontPos);
	Delete(m_TimeFontPoly);
	Delete(m_Time);
	Delete(m_NumBer);
	Delete(m_NumBerPos);
}

//==========================================================================
// 更新
void CTime::Update(void)
{
	if (m_Time->Countdown())
	{
		m_TimeUpKey = true;
	}
	m_GetTime = m_Time->GetTime();
}

//==========================================================================
// 描画
void CTime::Draw(void)
{
	m_TimeFontPoly->Draw(m_TimeFontPos);
	m_NumBer->Draw(m_TimeFontPoly, m_NumBerPos, m_Time->GetTime());
}
