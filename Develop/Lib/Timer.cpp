//==========================================================================
// タイマー[Timer.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Timer.h"

CTimer::CTimer()
{
}

CTimer::~CTimer()
{
}

//==========================================================================
// 初期化
void CTimer::Init(int Time, int Comma)
{
	this->m_Time.Set(Time, 0);
	this->m_Comma.Set(Comma, 0);
}

//==========================================================================
// カウントダウン処理
bool CTimer::Countdown(void)
{
	if ((this->m_Time.m_Count != 0) || (this->m_Comma.m_Count != 0))
	{
		if (this->m_Time.m_Limit <= this->m_Time.m_Count)
		{
			if (this->m_Comma.m_Count <= this->m_Comma.m_Limit)
			{
				this->m_Time.m_Count--;
				this->m_Comma.m_Count = this->m_Comma.m_Defalt;
			}
			this->m_Comma.m_Count--;
		}
	}
	else if((this->m_Time.m_Count == 0) && (this->m_Comma.m_Count == 0))
	{
		return true;
	}

	return false;
}

//==========================================================================
// カウント処理
void CTimer::Count(void)
{
	this->m_Comma.m_Count++;
	if (this->m_Comma.m_Limit <= this->m_Comma.m_Count)
	{
		this->m_Time.m_Count++;
		this->m_Comma.m_Count = 0;
	}
}

//==========================================================================
// セット
void CTimer::List::Set(int Count, int Limit)
{
	if (Count == 0)
	{
		this->m_Count = Count;
		this->m_Limit = 60;
	}
	else
	{
		this->m_Count = Count;
		this->m_Limit = Limit;
	}
	this->m_Defalt = this->m_Count;
}
