//==========================================================================
// 2Dポリゴン[2DPolygon.cpp]
// author: tatuya ogawa
//==========================================================================
#include "2DPolygon.h"
#include "DXDevice.h"

//==========================================================================
// マクロ定義
//==========================================================================
#ifndef ErrorMessage
#define ErrorMessage(p,c) MessageBox((p), (c), "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING)
#endif // !ErrorMessage

C2DPolygon::C2DPolygon()
{
}

C2DPolygon::~C2DPolygon()
{
}

//==========================================================================
// 初期化 失敗時true
bool C2DPolygon::Init(const char ** Input, int NumData)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();

	this->m_Texture = nullptr;
	this->m_NumTexture = NumData;
	this->New(this->m_Texture, this->m_NumTexture);
	this->m_pVertexBuffer = nullptr;

	for (int i = 0; i < this->m_NumTexture; i++)
	{
		D3DXIMAGE_INFO dil; // 画像データの管理

		this->m_Texture[i].Texture = nullptr;
		if (FAILED(D3DXCreateTextureFromFile(pDevice, Input[i], &this->m_Texture[i].Texture)))
		{
			ErrorMessage(nullptr, "テクスチャの読み込み失敗");
			return true;
		}

		// 画像データの格納
		D3DXGetImageInfoFromFile(Input[i], &dil);
		this->m_Texture[i].Widht = dil.Width;
		this->m_Texture[i].Height = dil.Height;
	}

	if (FAILED(pDevice->CreateVertexBuffer(sizeof(VERTEX_3) * 4, D3DUSAGE_WRITEONLY, FVF_VERTEX_3, D3DPOOL_MANAGED, &this->m_pVertexBuffer, nullptr)))
	{
		ErrorMessage(nullptr, "頂点バッファが作れませんでした。");
		return true;
	}

	return false;
}

//==========================================================================
// 解放
void C2DPolygon::Uninit(void)
{
	this->Release(this->m_pVertexBuffer);
	for (int i = 0; i < this->m_NumTexture; i++)
	{
		this->Release(this->m_Texture[i].Texture);
	}
	this->Delete(this->m_Texture);
	this->m_NumTexture = 0;
}

//==========================================================================
// 描画
void C2DPolygon::Draw(C2DObject * Input, bool ADD)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	VERTEX_3* pPseudo = nullptr;

	this->m_pVertexBuffer->Lock(0, sizeof(VERTEX_3) * 4, (void**)&pPseudo, D3DLOCK_DISCARD);
	Input->CreateVertexAngle(pPseudo, &this->m_Texture[Input->m_ID].Widht, &this->m_Texture[Input->m_ID].Height);
	Input->CreateVertex(pPseudo, &this->m_Texture[Input->m_ID].Widht, &this->m_Texture[Input->m_ID].Height);
	this->m_pVertexBuffer->Unlock();	// ロック解除

	if (ADD == true)
	{
		this->SetRenderSUB(pDevice);
	}

	pDevice->SetStreamSource(0, this->m_pVertexBuffer, 0, sizeof(VERTEX_3));

	pDevice->SetFVF(FVF_VERTEX_3);

	pDevice->SetTexture(0, nullptr);
	pDevice->SetTexture(0, this->m_Texture[Input->m_ID].Texture);

	this->SetRenderZENABLE_START(pDevice);

	pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

	if (ADD == true)
	{
		this->SetRenderADD(pDevice);
	}

	this->SetRenderZENABLE_END(pDevice);
}
