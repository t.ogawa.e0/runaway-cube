//==========================================================================
// 2Dオブジェクト[2DObject.cpp]
// author: tatuya ogawa
//==========================================================================
#include "2DObject.h"

C2DObject::C2DObject()
{
}

C2DObject::~C2DObject()
{
}

//==========================================================================
// 初期化
void C2DObject::Init(int TexNumber)
{
	this->m_Affine.Angle = 0.0f;
	this->m_Affine.Key = false;
	this->m_Anim.Key = false;
	this->m_Anim.Count = -1;
	this->m_Anim.Direction = this->m_Anim.Frame = this->m_Anim.Pattern = 0;
	this->m_Color.g = this->m_Color.r = this->m_Color.b = this->m_Color.a = 255;
	this->m_Cut.x = this->m_Cut.y = this->m_Cut.w = this->m_Cut.h = 0.0f;
	this->m_ID = TexNumber;
	this->m_Pos.x = this->m_Pos.y = 0.0f;
	this->m_Scale = 1.0f;
	this->m_Key = false;
	this->m_SetAnimationCount = false;
	this->m_CentralCoordinates = false;
}

//==========================================================================
// アニメーション用初期化
void C2DObject::Init(int TexNumber, int Frame, int Pattern, int Direction)
{
	this->m_Affine.Angle = 0.0f;
	this->m_Affine.Key = false;
	this->m_Anim.Key = true;
	this->m_Anim.Count = -1;
	this->m_Anim.Direction = Direction;
	this->m_Anim.Frame = Frame;
	this->m_Anim.Pattern = Pattern;
	this->m_Color.g = this->m_Color.r = this->m_Color.b = this->m_Color.a = 255;
	this->m_Cut.x = this->m_Cut.y = this->m_Cut.w = this->m_Cut.h = 0.0f;
	this->m_ID = TexNumber;
	this->m_Pos.x = this->m_Pos.y = 0.0f;
	this->m_Scale = 1.0f;
	this->m_Key = false;
	this->m_SetAnimationCount = false;
	this->m_CentralCoordinates = false;
}

//==========================================================================
// 座標
void C2DObject::SetPos(float x, float y)
{
	this->m_Pos.x = x;
	this->m_Pos.y = y;
}

void C2DObject::SetX(float x)
{
	this->m_Pos.x = x;
}

void C2DObject::SetY(float y)
{
	this->m_Pos.y = y;
}

void C2DObject::SetXPlus(float x)
{
	this->m_Pos.x += x;
}

void C2DObject::SetYPlus(float y)
{
	this->m_Pos.y += y;
}

//==========================================================================
// サイズ
void C2DObject::Scale(float Size)
{
	this->m_Scale += Size;
}

//==========================================================================
// 回転角度
void C2DObject::Angle(float Angle)
{
	this->m_Affine.Key = true;
	this->m_Affine.Angle += Angle;
}

//==========================================================================
// 色セット
void C2DObject::SetColor(int r, int g, int b, int a)
{
	this->m_Color.r = r;
	this->m_Color.g = g;
	this->m_Color.b = b;
	this->m_Color.a = a;
}

//==========================================================================
// 無回転バーテックス
C2DObject::VERTEX_3 * C2DObject::CreateVertex(VERTEX_3 * pPseudo, int * Widht, int * Height)
{
	// 無回転の場合
	if (this->m_Affine.Key == false)
	{
		CUV UV;

		this->SetTexturCut(Widht, Height);
		this->AnimationPalam();
		this->UV(&UV, Widht, Height);

		// 中心座標モード
		if (this->m_CentralCoordinates)
		{
			float fWidht = (this->m_Cut.w / 2)*this->m_Scale;
			float fHeight = (this->m_Cut.h / 2)*this->m_Scale;

			pPseudo[0].pos = D3DXVECTOR4((this->m_Pos.x - fWidht - 0.5f), (this->m_Pos.y - fHeight - 0.5f), 1.0f, 1.0f);
			pPseudo[1].pos = D3DXVECTOR4((this->m_Pos.x + fWidht - 0.5f), (this->m_Pos.y - fHeight - 0.5f), 1.0f, 1.0f);
			pPseudo[2].pos = D3DXVECTOR4((this->m_Pos.x - fWidht - 0.5f), (this->m_Pos.y + fHeight - 0.5f), 1.0f, 1.0f);
			pPseudo[3].pos = D3DXVECTOR4((this->m_Pos.x + fWidht - 0.5f), (this->m_Pos.y + fHeight - 0.5f), 1.0f, 1.0f);
		}
		else // 中心座標モード無効時
		{
			if (this->m_Anim.Key)
			{
				pPseudo[0].pos = D3DXVECTOR4((this->m_Pos.x + 0.0f - 0.5f)*this->m_Scale, (this->m_Pos.y + 0.0f - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[1].pos = D3DXVECTOR4((this->m_Pos.x + this->m_Cut.w - 0.5f)*this->m_Scale, (this->m_Pos.y + 0.0f - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[2].pos = D3DXVECTOR4((this->m_Pos.x + 0.0f - 0.5f)*this->m_Scale, (this->m_Pos.y + this->m_Cut.h - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[3].pos = D3DXVECTOR4((this->m_Pos.x + this->m_Cut.w - 0.5f)*this->m_Scale, (this->m_Pos.y + this->m_Cut.h - 0.5f)*this->m_Scale, 1.0f, 1.0f);
			}
			else
			{
				pPseudo[0].pos = D3DXVECTOR4((this->m_Pos.x - 0.0f - 0.5f)*this->m_Scale, (this->m_Pos.y - 0.0f - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[1].pos = D3DXVECTOR4((this->m_Pos.x + (*Widht) - 0.5f)*this->m_Scale, (this->m_Pos.y - 0.0f - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[2].pos = D3DXVECTOR4((this->m_Pos.x - 0.0f - 0.5f)*this->m_Scale, (this->m_Pos.y + (*Height) - 0.5f)*this->m_Scale, 1.0f, 1.0f);
				pPseudo[3].pos = D3DXVECTOR4((this->m_Pos.x + (*Widht) - 0.5f)*this->m_Scale, (this->m_Pos.y + (*Height) - 0.5f)*this->m_Scale, 1.0f, 1.0f);
			}
		}

		pPseudo[0].color = pPseudo[1].color = pPseudo[2].color = pPseudo[3].color = D3DCOLOR_RGBA(this->m_Color.r, this->m_Color.g, this->m_Color.b, this->m_Color.a);

		pPseudo[0].Tex = D3DXVECTOR2(UV.u0, UV.v0);
		pPseudo[1].Tex = D3DXVECTOR2(UV.u1, UV.v0);
		pPseudo[2].Tex = D3DXVECTOR2(UV.u0, UV.v1);
		pPseudo[3].Tex = D3DXVECTOR2(UV.u1, UV.v1);
	}

	return pPseudo;
}

//==========================================================================
// 回転バーテックス
C2DObject::VERTEX_3 * C2DObject::CreateVertexAngle(VERTEX_3 * pPseudo, int * Widht, int * Height)
{
	// 回転時
	if (this->m_Affine.Key == true)
	{
		CUV UV;

		this->SetTexturCut(Widht, Height);
		this->AnimationPalam();
		this->UV(&UV, Widht, Height);

		float PosX = this->m_Pos.x;
		float PosY = this->m_Pos.y;

		// 中心座標割り出し
		float APolyX = (-(this->m_Cut.w / 2)* this->m_Scale) + (this->m_Cut.w / 2);
		float APolyX_W = ((this->m_Cut.w - (this->m_Cut.w / 2))*  this->m_Scale) + (this->m_Cut.w / 2);
		float APolyY = (-(this->m_Cut.h / 2)* this->m_Scale) + (this->m_Cut.h / 2);
		float APolyY_H = ((this->m_Cut.h - (this->m_Cut.h / 2))*  this->m_Scale) + (this->m_Cut.h / 2);

		// ずらした分修正
		APolyX -= (this->m_Cut.w / 2);
		APolyX_W -= (this->m_Cut.w / 2);
		APolyY -= (this->m_Cut.h / 2);
		APolyY_H -= (this->m_Cut.h / 2);

		// 中心座標モード無効時
		if (!this->m_CentralCoordinates)
		{
			PosX += (this->m_Cut.w / 2);
			PosY += (this->m_Cut.h / 2);
			PosX *= this->m_Scale;
			PosY *= this->m_Scale;
		}

		pPseudo[0].pos = D3DXVECTOR4((PosX + APolyX*  cosf(this->m_Affine.Angle) - APolyY* sinf(this->m_Affine.Angle) - 0.5f), PosY + APolyX* sinf(this->m_Affine.Angle) + APolyY* cosf(this->m_Affine.Angle) - 0.5f, 1.0f, 1.0f);
		pPseudo[1].pos = D3DXVECTOR4((PosX + APolyX_W* cosf(this->m_Affine.Angle) - APolyY*  sinf(this->m_Affine.Angle) - 0.5f), PosY + APolyX_W* sinf(this->m_Affine.Angle) + APolyY* cosf(this->m_Affine.Angle) - 0.5f, 1.0f, 1.0f);
		pPseudo[2].pos = D3DXVECTOR4((PosX + APolyX*  cosf(this->m_Affine.Angle) - APolyY_H* sinf(this->m_Affine.Angle) - 0.5f), PosY + APolyX*	sinf(this->m_Affine.Angle) + APolyY_H* cosf(this->m_Affine.Angle) - 0.5f, 1.0f, 1.0f);
		pPseudo[3].pos = D3DXVECTOR4((PosX + APolyX_W* cosf(this->m_Affine.Angle) - APolyY_H* sinf(this->m_Affine.Angle) - 0.5f), PosY + APolyX_W* sinf(this->m_Affine.Angle) + APolyY_H* cosf(this->m_Affine.Angle) - 0.5f, 1.0f, 1.0f);

		pPseudo[0].color = pPseudo[1].color = pPseudo[2].color = pPseudo[3].color = D3DCOLOR_RGBA(this->m_Color.r, this->m_Color.g, this->m_Color.b, this->m_Color.a);

		pPseudo[0].Tex = D3DXVECTOR2(UV.u0, UV.v0);
		pPseudo[1].Tex = D3DXVECTOR2(UV.u1, UV.v0);
		pPseudo[2].Tex = D3DXVECTOR2(UV.u0, UV.v1);
		pPseudo[3].Tex = D3DXVECTOR2(UV.u1, UV.v1);
	}

	return pPseudo;
}

//==========================================================================
// アニメーション切り替わり時の判定 切り替え時true 
bool C2DObject::GetPattanNum(void)
{
	if (this->m_Anim.Count == (this->m_Anim.Frame*this->m_Anim.Pattern) - 1)
	{
		this->m_Anim.Count = -1;
		return true;
	}
	return false;
}

//==========================================================================
// アニメーションカウンタのセット
void C2DObject::SetAnimationCount(int nCount)
{
	// アニメーションが有効時
	if (this->m_Anim.Key == true)
	{
		this->m_SetAnimationCount = true;
		this->m_Anim.Count = nCount;
	}
}

//==========================================================================
// UVの生成
void C2DObject::UV(CUV * UV, int * Widht, int * Height)
{
	// 左上
	UV->u0 = (float)this->m_Cut.x / *Widht;
	UV->v0 = (float)this->m_Cut.y / *Height;

	// 左下
	UV->u1 = (float)(this->m_Cut.x + this->m_Cut.w) / *Widht;
	UV->v1 = (float)(this->m_Cut.y + this->m_Cut.h) / *Height;
}

//==========================================================================
// アニメーションの情報
void C2DObject::AnimationPalam(void)
{
	// アニメーションが有効時
	if (this->m_Anim.Key == true)
	{
		if (this->m_SetAnimationCount)
		{
			int PattanNum = (this->m_Anim.Count / this->m_Anim.Frame) % this->m_Anim.Pattern;	// フレームに１回	パターン数
			int patternV = PattanNum % this->m_Anim.Direction; // 横方向のパターン
			int patternH = PattanNum / this->m_Anim.Direction; // 縦方向のパターン

			this->m_Cut.x = patternV * this->m_Cut.w; // 切り取り座標X
			this->m_Cut.y = patternH * this->m_Cut.h; // 切り取り座標Y
		}
		else
		{
			this->m_Anim.Count++;
			int PattanNum = (this->m_Anim.Count / this->m_Anim.Frame) % this->m_Anim.Pattern;	// フレームに１回	パターン数
			int patternV = PattanNum % this->m_Anim.Direction; // 横方向のパターン
			int patternH = PattanNum / this->m_Anim.Direction; // 縦方向のパターン

			this->m_Cut.x = patternV * this->m_Cut.w; // 切り取り座標X
			this->m_Cut.y = patternH * this->m_Cut.h; // 切り取り座標Y
		}
	}
}

//==========================================================================
// テクスチャの切り取り
void C2DObject::SetTexturCut(int * Widht, int * Height)
{
	if (this->m_Cut.w == 0.0f)
	{
		this->m_Cut.w = (float)*Widht;
	}
	if (this->m_Cut.h == 0.0f)
	{
		this->m_Cut.h = (float)*Height;
	}

	if (this->m_Key == false && this->m_Anim.Key == true)
	{
		int nCount = 0;
		this->m_Cut.w = (float)*Widht / this->m_Anim.Direction;
		for (int i = 0;; i += this->m_Anim.Direction)
		{
			if (this->m_Anim.Pattern <= i) { break; }
			nCount++;
		}
		this->m_Cut.h = (float)*Height / nCount;
		this->m_Cut.x = this->m_Cut.y = 0.0f;
		this->m_Key = true;
	}
}
