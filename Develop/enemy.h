//==========================================================================
// エネミー[enemy.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _ENEMY_H_
#define _ENEMY_H_

//==========================================================================
//
// class  : CEnemy
// Content: エネミー
//
//==========================================================================
class CEnemy : private CTemplate
{
private:
	static constexpr float m_Spped = 0.05f;
private:
	// AI
	class C_AI : private CTemplate
	{
	private:
		struct Mode
		{
			enum List
			{
				AmbushMode, // 待ち伏せモード
				TrackingMode, // 追跡モード
				AttackMood, // 攻撃モード
			};
		};

		struct Type
		{
			enum List
			{
				AutoAttack, // 自動攻撃タイプ
				Ambush, // 待ち伏せタイプ
				not, // 何もしない
			};
		};
	public:
		typedef Type::List TypeList;
	public: // 管理
		int m_ID; // 管理ID
		C_AI *m_pNext; // 次のアドレス
		C_AI *m_pBack; // 前のアドレス
	private:
		C3DObject m_Pos; // エネミー座標
		C3DObject m_OldPos; // エネミー古い座標
		C3DObject m_PosBox1; // ボックス座標1
		C3DObject m_PosBox2; // ボックス座標1
		C3DObject m_GunPos; // 銃の座標
		CCamera *m_Camera; // カメラ
		Mode::List m_ModeType; // モード
		TypeList m_Type; // タイプ
		float m_MoveSpped; // 移動速度
		float m_MoveY; // 縦移動
		float m_Magnification; // 倍率
		bool m_Existence; // 存在
		bool m_Hit; // 当たり判定
		bool m_Playback; // 出現
		bool m_Attak; // 攻撃
		bool m_Destruction; // 消滅
		int m_AttakFrame; // 攻撃間隔
		int m_MoveFlame; // 移動時間
		int m_backpattern; // backのパターン
	public:
		// 初期化
		void Init(C3DObject * pPos, TypeList type);
		// 解放
		void Uninit(void);
		// 更新
		void Update(void);
		// 描画
		void Draw(void);
		// ビューカメラのゲッター
		D3DXMATRIX *GetCamera(void);
		// データのげった
		C3DObject *GetData(void) { return &this->m_Pos; }
		// データのげった
		C3DObject *GetGunPos(void) { return &this->m_GunPos; }
		// データのげった
		C3DObject *GetOldPos(void) { return &this->m_OldPos; }
		bool GetDestruction(void) { return this->m_Destruction; }
		bool GetHit(void) { return this->m_Hit; }
		void SetDestruction(void) { this->m_Hit = true; }
		float GetSpped(void) { return this->m_MoveSpped; }
		int *Getbackpattern(void) { return &this->m_backpattern; }
	private:
		// モード
		void Mode(void);
		// AI
		void AI(void);
		// 判断
		void Judgment(void);
		// カメラ
		void Camera(void);
		// カメラの動く処理
		void MoveCamera(void);
		// 移動
		void Move(void);
		// 出現処理
		void Playback(void);
		// 消滅
		void Destruction(void);
		// エネミーの銃
		void EnemyGun(void);
	};
public:
	typedef C_AI::TypeList EnemyTypeList;
private:
	class CEnemyParamData
	{
	public:
		int m_Time; // 時間
		float m_x; // x座標
		float m_y; // y座標
		float m_z; // z座標
		EnemyTypeList m_Type; // タイプ
	};
public:
	CEnemy();
	~CEnemy();
	// 初期化
	static bool Init(const char * pCSVFile);
	// 解放
	static void Uninit(void);
	// 更新
	static void Update(void);
	// 描画
	static void Draw(void);
	// 生成
	static void Create(int time);
	// ビューカメラのゲッター
	static D3DXMATRIX *GetCamera(void);
	// 銃データのゲッター
	static CGun* GetGunData(void) { return m_enemygun; }
	// ゲッター データがない場合null
	static C_AI *GetData(const int *Count);
	// 特定のデータ消去
	static void PinpointDelete(const int * ID);
	// エネミーの数
	static int LimitNumEnemy(void) { return m_LimitEnemyParamDataCount; }
	// エネミーの数
	static int GetDesEnemyCount(void) { return m_DesEnemyCount; }
private:
	// 消去
	static void PinpointDelete(void);
	// csvファイル読み込み
	static bool CSVFileOpen(const char * pCSVFile);
private:
	static C_AI * m_pStartAddress; // 先頭アドレス
	static CCube * m_Data; // キューブのデータ
	static CXmodel * m_MdelData; // 核
	static CShadow *m_Shadow; // 影
	static CGun * m_enemygun; // エネミーの銃
	static int m_NumData; // データ数
	static int m_IDCount; // IDカウンタ
	static int m_EnemyParamDataCount; // エネミーのカウンタ
	static int m_DesEnemyCount; // 消えたエネミーのcount
	static int m_LimitEnemyParamDataCount; // エネミーの数
	static CEnemyParamData *m_ParamData; // パラメーター格納
};

#endif // !_ENEMY_H_
