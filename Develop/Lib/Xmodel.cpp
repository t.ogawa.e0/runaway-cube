//==========================================================================
// Xモデル[Xmodel.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Xmodel.h"
#include "DXDevice.h"

//==========================================================================
// マクロ定義
//==========================================================================
#ifndef ErrorMessage
#define ErrorMessage(p,c) MessageBox((p), (c), "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING)
#endif // !ErrorMessage

CXmodel::CXmodel()
{
}

CXmodel::~CXmodel()
{
}

//==========================================================================
// 初期化 Excel 失敗時true
bool CXmodel::Init(const char* pFile, LightMoad mode)
{
	this->m_MaterialMood = mode;

	// ファイルオープン
	if (this->OpenFile(pFile, this->m_Link, &this->m_NumData)) { return true; }

	return this->InitXModel();
}

//==========================================================================
// 初期化 失敗時true
bool CXmodel::Init(const char ** pFile, int Size, LightMoad mode)
{
	this->m_MaterialMood = mode;
	this->m_NumData = Size;

	// リンク格納
	this->InputLink(pFile);

	return this->InitXModel();
}

//==========================================================================
// Xモデルの初期化
bool CXmodel::InitXModel(void)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	LPD3DXBUFFER pAdjacency; // 隣接情報
	LPD3DXMESH pTempMesh; // テンプレートメッシュ
	LPD3DXBUFFER pMaterialBuffer; // マテリアルバッファ
	LINKLIST *lTexList; // リンク

	this->New(this->m_MdelData, this->m_NumData); // メモリ確保

	// データの数だけ
	for (int i = 0; i < this->m_NumData; i++)
	{
		pAdjacency = nullptr;
		pTempMesh = nullptr;
		pMaterialBuffer = nullptr;
		lTexList = nullptr;

		D3DVERTEXELEMENT9 pElements[MAXD3DDECLLENGTH + 1];

		if (!this->OverlapModel(&i))
		{
			this->m_MdelData[i].ModelID = i; // IDの記録

			// 読み込み処理
			if (this->Read(&pDevice, &pAdjacency, &pMaterialBuffer, &i)) { return true; }

			// モデルデータの最適化
			if (this->Optimisation(pAdjacency, &i)) { return true; }
			this->m_MdelData[i].pMesh->GetDeclaration(pElements);

			// データの複製
			if (this->Replication(pElements, &pDevice, &pTempMesh, &i)) { return true; }

			this->New(this->m_MdelData[i].ID, this->m_MdelData[i].NumMaterial); // テクスチャIDの領域の確保

			// テクスチャのリンク読み込み
			this->LoadTextureLink((LPD3DXMATERIAL)pMaterialBuffer->GetBufferPointer(), lTexList, "resource/texture/white.jpg", &i);

			this->New(this->m_MdelData[i].pMatMesh, this->m_MdelData[i].NumMaterial); // メッシュ情報を確保

			// マテリアルの設定
			this->MaterialSetting((LPD3DXMATERIAL)pMaterialBuffer->GetBufferPointer(), &i);

			this->m_MdelData[i].pMeshTex = nullptr;

			// テクスチャが存在するかどうかのチェック
			if (this->ExistenceTex(&i) == true)
			{
				this->New(this->m_MdelData[i].pMeshTex, this->m_MdelData[i].NumTex);

				// テクスチャの読み込み
				if (this->LoadTexture(lTexList, &pDevice, &i)) { return true; }
			}

			this->Delete(lTexList);
			this->Release(pMaterialBuffer);
			this->Release(this->m_MdelData[i].pMesh);
			this->Release(pAdjacency);
			this->m_MdelData[i].pMesh = pTempMesh;
		}
	}

	this->Delete(this->m_Link);

	return false;
}

//==========================================================================
// リンク入力
void CXmodel::InputLink(const char ** pInput)
{
	this->m_Link = nullptr;
	this->New(this->m_Link, this->m_NumData);

	for (int i = 0; i < this->m_NumData; i++)
	{
		this->m_Link[i].m_Data[0] = 0;
		strcpy(this->m_Link[i].m_Data, pInput[i]);
	}
}

//==========================================================================
// 解放
void CXmodel::Uninit(void)
{
	// モデル管理用データ配列がnullではない時に
	if (this->m_MdelData != nullptr)
	{
		// データの数だけ
		for (int i = 0; i < this->m_NumData; i++)
		{
			// モデルデータとIDが一致したとき
			if (this->m_MdelData[i].ModelID == i)
			{
				// テクスチャがnullではない時に
				if (this->m_MdelData[i].pMeshTex != nullptr)
				{
					// テクスチャの枚数分
					for (int nNumTex = 0; nNumTex < this->m_MdelData[i].NumTex; nNumTex++)
					{
						// テクスチャ解放
						this->Release(this->m_MdelData[i].pMeshTex[nNumTex]);
					}
					this->Delete(this->m_MdelData[i].pMeshTex);
				}
				this->Delete(this->m_MdelData[i].ID);
				this->Delete(this->m_MdelData[i].pMatMesh);
				this->Release(this->m_MdelData[i].pMesh);
				this->Delete(this->m_MdelData[i].ExistenceTexture);
			}
			this->m_MdelData[i].NumMaterial = 0;
			this->m_MdelData[i].NumTex = 0;
		}
		this->Delete(this->m_MdelData);
		this->m_MdelData = nullptr;
		this->m_NumData = 0;
	}
}

//==========================================================================
// 描画
void CXmodel::Draw(C3DObject * Poly)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	D3DXMATRIX aMtxWorld; // ワールド行列

	if (this->m_MdelData != nullptr)
	{
		pDevice->SetTransform(D3DTS_WORLD, Poly->CreateMtxWorld1(&aMtxWorld));

		pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);// ライト設定

		pDevice->SetFVF(this->m_MdelData[Poly->ID].pMesh->GetFVF());
		for (int i = 0; i < (int)this->m_MdelData[Poly->ID].NumMaterial; i++)
		{
			// マテリアル情報をセット
			pDevice->SetMaterial(&this->m_MdelData[Poly->ID].pMatMesh[i]);

			// テクスチャが存在するときにセット
			pDevice->SetTexture(0, nullptr);
			if (this->m_MdelData[Poly->ID].ExistenceTexture[i] == true)
			{
				// テクスチャ情報をセット
				pDevice->SetTexture(0, this->m_MdelData[Poly->ID].pMeshTex[this->m_MdelData[Poly->ID].ID[i]]);
			}
			// メッシュを描画
			this->m_MdelData[Poly->ID].pMesh->DrawSubset(i);
		}
	}
}

//==========================================================================
// モデルデーターのチェック
bool CXmodel::OverlapModel(int* Num)
{
	for (int i = 0; i < (*Num); i++)
	{
		// リンクの内容が同じ場合
		if (!strcmp(this->m_Link[(*Num)].m_Data, this->m_Link[i].m_Data))
		{
			this->m_MdelData[(*Num)] = this->m_MdelData[i];
			return true;
		}
	}
	return false;
}

//==========================================================================
// テクスチャのファイルパス読み込み
void CXmodel::LoadTextureLink(const LPD3DXMATERIAL Input, LINKLIST *&lTexList, const char* pPas, int* Num)
{
	bool bStartSet = false;
	int nNumID = 0;
	LINKLIST *lSetList = nullptr;

	this->m_MdelData[*Num].NumTex = 1;
	this->New(lSetList, this->m_MdelData[*Num].NumTex);
	lSetList[0].Link = (LPSTR)pPas;
	lTexList = lSetList;

	// テクスチャの有無のチェック
	this->New(this->m_MdelData[*Num].ExistenceTexture, (int)this->m_MdelData[*Num].NumMaterial);

	// マテリアルの数だけ
	for (int i = 0; i < (int)this->m_MdelData[*Num].NumMaterial; i++)
	{
		bool bNauLink = false;
		this->m_MdelData[*Num].ExistenceTexture[i] = false;

		// 使用しているテクスチャがあれば読み込む
		if (Input[i].pTextureFilename != nullptr &&lstrlen(Input[i].pTextureFilename) > 0)
		{
			this->m_MdelData[*Num].ExistenceTexture[i] = true;
			for (nNumID = 0; nNumID < this->m_MdelData[*Num].NumTex; nNumID++)
			{
				// 同じとき
				if (!strcmp(Input[i].pTextureFilename, lTexList[nNumID].Link))
				{
					this->m_MdelData[*Num].ID[i] = nNumID;
					bNauLink = true;
				}
			}

			if (bNauLink == false && bStartSet == true)
			{
				this->m_MdelData[*Num].NumTex++;
				lSetList = nullptr;
				this->New(lSetList, this->m_MdelData[*Num].NumTex);

				// データ移植
				for (int nSet = 0; nSet < (this->m_MdelData[*Num].NumTex - 1); nSet++)
				{
					lSetList[nSet] = lTexList[nSet];
				}

				this->Delete(lTexList);

				// リンク流し込み
				lSetList[this->m_MdelData[*Num].NumTex - 1].Link = Input[i].pTextureFilename;
				lTexList = lSetList;
				this->m_MdelData[*Num].ID[i] = nNumID;
			}

			if (bStartSet == false)
			{
				this->m_MdelData[*Num].ID[i] = nNumID - 1;
				lTexList[0].Link = Input[i].pTextureFilename;
				bStartSet = true;
			}
		}
	}
}

//==========================================================================
// テクスチャの読み込み
bool CXmodel::LoadTexture(LINKLIST * lTexList, LPDIRECT3DDEVICE9 * pDevice, int* Num)
{
	for (int i = 0; i < this->m_MdelData[*Num].NumTex; i++)
	{
		this->m_MdelData[*Num].pMeshTex[i] = nullptr;	// テクスチャ初期化

		// 使用しているテクスチャがあれば読み込む
		if (lTexList[i].Link != nullptr &&lstrlen(lTexList[i].Link) > 0)
		{
			// テクスチャ読み込み
			if (FAILED(D3DXCreateTextureFromFile(*pDevice, lTexList[i].Link, &this->m_MdelData[*Num].pMeshTex[i])))
			{
				ErrorMessage(nullptr, "テクスチャの読み込みに失敗しました");
				return true;
			}
		}
	}
	return false;
}

//==========================================================================
// マテリアルの設定
void CXmodel::MaterialSetting(const LPD3DXMATERIAL Input, int* Num)
{
	for (int i = 0; i < (int)this->m_MdelData[*Num].NumMaterial; i++)
	{
		switch (this->m_MaterialMood)
		{
		case CXmodel::LightMoad::Auto:
			this->MaterialSettingAuto(&this->m_MdelData[*Num].pMatMesh[i], &Input[i].MatD3D, &this->m_MdelData[*Num].ExistenceTexture[i]);
			break;
		case CXmodel::LightMoad::System:
			this->MaterialSettingSystem(&this->m_MdelData[*Num].pMatMesh[i]);
			break;
		case CXmodel::LightMoad::Material:
			this->MaterialSettingMaterial(&this->m_MdelData[*Num].pMatMesh[i], &Input[i].MatD3D);
			break;
		default:
			this->MaterialSettingAuto(&this->m_MdelData[*Num].pMatMesh[i], &Input[i].MatD3D, &this->m_MdelData[*Num].ExistenceTexture[i]);
			break;
		}
	}
}

//==========================================================================
// マテリアルの設定 オートモード
void CXmodel::MaterialSettingAuto(D3DMATERIAL9 *pMatMesh, const D3DMATERIAL9 *Input, bool *ExistenceTexture)
{
	if (*ExistenceTexture == true)
	{
		this->MaterialSettingSystem(pMatMesh);
	}
	else if (*ExistenceTexture == false)
	{
		this->MaterialSettingMaterial(pMatMesh, Input);
	}
}

//==========================================================================
// マテリアルの設定 システム設定
void CXmodel::MaterialSettingSystem(D3DMATERIAL9 *pMatMesh)
{
	// マテリアルの設定
	ZeroMemory(pMatMesh, sizeof(*pMatMesh));
	pMatMesh->Diffuse.r = 1.0f; // ディレクショナルライト
	pMatMesh->Diffuse.g = 1.0f; // ディレクショナルライト
	pMatMesh->Diffuse.b = 1.0f; // ディレクショナルライト
	pMatMesh->Diffuse.a = 1.0f; // ディレクショナルライト
	pMatMesh->Ambient.r = 1.0f; // アンビエントライト
	pMatMesh->Ambient.g = 1.0f; // アンビエントライト
	pMatMesh->Ambient.b = 1.0f; // アンビエントライト
	pMatMesh->Ambient.a = 1.0f; // アンビエントライト
}

//==========================================================================
// マテリアルの設定 マテリアル素材設定
void CXmodel::MaterialSettingMaterial(D3DMATERIAL9 *pMatMesh, const D3DMATERIAL9 *Input)
{
	*pMatMesh = *Input;
}

//==========================================================================
// 読み込み
bool CXmodel::Read(LPDIRECT3DDEVICE9 *pDevice, LPD3DXBUFFER * pAdjacency, LPD3DXBUFFER *  pMaterialBuffer, int* Num)
{
	if (FAILED(D3DXLoadMeshFromX(this->m_Link->m_Data, D3DXMESH_SYSTEMMEM, *pDevice, pAdjacency, pMaterialBuffer, nullptr, &this->m_MdelData[*Num].NumMaterial, &this->m_MdelData[*Num].pMesh)))
	{
		ErrorMessage(nullptr, "Xデータが読み込めませんでした。");
		return true;
	}

	return false;
}

//==========================================================================
// 最適化
bool CXmodel::Optimisation(LPD3DXBUFFER pAdjacency, int* Num)
{
	if (FAILED(this->m_MdelData[*Num].pMesh->OptimizeInplace(D3DXMESHOPT_COMPACT | D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_VERTEXCACHE, (DWORD*)pAdjacency->GetBufferPointer(), nullptr, nullptr, nullptr)))
	{
		ErrorMessage(nullptr, "Xデータの最適化に失敗しました。");
		return true;
	}

	return false;
}

//==========================================================================
// 複製
bool CXmodel::Replication(D3DVERTEXELEMENT9 * pElements, LPDIRECT3DDEVICE9 *pDevice, LPD3DXMESH * pTempMesh, int* Num)
{
	if (FAILED(this->m_MdelData[*Num].pMesh->CloneMesh(D3DXMESH_MANAGED | D3DXMESH_WRITEONLY, pElements, *pDevice, pTempMesh)))
	{
		ErrorMessage(nullptr, "複製に失敗しました。");
		return true;
	}

	return false;
}

//==========================================================================
// 使用しているテクスチャが0の場合false 1枚でもあればtrue
bool CXmodel::ExistenceTex(int * Num)
{
	for (int i = 0; i < (int)this->m_MdelData[*Num].NumMaterial; i++)
	{
		if (this->m_MdelData[*Num].ExistenceTexture[i] == true)
		{
			return true;
			break;
		}
	}
	return false;
}
