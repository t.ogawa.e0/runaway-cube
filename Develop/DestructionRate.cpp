//==========================================================================
// 破壊率[DestructionRate.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "DestructionRate.h"

//==========================================================================
// 実体
//==========================================================================
int CDestructionRate::m_DrawCount; // 描画終了まで
float CDestructionRate::m_1Percentage; // 1%
float CDestructionRate::m_NowPercentage; // 今の%
C2DObject * CDestructionRate::m_Pos; // 座標
C2DPolygon * CDestructionRate::m_Poly; // ポリゴン
CNumber * CDestructionRate::m_Number; // 数字

CDestructionRate::CDestructionRate()
{
}

CDestructionRate::~CDestructionRate()
{
}

//==========================================================================
// 初期化
bool CDestructionRate::Init(int Numenemy)
{
	C2DObject * pPos = nullptr;
	const char * pTexData[] =
	{
		"resource/texture/敵排除率フォント.png",
		"resource/texture/Number.png",
		"resource/texture/%フォント.png",
	};

	m_DrawCount = m_DrawLimit;
	m_1Percentage = (float)(m_LimitPercentage / Numenemy);
	m_NowPercentage = 0.0f;
	New(m_Pos, Sizeof(pTexData));
	New(m_Poly);
	New(m_Number);

	if (m_Poly->Init(pTexData, Sizeof(pTexData))) { return true; }
	pPos = &m_Pos[0];
	pPos->Init(0);
	pPos->Scale(-0.7f);
	pPos->SetY((float)CDirectXDevice::GetWindowsSize().m_Height *0.5f);

	pPos = &m_Pos[1];
	pPos->Init(1, 1, 10, 10);
	pPos->Scale(-0.7f);
	pPos->SetY((float)CDirectXDevice::GetWindowsSize().m_Height *0.51f);
	pPos->SetX((float)m_Poly->GetTexPalam(m_Pos[0].m_ID)->Widht);

	pPos = &m_Pos[2];
	pPos->Init(2);
	pPos->Scale(-0.7f);
	pPos->SetY((float)CDirectXDevice::GetWindowsSize().m_Height *0.51f);

	m_Number->Init(3, true, false);

	return false;
}

//==========================================================================
// 解放
void CDestructionRate::Uninit(void)
{
	m_Poly->Uninit();
	m_Number->Uninit();

	Delete(m_Pos);
	Delete(m_Poly);
	Delete(m_Number);
}

//==========================================================================
// 更新
void CDestructionRate::Update(void)
{
	m_DrawCount++;
}

//==========================================================================
// 描画
void CDestructionRate::Draw(void)
{
	if (m_DrawCount <= m_DrawLimit)
	{
		float PosX = 0.0f;

		m_Poly->Draw(&m_Pos[0]);
		PosX = m_Number->Draw(m_Poly, &m_Pos[1], (int)(m_NowPercentage + 0.5f));

		m_Pos[2].SetX(PosX);
		m_Poly->Draw(&m_Pos[2]);
	}
}

//==========================================================================
// %カウンタ
void CDestructionRate::Count(void)
{
	m_DrawCount = 0;
	m_NowPercentage += m_1Percentage;
	if (m_LimitPercentage <= m_NowPercentage)
	{
		m_NowPercentage = m_LimitPercentage;
	}
}
