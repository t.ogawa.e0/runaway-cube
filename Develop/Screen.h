//==========================================================================
// スクリーン[Screen.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Screen_H_
#define _Screen_H_

//==========================================================================
// Include
//==========================================================================
#include "SceneChange.h"

//==========================================================================
//
// class  : CScreen
// Content: スクリーン
//
//==========================================================================
class CScreen
{
public:
	CScreen();
	~CScreen();

	// シーンの切り替え
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static bool Update(void);
	// 描画
	static void Draw(void);
private:
	// スクリーンの切り替え
	static bool Change(CSceneManager::SceneName Name);

	// 初期化start
	static bool StartInit(void);
	// フェード
	static void Fade(void);
private:
	static bool m_DebugKey;
	static bool m_Start;
	static bool m_Change; // 切り替えフラグ
	static int m_Count; // カウンタ
};

#endif // !_Screen_H_
