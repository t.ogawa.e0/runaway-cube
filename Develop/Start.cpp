//==========================================================================
// スタート演出[Start.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "Start.h"

//==========================================================================
// 実体
//==========================================================================
bool * CStartScene::m_CountdownFontDraw; // フォント1描画判定
C2DObject * CStartScene::m_CountdownFontPos; // 座標
C2DPolygon * CStartScene::m_CountdownFontPoly; // ポリゴン
int * CStartScene::m_CountdownFontMove; // 移動

 bool * CStartScene::m_CountDraw; // フォント2描画判定
CTimer * CStartScene::m_Timer; // タイマー

C2DObject * CStartScene::m_StartPos; // 座標
C2DPolygon * CStartScene::m_StartPoly; // ポリゴン 
bool * CStartScene::m_StartFontDraw; // フォント1描画判定
int * CStartScene::m_StartColor; // 色

bool * CStartScene::m_StartKey;

CStartScene::CStartScene()
{
}

CStartScene::~CStartScene()
{
}

//==========================================================================
// 初期化
bool CStartScene::Init(void)
{
	New(m_StartColor);
	New(m_StartPos);
	New(m_StartPoly);
	New(m_StartFontDraw);

	New(m_CountdownFontDraw);
	New(m_CountdownFontMove);
	New(m_CountdownFontPos);
	New(m_CountdownFontPoly);

	New(m_CountDraw);
	New(m_Timer);

	New(m_StartKey);

	{ // 数字
		m_Timer->Init(1, 30);
		(*m_CountdownFontDraw) = true;
		(*m_CountdownFontMove) = 0;
	}

	{ // countdown...
		const char * pTexList[] =
		{
			"resource/texture/Readyフォント.png",
		};
		m_CountdownFontPos->Init(0);
		m_CountdownFontPos->SetColor(255, 255, 0, 255);
		m_CountdownFontPos->SetCentralCoordinatesMood(true);
		m_CountdownFontPos->SetX(-(float)(CDirectXDevice::GetWindowsSize().m_Width));
		m_CountdownFontPos->SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 2));
		(*m_CountDraw) = false;
		if (m_CountdownFontPoly->Init(pTexList, Sizeof(pTexList))) { return true; }
	}

	{ // Start...
		const char * pTexList[] =
		{
			"resource/texture/GO!フォント.png",
		};
		(*m_StartFontDraw) = false;
		(*m_StartColor) = 255;
		m_StartPos->Init(0);
		m_StartPos->Scale(-1.0f);
		m_StartPos->SetColor(255, 255, 0, (*m_StartColor));
		m_StartPos->SetCentralCoordinatesMood(true);
		m_StartPos->SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
		m_StartPos->SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 2));
		if (m_StartPoly->Init(pTexList, Sizeof(pTexList))) { return true; }
	}

	(*m_StartKey) = false;

	return false;
}

//==========================================================================
// 解放
void CStartScene::Uninit(void)
{
	m_CountdownFontPoly->Uninit();
	m_StartPoly->Uninit();

	Delete(m_StartColor);
	Delete(m_StartPos);
	Delete(m_StartPoly);
	Delete(m_StartFontDraw);

	Delete(m_CountdownFontDraw);
	Delete(m_CountdownFontMove);
	Delete(m_CountdownFontPos);
	Delete(m_CountdownFontPoly);

	Delete(m_CountDraw);
	Delete(m_Timer);

	Delete(m_StartKey);
}

//==========================================================================
// 更新
bool CStartScene::Update(void)
{
	CountdownFontUpdate();
	StartUpdate();
	CountUpdate();

	return (*m_StartKey);
}

//==========================================================================
// 描画
void CStartScene::Draw(void)
{
	CountdownFontDraw();
	StartDraw();
}

//==========================================================================
// カウントの更新
void CStartScene::CountUpdate(void)
{
	if ((*m_CountDraw))
	{
		m_Timer->Countdown();
		if (m_Timer->GetTime() == 0)
		{
			(*m_CountDraw) = false;
			(*m_StartFontDraw) = true;
			(*m_StartKey) = true;
		}
	}
}

//==========================================================================
// フォントの更新
void CStartScene::CountdownFontUpdate(void)
{
	if ((*m_CountdownFontDraw) && (*m_CountdownFontMove) != 0)
	{
		(*m_CountdownFontMove)++;
		if (m_CountdownFontPos->GetPos()->x <= ((float)(CDirectXDevice::GetWindowsSize().m_Width / 2) + (float)(CDirectXDevice::GetWindowsSize().m_Width)))
		{
			if (60 <= (*m_CountdownFontMove))
			{
				m_CountdownFontPos->SetXPlus(30);
				(*m_CountDraw) = true;
			}
		}
		else
		{
			(*m_CountdownFontDraw) = false;
		}
	}

	if ((*m_CountdownFontDraw) && (*m_CountdownFontMove) == 0)
	{
		if (m_CountdownFontPos->GetPos()->x <= (float)(CDirectXDevice::GetWindowsSize().m_Width / 2))
		{
			m_CountdownFontPos->SetXPlus(30);
		}
		else
		{
			(*m_CountdownFontMove)++;
		}
	}
}

//==========================================================================
// フォントの描画
void CStartScene::CountdownFontDraw(void)
{
	if (*m_CountdownFontDraw)
	{
		m_CountdownFontPoly->Draw(m_CountdownFontPos);
	}
}

//==========================================================================
// 更新
void CStartScene::StartUpdate(void)
{
	if ((*m_StartFontDraw))
	{
		m_StartPos->Scale(0.05f);
		m_StartPos->SetColor(255, 255, 0, (*m_StartColor));
		(*m_StartColor) -= 5;
		if ((*m_StartColor) <= 0)
		{
			(*m_StartColor) = 0;
			(*m_StartFontDraw) = false;
		}
	}
}

//==========================================================================
// 描画
void CStartScene::StartDraw(void)
{
	if ((*m_StartFontDraw))
	{
		m_StartPoly->Draw(m_StartPos);
	}
}
