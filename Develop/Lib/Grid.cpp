//==========================================================================
// グリッド[Grid.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Grid.h"
#include "DXDevice.h"

//==========================================================================
// 実体
//==========================================================================
D3DXMATRIX *CGrid::m_aMtxWorld;
CGrid::VERTEX_2 *CGrid::m_vPos;

CGrid::CGrid()
{
}

CGrid::~CGrid()
{
}

//==========================================================================
// 初期化
void CGrid::Init(void)
{
	float X = 5;
	float Z = 5;

	m_vPos = nullptr;
	m_aMtxWorld = nullptr;

	New(m_vPos, nNumGrid);
	New(m_aMtxWorld);

	int nNumLine = (int)(nNumGrid / 2);

	for (int i = 0; i < nNumLine; i += 2, X--, Z--)
	{
		m_vPos[i].pos = D3DXVECTOR3(1.0f * X, 0.0f, 5.0f); // x座標に線
		m_vPos[i + 1].pos = D3DXVECTOR3(1.0f * X, 0.0f, -5.0f); // x座標に線
		m_vPos[nNumLine + i].pos = D3DXVECTOR3(5.0f, 0.0f, 1.0f * Z); // z座標に線
		m_vPos[nNumLine + i + 1].pos = D3DXVECTOR3(-5.0f, 0.0f, 1.0f * Z); // z座標に線

		m_vPos[i].color = D3DCOLOR_RGBA(255, 255, 255, 255);
		m_vPos[i + 1].color = D3DCOLOR_RGBA(255, 255, 255, 255);
		m_vPos[nNumLine + i].color = D3DCOLOR_RGBA(255, 255, 255, 255);
		m_vPos[nNumLine + i + 1].color = D3DCOLOR_RGBA(255, 255, 255, 255);

		if (i == 10)
		{
			m_vPos[i].color = D3DCOLOR_RGBA(255, 0, 0, 255);
			m_vPos[i + 1].color = D3DCOLOR_RGBA(255, 0, 0, 255);
			m_vPos[nNumLine + i].color = D3DCOLOR_RGBA(255, 0, 0, 255);
			m_vPos[nNumLine + i + 1].color = D3DCOLOR_RGBA(255, 0, 0, 255);
		}
	}
}

//==========================================================================
// 解放
void CGrid::Uninit(void)
{
	Delete(m_vPos);
	Delete(m_aMtxWorld);
}

//==========================================================================
// 描画
void CGrid::Draw(void)
{
	if (m_vPos != nullptr)
	{
		LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();

		D3DXMatrixIdentity(m_aMtxWorld);

		// ライト設定
		pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

		// 各種行列の設定
		pDevice->SetTransform(D3DTS_WORLD, m_aMtxWorld);

		// FVFの設定
		pDevice->SetFVF(FVF_VERTEX_2);

		pDevice->DrawPrimitiveUP(D3DPT_LINELIST, (int)(nNumGrid / 2), m_vPos, sizeof(VERTEX_2));
	}
}
