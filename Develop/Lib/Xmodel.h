//==========================================================================
// Xモデル[Xmodel.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Xmodel_H_
#define _Xmodel_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "ExcelOpen.h"
#include "3DObject.h"
#include "Template.h"

//==========================================================================
//
// class  : CXmodel
// Content: Xモデル
//
//==========================================================================
class CXmodel : private CExcelOpen, private CTemplate
{
private:
	class LINKLIST
	{
	public:
		LPSTR Link; // リンク
	};

	// モデルデータ管理
	class XMODELDATA
	{
	public:
		LPD3DXMESH pMesh; // メッシュデータ
		DWORD NumMaterial; // マテリアル数
		D3DMATERIAL9 *pMatMesh; // マテリアル情報
		LPDIRECT3DTEXTURE9* pMeshTex; // メッシュのテクスチャ
		int* ID; // テクスチャのID
		int NumTex; // テクスチャの数
		int ModelID; // ID
		bool *ExistenceTexture; // テクスチャの有無
	};
public:
	// マテリアルライトの設定
	enum class LightMoad
	{
		Auto = 0, // 自動
		System, // システム設定
		Material // マテリアル設定
	};
private:
	XMODELDATA * m_MdelData; // モデルデータ
	int m_NumData; // データの数
	FILELINK1 *m_Link; // リンク
	LightMoad m_MaterialMood; // マテリアルのモード
public:
	CXmodel();
	~CXmodel();

	// 初期化 Excel 失敗時true
	bool Init(const char* pFile, LightMoad mode = LightMoad::Auto);

	// 初期化 失敗時true
	bool Init(const char** pFile, int Size, LightMoad mode = LightMoad::Auto);

	// 解放
	void Uninit(void);

	// 描画
	void Draw(C3DObject * Poly);

	// モデルデータのパラメーターゲッター
	XMODELDATA* GetMdelDataParam(C3DObject * Poly) { return &this->m_MdelData[Poly->ID]; }

private:
	// Xモデルの初期化
	bool InitXModel(void);

	// リンク入力
	void InputLink(const char ** pInput);

	// モデルデーターのチェック
	bool OverlapModel(int* Num);

	// テクスチャのファイルパス読み込み
	void LoadTextureLink(const LPD3DXMATERIAL Input, LINKLIST *&lTexList, const char* pPas, int* Num);

	// テクスチャの読み込み
	bool LoadTexture(LINKLIST * lTexList, LPDIRECT3DDEVICE9 * pDevice, int* Num);

	// マテリアルの設定
	void MaterialSetting(const LPD3DXMATERIAL Input, int* Num);

	// マテリアルの設定 オートモード
	void MaterialSettingAuto(D3DMATERIAL9 *pMatMesh, const D3DMATERIAL9 *Input, bool *ExistenceTexture);

	// マテリアルの設定 システム設定
	void MaterialSettingSystem(D3DMATERIAL9 *pMatMesh);

	// マテリアルの設定 マテリアル素材設定
	void MaterialSettingMaterial(D3DMATERIAL9 *pMatMesh, const D3DMATERIAL9 *Input);

	// 読み込み
	bool Read(LPDIRECT3DDEVICE9* pDevice, LPD3DXBUFFER * pAdjacency, LPD3DXBUFFER *  pMaterialBuffer, int* Num);

	// 最適化
	bool Optimisation(LPD3DXBUFFER pAdjacency, int* Num);

	// 複製
	bool Replication(D3DVERTEXELEMENT9 * pElements, LPDIRECT3DDEVICE9 *pDevice, LPD3DXMESH * pTempMesh, int* Num);

	// 使用しているテクスチャが0の場合false 1枚でもあればtrue
	bool ExistenceTex(int * Num);
};

#endif // !_Xmodel_H_
