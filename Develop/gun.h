//==========================================================================
// 銃[gun.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _GUN_H_
#define _GUN_H_

//==========================================================================
//
// class  : CGun
// Content: 銃
//
//==========================================================================
class CGun : private CTemplate
{
public:
	CGun();
	~CGun();
	// 初期化
	bool Init(const char ** InputGun);
	// 弾の初期化
	bool InitBarrett(const char ** pFile, int size);
	// 使用するエフェクトの初期化
	bool InitEffect(const char * InputExplosion, int UpdateFrame, int Pattern, int Direction);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(D3DXMATRIX *MtxView);
	// 銃の座標セット
	void SetPos(C3DObject * Input);
	// 銃のトリガー
	void Trigger(bool Input, const float *Spped);
	// 描画判定
	void DrawJudgment(bool Input) { this->m_Draw = Input; }
	// 弾の情報のゲッター 
	CBarrett *GetBallettPos(void) { return this->m_Barrett; }
private:
	CXmodel *m_ModelData = nullptr; // モデルデータ
	C3DObject *m_Pos = nullptr; // 座標
	CBarrett *m_Barrett = nullptr; // 弾データ
	bool m_Draw; // 描画フラグ
};

#endif // !_GUN_H_
