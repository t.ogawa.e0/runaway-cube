//==========================================================================
// スタート演出[Start.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Start_H_
#define _Start_H_

//==========================================================================
//
// class  : CStartScene
// Content: スタート演出
//
//==========================================================================
class CStartScene : private CTemplate
{
public:
	CStartScene();
	~CStartScene();
	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static bool Update(void);
	// 描画
	static void Draw(void);
private:
	// カウントの更新
	static void CountUpdate(void);
private:
	// フォントの更新
	static void CountdownFontUpdate(void);
	// フォントの描画
	static void CountdownFontDraw(void);
private:
	// 更新
	static void StartUpdate(void);
	// 描画
	static void StartDraw(void);
private:
	static C2DObject * m_CountdownFontPos; // 座標
	static C2DPolygon * m_CountdownFontPoly; // ポリゴン
	static bool * m_CountdownFontDraw; // フォント1描画判定
	static int * m_CountdownFontMove; // 移動
private:
	static constexpr float m_DefaltCount = (99.0f / 60.0f);
	static bool * m_CountDraw; // フォント2描画判定
	static CTimer * m_Timer; // タイマー
private:
	static C2DObject * m_StartPos; // 座標
	static C2DPolygon * m_StartPoly; // ポリゴン 
	static bool * m_StartFontDraw; // フォント1描画判定
	static int * m_StartColor; // フォント1描画判定
private:
	static bool * m_StartKey;
};

#endif // !_Start_H_
