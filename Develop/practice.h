//==========================================================================
// チュートリアル[practice.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _PRACTICE_H_
#define _PRACTICE_H_

#include"SceneChange.h"

//==========================================================================
//
// class  : CPracticeScene
// Content: チュートリアルシーン
//
//==========================================================================
class CPracticeScene : public CBaseScene
{
public:
	CPracticeScene();
	~CPracticeScene();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
private:
	CSound m_Sound;
};

#endif // !_PRACTICE_H_
