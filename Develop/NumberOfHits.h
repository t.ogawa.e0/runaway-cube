//==========================================================================
// íe[NumberOfHits.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _NumberOfHits_H_
#define _NumberOfHits_H_

//==========================================================================
//
// class  : CNumberOfHits
// Content: íe
//
//==========================================================================
class CNumberOfHits : private CTemplate
{
private:
	static constexpr int m_DrawLimit = 60;
public:
	CNumberOfHits();
	~CNumberOfHits();
	// ú»
	static bool Init(void);
	// ðú
	static void Uninit(void);
	// XV
	static void Update(void);
	// `æ
	static void Draw(void);
	// íecount
	static void HitCount(void);
private:
	static int m_DrawCount; // `æI¹ÜÅ
	static int m_HitsCount; // íe
	static C2DObject * m_Pos; // ÀW
	static C2DPolygon * m_Poly; // |S
	static CNumber * m_Number; // 
};

#endif // !_NumberOfHits_H_
