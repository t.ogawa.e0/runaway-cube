//==========================================================================
// リザルト[result.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "result.h"
#include "ResultFont.h"
#include "ContentsResult.h"
#include "PressEnter.h"
#include "Background.h"
#include "ThankYouForPlaying.h"
#include "Fade.h"

CResultScene::CResultScene()
{
}

CResultScene::~CResultScene()
{
}

//==========================================================================
// 初期化
bool CResultScene::Init(void)
{
	CLight light;
	CSound::SoundLabel SoundList[] =
	{
		{ "resource/sound/Resultmusic.wav",-1,1.0f },
	};

	light.Init({ 1,0,0 });
	if (CResultFont::Init()) { return true; }
	if (CContentsResult::Init()) { return true; }
	if (CPressEnter::Init()) { return true; }
	if (CBackground::Init()) { return true; }
	if (CThankYouForPlaying::Init()) { return true; }

	// サウンド
	if (this->m_Sound.Init(SoundList, 1)) { return true; }
	this->m_Sound.Volume(0, 50);
	this->m_Sound.Play(0);

	return false;
}

//==========================================================================
// 解放
void CResultScene::Uninit(void)
{
	CResultFont::Uninit();
	CContentsResult::Uninit();
	CPressEnter::Uninit();
	CBackground::Uninit();
	CThankYouForPlaying::Uninit();

	// サウンド
	this->m_Sound.Uninit();
}

//==========================================================================
// 更新
void CResultScene::Update(void)
{
	CResultFont::Update();
	CPressEnter::Update();
	CBackground::Update();

	CThankYouForPlaying::Update(CContentsResult::Update());

	if (*CThankYouForPlaying::GetKey())
	{
		// フェード処理開始フラグ
		if (!CFade::GetDraw())
		{
			CFade::In();
		}
	}
}

//==========================================================================
// 描画
void CResultScene::Draw(void)
{
	CBackground::Draw();
	CResultFont::Draw();
	CContentsResult::Draw();
	CPressEnter::Draw();
	CThankYouForPlaying::Draw();
}
