//==========================================================================
// オブジェクト管理[Object.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _CObject_H_
#define _CObject_H_

//==========================================================================
//
// class  : CObject
// Content: オブジェクト管理クラス
//
//==========================================================================
class CObject
{
protected:
	CObject();
	virtual ~CObject();
	// 継承初期化
	virtual bool Init(void) = 0;
	// 継承解放
	virtual void Uninit(void) = 0;
	// 継承更新
	virtual void Update(void) = 0;
	// 継承描画
	virtual void Draw(void) = 0;
private:
	CObject *m_pNext = nullptr; // 次のアドレス
	CObject *m_pBack = nullptr; // 前のアドレス
	int m_ID = 0; // 管理ID
public:
	// オブジェクトの生成
	// p = new T();
	template <typename T> static T* NewObject(T*& p);
	// 登録済みオブジェクトの初期化
	static bool InitAll(void);
	// 登録済みオブジェクトの解放
	static void UninitAll(void);
	// 登録済みオブジェクトの更新
	static void UpdateAll(void);
	// 登録済みオブジェクトの描画
	static void DrawAll(void);
private:
	static CObject *m_pStart; // 先頭アドレス
	static int m_NumData; // データ数
	static int m_IDCount; // IDカウンタ
};

//==========================================================================
// p = new T();
template<typename T>
inline T * CObject::NewObject(T *& p)
{
	(*&p) = nullptr;
	(*&p) = new T();

	return (*&p);
}

#endif // !_CObject_H_
