//==========================================================================
// デバイス[DXDevice.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _DXDevice_H_
#define _DXDevice_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <vector>
#include "Template.h"

//==========================================================================
//
// class  : CDirectXDevice
// Content: デバイス 
//
//==========================================================================
class CDirectXDevice : private CTemplate
{
private:
	class CWindowsSize
	{
	public:
		int m_Width;
		int m_Height;
	};
private:
	static LPDIRECT3D9 m_pD3D; //ダイレクト3Dインターフェース
	static LPDIRECT3DDEVICE9 m_pD3DDevice;//ダイレクト3Dデバイス
	static CWindowsSize m_WindowsSize; // ウィンドウサイズ
	static D3DPRESENT_PARAMETERS m_d3dpp;
	static D3DDISPLAYMODE m_d3dpm;
	static std::vector<D3DDISPLAYMODE> m_WindowsModes; // ディスプレイの解像度格納
	static int m_NumWindowsModes; // ウィンドウモードの数
public:
	CDirectXDevice();
	~CDirectXDevice();
	// デバイスの生成
	static bool CreateDevice(const HWND * hWnd);
	// 初期化 失敗時true
	// WindowSize = 0〜100%
	static bool Init(const HINSTANCE * hInstance, const HWND * hWnd, BOOL bWindow, int WindowSize);
	// 解放
	static void Uninit(void);

	static LPDIRECT3DDEVICE9 GetD3DDevice(void) { return m_pD3DDevice; };	//デバイスの習得

	static CWindowsSize GetWindowsSize(void) { return m_WindowsSize; }
};

#endif // !_DXDevice_H_
