//==========================================================================
// ホーム[home.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _HOME_H_
#define _HOME_H_

#include"SceneChange.h"

//==========================================================================
//
// class  : CHomeScene
// Content: ホームシーン
//
//==========================================================================
class CHomeScene : public CBaseScene
{
public:
	CHomeScene();
	~CHomeScene();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
private:
};


#endif // !_HOME_H_
