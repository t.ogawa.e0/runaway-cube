//==========================================================================
// ダメージ演出[DamageEffect.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _DamageEffect_H_
#define _DamageEffect_H_

//==========================================================================
//
// class  : CDamageEffect
// Content: ダメージ演出
//
//==========================================================================
class CDamageEffect : private CTemplate
{
private:
	class CParam
	{
	public:
		int m_Count; // カウンタ
		int m_αCount; // アルファ値カウンタ
		C2DObject m_Pos; // 座標
		C2DPolygon m_Poly; // ポリゴン
	};
public:
	CDamageEffect();
	~CDamageEffect();
	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static void Update(void);
	// 描画
	static void Draw(void);
	// ダメージ演出
	static void Damage(void);
	// α値のゲッター
	static int *Getα(void);
private:
	static CParam * m_Param;
};

#endif // !_DamageEffect_H_
