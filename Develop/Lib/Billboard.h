//==========================================================================
// ビルボード[Billboard.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Billboard_H_
#define _Billboard_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Vertex3D.h"
#include "Rectangle.h"
#include "SetRender.h"
#include "3DObject.h"
#include "Template.h"

//==========================================================================
//
// class  : CBillboard
// Content: ビルボード
//
//==========================================================================
class CBillboard : private VERTEX_3D, private CRectangle, private CSetRender, private CTemplate
{
private:
	static constexpr float VCRCT = 0.5f;
	static const int nBite = 256; // バイト数
private:
	class CUV // UV
	{
	public:
		float u0;
		float v0;
		float u1;
		float v1;
	};
	class CCutList
	{
	public:
		float x;
		float y;
		float w;
		float h;
		float z;
	};
	class CTexSize
	{
	public:
		int Widht;
		int Height;
	};

	// テクスチャ情報格納
	class CAnimation
	{
	public:
		int Frame; // 更新タイミング
		int Pattern; // アニメーションのパターン数
		int Direction; // 一行のアニメーション数
		CCutList m_Cut; // 切り取り情報
		CTexSize m_TexSize; // テクスチャのサイズ
	};
private:
	LPDIRECT3DTEXTURE9 m_pTexture; // テクスチャを張り付けるためのポインタ
	LPDIRECT3DVERTEXBUFFER9 m_pVertexBuffer; // バッファ
	LPDIRECT3DINDEXBUFFER9 m_pIndexBuffer; // インデックスバッファ
	CAnimation *m_pAnimation; // アニメーション情報
	CUV m_UV; // UV格納
	VERTEX_4* m_pPseudo;// 頂点バッファのロック
public:
	CBillboard();
	~CBillboard();
	// 初期化 失敗時true
	bool Init(const char* pTex);

	// アニメーション用初期化 失敗時true
	bool Init(const char* pTex, int UpdateFrame, int Pattern, int Direction);
	// 解放
	void Uninit(void);
	// 描画
	void Draw(C3DObject * pInput, D3DXMATRIX *MtxView, int * AnimationCount = nullptr, bool bADD = false);

	// アニメーション切り替わり時の判定 切り替え時true 
	bool GetPattanNum(int * AnimationCount);

	// アニメーション用のカウンタの初期化 引数はカウンタ用変数
	int *AnimationCountInit(int * AnimationCount);
private:
	// テクスチャの読み込み
	bool LoadTex(LPDIRECT3DDEVICE9 pDevice, const char* pTex);

	// バーテックスバッファのセット
	bool SetVertexBuffer(LPDIRECT3DDEVICE9 pDevice);

	// インデックスバッファのセット
	bool SetIndexBuffer(LPDIRECT3DDEVICE9 pDevice);

	// 頂点バッファの生成
	void CreateVertexBuffer(VERTEX_4 * Output);

	// アニメーション情報のセット
	void AnimationPalam(int * AnimationCount);

	// UVの生成
	void UV(void);
};

#endif // !_Billboard_H_
