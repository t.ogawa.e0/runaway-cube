//==========================================================================
// デバッグ[debug.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _DEBUG_H_
#define _DEBUG_H_

//==========================================================================
//
// class  : CDebug
// Content: デバッグ
//
//==========================================================================
class CDebug
{
public:
	CDebug();
	~CDebug();

	// シーンセット
	static void Set(CSceneManager::SceneName Input);

	// 描画
	static void Draw(void);
private:
	static void Title(void);
	static void Home(void);
	static void Game(void);
	static void Result(void);
	static void Screen_Saver(void);
	static void Practice(void);
	static void Loat(void);
	static char *BoolFont(bool Input) { return ((Input) & 1) ? "true" : "false";}
private:
	static CSceneManager::SceneName m_Name;
	static int m_FontPosY;
};

#endif // !_DEBUG_H_
