//==========================================================================
// 空[Sky.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Sky_H_
#define _Sky_H_

//==========================================================================
//
// class  : CSky
// Content: 空
//
//==========================================================================
class CSky : private CTemplate
{
public:
	CSky();
	~CSky();
	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static void Update(void);
	// 描画
	static void Draw(void);
private:
	static C2DObject * m_Pos;
	static C2DPolygon * m_Poly;
};

#endif // !_Sky_H_
