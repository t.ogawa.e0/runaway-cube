//==========================================================================
// 2Dオブジェクト[2DObject.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _2DObject_H_
#define _2DObject_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Vertex3D.h"

//==========================================================================
//
// class  : C2DObject
// Content: 2Dオブジェクト オブジェクトを動かすためのもの
//
//==========================================================================
class C2DObject : private VERTEX_3D
{
private:
	class CUV // UV
	{
	public:
		float u0;
		float v0;
		float u1;
		float v1;
	};
	class CList
	{
	public:
		float x;
		float y;
		float w;
		float h;
	};
	class CColor
	{
	public:
		int r;
		int g;
		int b;
		int a;
	};
	class CPos
	{
	public:
		float x;
		float y;
	};
	class CAnim
	{
	public:
		int Count; // アニメーションカウンタ
		int Frame; // 更新タイミング
		int Pattern; // アニメーションのパターン数
		int Direction; // 一行のアニメーション数
		bool Key;
	};
	class CAffine
	{
	public:
		float Angle;
		bool Key;
	};
	struct ANGLE
	{
		static constexpr float	_ANGLE_000 = 0.00000000f;
		static constexpr float	_ANGLE_045 = 0.785398185f;
		static constexpr float	_ANGLE_090 = 1.57079637f;
		static constexpr float	_ANGLE_135 = 2.35619450f;
		static constexpr float	_ANGLE_180 = 3.14159274f;
		static constexpr float	_ANGLE_225 = -2.35619450f;
		static constexpr float	_ANGLE_270 = -1.57079637f;
		static constexpr float	_ANGLE_315 = -0.785398185f;

	}m_ANGLE;
public:
	int m_ID; // テクスチャID
private:
	CList m_Cut; // テクスチャカット位置
	CPos m_Pos; // ポリゴンの座標
	CColor m_Color; // ポリゴンの色
	CAnim m_Anim; // アニメーション
	CAffine m_Affine; // アフィン変換
	float m_Scale; // スケール
	bool m_Key; // キー
	bool m_SetAnimationCount; // アニメーションカウンタがセットされているフラグ
	bool m_CentralCoordinates; // 中心座標モード
public:
	C2DObject();
	~C2DObject();

	// 初期化
	void Init(int TexNumber);

	// アニメーション用初期化
	// Frame=更新フレーム 
	// Pattern=アニメーション数
	// Direction=横一列のアニメーション数
	void Init(int TexNumber, int Frame, int Pattern, int Direction);

	// 座標
	void SetPos(float x, float y);

	void SetX(float x);

	void SetY(float y);

	void SetXPlus(float x);

	void SetYPlus(float y);

	// 中心座標モード
	void SetCentralCoordinatesMood(bool mood) { this->m_CentralCoordinates = mood; }

	CPos *GetPos(void) { return &this->m_Pos; }

	// サイズ
	void Scale(float Size);

	// 回転角度
	void Angle(float Angle);

	// 色セット
	void SetColor(int r, int g, int b, int a);

	// 無回転バーテックス
	// 使用禁止
	VERTEX_3 * CreateVertex(VERTEX_3 * pPseudo, int * Widht, int * Height);

	// 回転バーテックス
	// 使用禁止
	VERTEX_3 * CreateVertexAngle(VERTEX_3 * pPseudo, int * Widht, int * Height);

	// アニメーション切り替わり時の判定 切り替え時true 
	bool GetPattanNum(void);

	// 角度テンプレート
	ANGLE GetAngle(void) { return this->m_ANGLE; }

	// アニメーションカウンタのセット
	void SetAnimationCount(int nCount);

	CAnim * GetAnimParam(void) { return &this->m_Anim; }
private:
	// UVの生成
	void UV(CUV * UV, int * Widht, int * Height);

	// アニメーションの情報
	void AnimationPalam(void);

	// テクスチャの切り取り
	void SetTexturCut(int * Widht, int * Height);
};

#endif // !_2DObject_H_
