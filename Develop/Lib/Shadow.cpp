//==========================================================================
// 影[Shadow.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Shadow.h"
#include "DXDevice.h"

//==========================================================================
// マクロ定義
//==========================================================================
#ifndef ErrorMessage
#define ErrorMessage(p,c) MessageBox((p), (c), "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING)
#endif // !ErrorMessage

CShadow::CShadow()
{
}

CShadow::~CShadow()
{
}

//==========================================================================
// 初期化 失敗時true
bool CShadow::Init(const char * Input)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	CUV uUV;
	D3DXIMAGE_INFO dil; // 画像データの管理
	WORD* pIndex;

	this->m_pIndexBuffer = nullptr;
	this->m_pVertexBuffer = nullptr;
	this->m_pTexture = nullptr;

	if (FAILED(D3DXCreateTextureFromFile(pDevice, Input, &this->m_pTexture)))
	{
		ErrorMessage(nullptr, "テクスチャの読み込み失敗");
		return true;
	}

	// 画像データの格納
	D3DXGetImageInfoFromFile(Input, &dil);

	uUV.u0 = 0.0f;
	uUV.v0 = 0.0f;
	uUV.u1 = 1.0f;
	uUV.v1 = 1.0f;

	if (FAILED(pDevice->CreateVertexBuffer(sizeof(VERTEX_4) * 4, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &this->m_pVertexBuffer, nullptr)))
	{
		ErrorMessage(nullptr, "頂点バッファが作れませんでした。");
		return true;
	}

	if (FAILED(pDevice->CreateIndexBuffer(sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &this->m_pIndexBuffer, nullptr)))
	{
		ErrorMessage(nullptr, "インデックスバッファが作れませんでした。");
		return true;
	}

	this->m_pVertexBuffer->Lock(0, 0, (void**)&this->m_pPseudo, D3DLOCK_DISCARD);
	this->CreateVertexBuffer(this->m_pPseudo, &uUV);
	this->m_pVertexBuffer->Unlock(); // ロック解除

	this->m_pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
	this->RectangleIndex(pIndex, 1);
	this->m_pIndexBuffer->Unlock();	// ロック解除

	return false;
}

//==========================================================================
// 解放
void CShadow::Uninit(void)
{
	this->Release(this->m_pTexture);

	// バッファ解放
	this->Release(this->m_pVertexBuffer);

	// インデックスバッファ解放
	this->Release(this->m_pIndexBuffer);
}

//==========================================================================
// 描画
void CShadow::Draw(const C3DObject * pInput)
{
	D3DXMATRIX aMtxWorld; // ワールド行列
	C3DObject aObject = *pInput;
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	int nColor = DefaltColor - (int)((aObject.Info.pos.y * ColorBuf) + 0.5f);

	aObject.Info.rot = D3DXVECTOR3(0, 0, 0);
	aObject.Info.sca *= aObject.Info.pos.y + 1.0f;
	if (nColor <= 0) { nColor = 0; }
	if (aObject.Info.pos.y <= 0) { nColor = 0; }
	this->m_pVertexBuffer->Lock(0, 0, (void**)&this->m_pPseudo, D3DLOCK_DISCARD);
	this->m_pPseudo[0].color = D3DCOLOR_RGBA(255, 255, 255, nColor);
	this->m_pPseudo[1].color = D3DCOLOR_RGBA(255, 255, 255, nColor);
	this->m_pPseudo[2].color = D3DCOLOR_RGBA(255, 255, 255, nColor);
	this->m_pPseudo[3].color = D3DCOLOR_RGBA(255, 255, 255, nColor);
	this->m_pVertexBuffer->Unlock(); // ロック解除

	// サイズ
	pDevice->SetStreamSource(0, this->m_pVertexBuffer, 0, sizeof(VERTEX_4));	// パイプライン

	pDevice->SetIndices(this->m_pIndexBuffer);

	// FVFの設定
	pDevice->SetFVF(FVF_VERTEX_4);

	pDevice->SetTexture(0, nullptr);
	pDevice->SetTexture(0, this->m_pTexture);

	// ライト設定
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	// 減算処理
	this->SetRenderREVSUBTRACT(pDevice);

	// 各種行列の設定
	aObject.Info.pos.y = 0.0f;

	pDevice->SetTransform(D3DTS_WORLD, aObject.CreateMtxWorld2(&aMtxWorld));

	// 描画設定
	pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);

	// 戻す
	this->SetRenderADD(pDevice);
}

//==========================================================================
// 頂点バッファの生成
void CShadow::CreateVertexBuffer(VERTEX_4 * Output, CUV * uv)
{
	VERTEX_4 vVertex[] =
	{
		// 上
		{ D3DXVECTOR3(-VCRCT,0.0f, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u0,uv->v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,0.0f, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u1,uv->v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,0.0f,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u1,uv->v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,0.0f,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,80),D3DXVECTOR2(uv->u0,uv->v1) }, // 左下
	};

	for (int i = 0; i < (int)(sizeof(vVertex) / sizeof(VERTEX_4)); i++)
	{
		Output[i] = vVertex[i];
	}
}
