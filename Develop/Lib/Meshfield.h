//==========================================================================
// メッシュ[Meshfield.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Meshfield_H_
#define _Meshfield_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "ExcelOpen.h"
#include "Vertex3D.h"
#include "3DObject.h"
#include "Template.h"

//==========================================================================
//
// class  : CMeshfield
// Content: 縮退ポリゴン
//
//==========================================================================
class CMeshfield : private CExcelOpen, private VERTEX_3D, private CTemplate
{
private:
	static constexpr int xInfo = 1;
	static constexpr int xRGBA = 255;
	static constexpr int x_2 = 2;
	static constexpr int x_Buff = 256;
	static constexpr float CorrectionValue = 0.5f;
private:
	class DEGENERATEPOLYGONINFORMATION // 縮退ポリゴン
	{
	public:
		int NumMeshX; // 面の数
		int NumMeshZ; // 面の数
		int VertexOverlap; // 重複する頂点数
		int	NumXVertexWey; // 視覚化されている1列の頂点数
		int	NumZVertex; // 基礎頂点数
		int	NumXVertex; // 基礎頂点数
		int	NumMeshVertex; // 視覚化されている全体の頂点数
		int	MaxPrimitive; // プリミティブ数
		int	MaxIndex; // 最大Index数
		int	MaxVertex; // 最大頂点数
	};

	class DEGENERATEPOLYGON
	{
	public:
		D3DMATERIAL9 pMatMesh; // メッシュ
		LPDIRECT3DVERTEXBUFFER9 pVertexBuffer; // 頂点バッファ
		LPDIRECT3DINDEXBUFFER9 pIndexBuffer; // インデックスバッファ
		LPDIRECT3DTEXTURE9 pTexture; // テクスチャ
		DEGENERATEPOLYGONINFORMATION Info; // 縮退ポリゴンの情報
	};
	class MESHLINK
	{
	public:
		char m_Data[x_Buff];
		int m_x, m_z;
	};
public:
	typedef MESHLINK LINK; // リンク格納
private:
	DEGENERATEPOLYGON * m_MeshData; // メッシュデータ
	int m_NumData; // データ数
	FILELINK2 *m_Link; // リンク
public:
	CMeshfield();
	~CMeshfield();
	// 初期化 Excel 失敗時true
	bool Init(const char * pFile);
	// 初期化 失敗時true
	bool Init(const MESHLINK * pFile, int Size);
	// 解放
	void Uninit(void);
	// 描画
	void Draw(C3DObject * Poly);
	// データ数取得
	int GetNumData(void) { return this->m_NumData; }
	// メッシュデータゲッター
	DEGENERATEPOLYGONINFORMATION GetMeshData(int num) { return this->m_MeshData[num].Info; }
private:
	// リンク入力
	void InputLink(const MESHLINK * pInput);
	// メッシュの初期化
	bool InitMeshfield(void);
	// メッシュ情報の生成
	void MeshFieldInfo(DEGENERATEPOLYGONINFORMATION * Output, const int * numX, const int * numZ);
	// インデックス情報の生成
	void CreateIndex(LPWORD * Output, const DEGENERATEPOLYGONINFORMATION * Input);
	// バーテックス情報の生成
	void CreateVertex(VERTEX_4 * Output, const DEGENERATEPOLYGONINFORMATION * Input);
	// 頂点バッファの生成
	bool CreateVertexBuffer(LPDIRECT3DDEVICE9 pDevice, DEGENERATEPOLYGON *Output);
	// インデックスバッファの生成
	bool CreateIndexBuffer(LPDIRECT3DDEVICE9 pDevice, DEGENERATEPOLYGON *Output);
	// 読込み
	bool Read(LPDIRECT3DDEVICE9 pDevice, FILELINK2* pLink, DEGENERATEPOLYGON *Output);
	// マテリアルの設定
	void MaterialSetting(DEGENERATEPOLYGON *Output);
};

#endif // !_Meshfield_H_
