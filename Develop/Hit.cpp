//==========================================================================
// 当たり判定[Hit.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "GunIncludeData.h"
#include "Hit.h"
#include "field.h"
#include "player.h"
#include "enemy.h"

#include "DamageEffect.h"
#include "NumberOfHits.h"
#include "DestructionRate.h"

CHit::CHit()
{
}

CHit::~CHit()
{
}

//==========================================================================
// ゲームの当たり判定
void CHit::GameHit(void)
{
	PlayerWallHit();
	GunHit(CPlayer::GetMosinData());
	GunHit(CEnemy::GetGunData());
	PlayerHitEnemyGun(CEnemy::GetGunData());
	EnemyHitPlayerGun(CPlayer::GetMosinData());
	EnemyHitEnemy();
}

//==========================================================================
// プレイヤーの当たり判定
void CHit::PlayerWallHit(void)
{
	bool bPlayerWallHit = false;

	// 壁の枚数分
	for (int i = 0; i < (*CField::NumWall()); i++)
	{
		// 当たり判定処理
		if (CHitDetermination::Simple(CPlayer::GetPos(), CField::WallPos(i), 1.05f))
		{
			// 前回の座標を今の座標にコピー
			CPlayer::GetPos()->Info.pos = CPlayer::GetOldPos()->Info.pos;
			bPlayerWallHit = true;
			break;
		}
	}

	// 判定が出ていないなら古い座標を記録
	if (bPlayerWallHit == false)
	{
		*CPlayer::GetOldPos() = *CPlayer::GetPos();
	}
}

//==========================================================================
// 弾と壁の当たり判定処理
void CHit::GunHit(CGun * pGun)
{
	int nCount = 0;
	int *pID = nullptr;
	C3DObject *pPos = nullptr;
	C3DObject *pOldPos = nullptr;
	C3DObject *pWallPos = nullptr;
	CBarrett *pBarrettData = pGun->GetBallettPos(); // 弾の座標を記録
	C3DObject Pos;
	Pos.Init();

	// アドレスがある限り続ける
	for (;;)
	{
		bool bDestroy = false;

		// アドレスがなかった場合終了
		if (pBarrettData->GetData(&nCount) == nullptr)
		{
			break;
		}

		pPos = &pBarrettData->GetData(&nCount)->m_Pos;

		if (pPos->Info.pos.y <= 0.0f)
		{
			pOldPos = &pBarrettData->GetData(&nCount)->m_OldPos;
			pID = &pBarrettData->GetData(&nCount)->m_ID;

			// 正確な座標が出るまで続ける
			for (;;)
			{
				pOldPos->MoveZ(0.1f);

				// 当たり判定が出たら
				if (pOldPos->Info.pos.y <= 0.0f)
				{
					pOldPos->MoveZ(-0.1f);
					pOldPos->Info.pos.y = 0;
					*pPos = *pOldPos;

					// 判定が出た弾を指定して解放
					pBarrettData->PinpointDelete(pID);

					break;
				}
			}
		}
		else if (0.0f <= pPos->Info.pos.y&& pPos->Info.pos.y <= CField::PosY())
		{
			// 壁の枚数繰り返す
			for (int i = 0; i < (*CField::NumWall()); i++)
			{
				pWallPos = CField::WallPos(i);

				// 当たり判定が出たら
				if (CHitDetermination::Simple(pPos, pWallPos, 1.0f))
				{
					pOldPos = &pBarrettData->GetData(&nCount)->m_OldPos;
					pID = &pBarrettData->GetData(&nCount)->m_ID;

					// 正確な座標が出るまで続ける
					for (;;)
					{
						pOldPos->MoveZ(0.1f);

						// 当たり判定が出たら
						if (CHitDetermination::Simple(pOldPos, pWallPos, 1.0f))
						{
							pOldPos->MoveZ(-0.1f);
							*pPos = *pOldPos;

							// 判定が出た弾を指定して解放
							pBarrettData->PinpointDelete(pID);

							bDestroy = true;
						}

						// 解放終了フラグ
						if (bDestroy == true)
						{
							break;
						}
					}
					break;
				}
			}
		}
		else if (CField::PosY() <= pPos->Info.pos.y)
		{
			// 判定外にいる時
			if (!CHitDetermination::Ball(&Pos, pPos, 200.0f))
			{
				pID = &pBarrettData->GetData(&nCount)->m_ID;

				// 判定が出た弾を指定して解放
				pBarrettData->PinpointDelete(pID);
			}
		}

		pOldPos = nullptr;
		pID = nullptr;
		nCount++;
	}
}

void CHit::EnemyHitPlayerGun(CGun * pGun)
{
	int nCount = 0;
	int *pID = nullptr;
	C3DObject *pEnemypos = nullptr;
	C3DObject *pPos = nullptr;
	C3DObject *pOldPos = nullptr;
	CBarrett *pBarrettData = pGun->GetBallettPos(); // 弾の座標を記録

	// アドレスがある限り続ける
	for (;;)
	{
		// アドレスがなかった場合終了
		if (pBarrettData->GetData(&nCount) == nullptr)
		{
			break;
		}

		pPos = &pBarrettData->GetData(&nCount)->m_Pos;

		for (int nEnemyCount = 0;;nEnemyCount++)
		{
			if (CEnemy::GetData(&nEnemyCount) == nullptr)
			{
				break;
			}

			pEnemypos = CEnemy::GetData(&nEnemyCount)->GetData();

			// 当たり判定が出たら
			if (CHitDetermination::Ball(pEnemypos, pPos, pBarrettData->GetData(&nCount)->m_Speed))
			{
				C3DObject Backup = pBarrettData->GetData(&nCount)->m_OldPos;
				pOldPos = &pBarrettData->GetData(&nCount)->m_OldPos;
				pID = &pBarrettData->GetData(&nCount)->m_ID;

				// 正確な座標が出るまで続ける
				for (;;)
				{
					pOldPos->MoveZ(0.1f);

					// 当たり判定が出たら
					if (CHitDetermination::Ball(pOldPos, pEnemypos, 0.5f))
					{
						pOldPos->MoveZ(-0.1f);
						*pPos = *pOldPos;

						// 判定が出た弾を指定して解放
						pBarrettData->PinpointDelete(pID);

						if (CEnemy::GetData(&nEnemyCount)->GetHit() == false)
						{
							CDestructionRate::Count();
						}

						CEnemy::PinpointDelete(&CEnemy::GetData(&nEnemyCount)->m_ID);
						break;
					}

					if (CHitDetermination::Ball(pPos, pOldPos, 0.5f))
					{
						*pOldPos = Backup;
						break;
					}
				}
				break;
			}
		}
		pOldPos = nullptr;
		pID = nullptr;
		nCount++;
	}
}

void CHit::PlayerHitEnemyGun(CGun * pGun)
{
	int *pID = nullptr;
	C3DObject *pPos = nullptr;
	C3DObject *pOldPos = nullptr;
	C3DObject PlayerPos = *CPlayer::GetHeadPos();
	CBarrett *pBarrettData = pGun->GetBallettPos(); // 弾の座標を記録

	for (int i = 0; ; i++)
	{
		// アドレスがなかった場合終了
		if (pBarrettData->GetData(&i) == nullptr)
		{
			break;
		}

		pPos = &pBarrettData->GetData(&i)->m_Pos;
		PlayerPos.Info.pos.y = pPos->Info.pos.y;

		// 当たり判定が出たら
		if (CHitDetermination::Ball(pPos, &PlayerPos, pBarrettData->GetData(&i)->m_Speed))
		{
			C3DObject Backup = pBarrettData->GetData(&i)->m_OldPos;
			pOldPos = &pBarrettData->GetData(&i)->m_OldPos;
			pID = &pBarrettData->GetData(&i)->m_ID;

			// 正確な座標が出るまで続ける
			for (;;)
			{
				pOldPos->MoveZ(0.1f);

				// 当たり判定が出たら
				if (CHitDetermination::Ball(pOldPos, &PlayerPos, 0.5f))
				{
					pOldPos->MoveZ(-0.1f);
					*pPos = *pOldPos;

					// 判定が出た弾を指定して解放
					pBarrettData->PinpointDelete(pID);
					CDamageEffect::Damage();
					CNumberOfHits::HitCount();
					break;
				}

				if (CHitDetermination::Ball(pPos, pOldPos, 0.5f))
				{
					*pOldPos = Backup;
					break;
				}
			}
			break;
		}
	}
}

void CHit::EnemyHitEnemy(void)
{
	C3DObject *pPos1 = nullptr;
	C3DObject *pOldPos1 = nullptr;

	C3DObject *pPos2 = nullptr;
	C3DObject *pOldPos2 = nullptr;

	int *nbackpattern = nullptr;

	for (int nEnemyCount1 = 0; ; nEnemyCount1++)
	{
		if (CEnemy::GetData(&nEnemyCount1) == nullptr)
		{
			break;
		}

		pPos1 = CEnemy::GetData(&nEnemyCount1)->GetData();
		pOldPos1 = CEnemy::GetData(&nEnemyCount1)->GetOldPos();
		nbackpattern = CEnemy::GetData(&nEnemyCount1)->Getbackpattern();

		for (int nEnemyCount2 = nEnemyCount1; ; nEnemyCount2++)
		{
			if (CEnemy::GetData(&nEnemyCount2) == nullptr)
			{
				break;
			}

			if (CEnemy::GetData(&nEnemyCount2)->m_ID != CEnemy::GetData(&nEnemyCount1)->m_ID)
			{
				pPos2 = CEnemy::GetData(&nEnemyCount2)->GetData();
				pOldPos2 = CEnemy::GetData(&nEnemyCount2)->GetOldPos();

				if (CHitDetermination::Ball(pPos1, pPos2, 1.2f))
				{
					if (*nbackpattern == -1)
					{
						*nbackpattern = rand() % 4;
					}
					rand();
					switch (*nbackpattern)
					{
					case 0:
						pPos1->MoveZ(-(CEnemy::GetData(&nEnemyCount1)->GetSpped() * 2));
						break;
					case 1:
						pPos1->MoveZ((CEnemy::GetData(&nEnemyCount1)->GetSpped() * 2));
						break;
					case 2:
						pPos1->MoveX((CEnemy::GetData(&nEnemyCount1)->GetSpped() * 2));
						break;
					case 3:
						pPos1->MoveX(-(CEnemy::GetData(&nEnemyCount1)->GetSpped() * 2));
						break;
					default:
						*nbackpattern = -1;
						rand();
						break;
					}
				}
				else
				{
					*nbackpattern = -1;
				}
			}
		}
	}
}
