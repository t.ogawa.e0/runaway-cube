//==========================================================================
// ロードスクリーン[LoadScreen.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _LoadScreen_H_
#define _LoadScreen_H_

//==========================================================================
//
// class  : CLoadScreen
// Content: ロードスクリーン
//
//==========================================================================
class CLoadScreen : private CTemplate
{
private:
	// パラメータ
	class CParam
	{
	public:
		int m_α; // α
		bool m_Change; // change
	};
public:
	CLoadScreen();
	~CLoadScreen();

	// 初期化
	static 	bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static void Update(void);
	// 描画
	static void Draw(void);
private:
	// αチェンジ
	static void αChange(int ID, int Speed);
private:
	static C2DObject * m_Pos; // 座標
	static C2DPolygon * m_Poly; // ポリゴン
	static CParam * m_Param; // パラメータ
	static int m_NumData; // データ数

};

#endif // !_LoadScreen_H_
