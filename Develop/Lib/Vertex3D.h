//==========================================================================
// バーテックス[Vertex3D.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Vertex3D_H_
#define _Vertex3D_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// マクロ定義
//==========================================================================
#ifndef FVF_VERTEX_4
#define FVF_VERTEX_4 (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#endif // !FVF_VERTEX_4

#ifndef FVF_VERTEX_3
#define FVF_VERTEX_3 (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#endif // !FVF_VERTEX_3

#ifndef FVF_VERTEX_2
#define FVF_VERTEX_2 (D3DFVF_XYZ | D3DFVF_DIFFUSE)
#endif // !FVF_VERTEX_2

//==========================================================================
//
// class  : VERTEX_3D
// Content: バーテックス
//
//==========================================================================
class VERTEX_3D // バーテックス
{
protected:
	class VERTEX_4
	{
	public:
		D3DXVECTOR3 pos; // 座標
		D3DXVECTOR3 Normal; // 法線
		D3DCOLOR color;  // 色
		D3DXVECTOR2 Tex; // 頂点
	};
	class VERTEX_3
	{
	public:
		D3DXVECTOR4 pos; // 座標
		D3DCOLOR color;  // 色
		D3DXVECTOR2 Tex; // 頂点
	};
	class VERTEX_2
	{
	public:
		D3DXVECTOR3 pos; // 座標変換が必要
		D3DCOLOR color; // ポリゴンの色
	};
};

#endif // !_Vertex3D_H_
