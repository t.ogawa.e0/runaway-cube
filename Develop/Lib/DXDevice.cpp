//==========================================================================
// デバイス[DXDevice.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DXDevice.h"

//==========================================================================
// マクロ定義
//==========================================================================
#ifndef ErrorMessage
#define ErrorMessage(p,c) MessageBox((p), (c), "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING)
#endif // !ErrorMessage

//==========================================================================
// 実体
//==========================================================================
LPDIRECT3D9 CDirectXDevice::m_pD3D; //ダイレクト3Dインターフェース
LPDIRECT3DDEVICE9 CDirectXDevice::m_pD3DDevice;//ダイレクト3Dデバイス
CDirectXDevice::CWindowsSize CDirectXDevice::m_WindowsSize; // ウィンドウサイズ
D3DPRESENT_PARAMETERS CDirectXDevice::m_d3dpp;
D3DDISPLAYMODE CDirectXDevice::m_d3dpm;
std::vector<D3DDISPLAYMODE> CDirectXDevice::m_WindowsModes; // ディスプレイの解像度格納
int CDirectXDevice::m_NumWindowsModes; // ウィンドウモードの数

CDirectXDevice::CDirectXDevice()
{
}

CDirectXDevice::~CDirectXDevice()
{
}

//==========================================================================
// デバイスの生成
bool CDirectXDevice::CreateDevice(const HWND * hWnd)
{
	m_pD3DDevice = nullptr;

	//デバイスオブジェクトを生成
	//[デバイス作成制御]<描画>と<頂点処理>
	if (FAILED(m_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, *hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &m_d3dpp, &m_pD3DDevice)))
	{
		ErrorMessage(*hWnd, "デバイスの生成に失敗しました。");
		return true;
	}

	m_d3dpp.hDeviceWindow = *hWnd;

	// ビューポートの設定
	D3DVIEWPORT9 vp;
	vp.X = 0;
	vp.Y = 0;
	vp.Width = m_d3dpp.BackBufferWidth;
	vp.Height = m_d3dpp.BackBufferHeight;
	vp.MinZ = 0.0f;
	vp.MaxZ = 1.0f;
	if (FAILED(m_pD3DDevice->SetViewport(&vp)))
	{
		ErrorMessage(*hWnd, "ビューポートの設定に失敗しました。");
		return true;
	}

	// レンダーステートの設定
	{// この3つでα値が使える
		m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);				// αブレンドを許可
		m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);		// αソースカラーの設定
		m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);	// α
	}

	// テクスチャステージステートの設定
	{
		m_pD3DDevice->SetTextureStageState(0/*テクスチャステージ番号*/, D3DTSS_ALPHAOP/*演算子*/, D3DTOP_MODULATE/*掛け算*/);
		m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
	}

	// サンプラーステートの設定(UV値を変えると増えるようになる)
	{
		// WRAP...		反復する
		// CLAMP...　	引き伸ばす
		// MIRROR...　	鏡状態
		m_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
		m_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
	}

	// フィルタリング
	{
		// 画像を滑らかにする
		m_pD3DDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
		m_pD3DDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
		m_pD3DDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); // 元のサイズより小さい時綺麗にする
	}

	return false;
}

//==========================================================================
// 初期化 失敗時true
bool CDirectXDevice::Init(const HINSTANCE * hInstance, const HWND * hWnd, BOOL bWindow, int WindowSize)
{
	D3DDISPLAYMODE d3dspMode;

	Decoy(&WindowSize);
	Decoy(hInstance);
	m_pD3D = nullptr;

	//Direct3Dオブジェクトの作成
	m_pD3D = Direct3DCreate9(D3D_SDK_VERSION);
	if (m_pD3D == nullptr)
	{
		return true;
	}

	//現在のディスプレイモードを取得
	if (FAILED(m_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &m_d3dpm)))
	{
		return true;
	}

	// 何種類ディスプレイモードあるかを調べる
	m_NumWindowsModes = m_pD3D->GetAdapterModeCount(D3DADAPTER_DEFAULT, m_d3dpm.Format);

	// ディスプレイモードを調べる
	for (int i = 0; i < m_NumWindowsModes; i++)
	{
		if (FAILED(m_pD3D->EnumAdapterModes(D3DADAPTER_DEFAULT, m_d3dpm.Format, i, &d3dspMode)))
		{
			ErrorMessage(*hWnd, "ディスプレイモードの検索に失敗しました");
			return true;
		}

		// ディスプレイモードを記憶
		m_WindowsModes.push_back(d3dspMode);
	}

	// どのディスプレイモードを使うかを決定
	d3dspMode = m_WindowsModes[m_NumWindowsModes - 1];

	ZeroMemory(&m_d3dpp, sizeof(m_d3dpp));

	if (bWindow == TRUE)
	{
		if (100<WindowSize)
		{
			WindowSize = 100;
		}
		float WindowPercent = (float)WindowSize * 0.01f;

		d3dspMode.Height = (UINT)(d3dspMode.Height * WindowPercent);
		d3dspMode.Width = (UINT)(d3dspMode.Width * WindowPercent);

		//フルスクリーンのとき、リフレッシュレートを変えられる
		m_d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	}
	else if (bWindow == FALSE)
	{
		//フルスクリーンのとき、リフレッシュレートを変えられる
		m_d3dpp.FullScreen_RefreshRateInHz = d3dspMode.RefreshRate;
	}

	//デバイスのプレゼンテーションパラメータ(デバイスの設定値)
	m_d3dpp.BackBufferWidth = d3dspMode.Width;		//バックバッファの幅
	m_d3dpp.BackBufferHeight = d3dspMode.Height;	//バックバッファの高さ
	m_d3dpp.BackBufferFormat = d3dspMode.Format;    //バックバッファフォーマット
	m_d3dpp.BackBufferCount = 1;					//バッファの数
	m_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;		//垂直？
	m_d3dpp.EnableAutoDepthStencil = TRUE;			//3Dの描画に必要
	m_d3dpp.AutoDepthStencilFormat = D3DFMT_D16;	// 16Bit Zバッファ作成
	m_d3dpp.Windowed = bWindow;						// フルスクリーン・モード
	m_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;

	m_WindowsSize.m_Width = d3dspMode.Width;
	m_WindowsSize.m_Height = d3dspMode.Height;

	return false;
}

//==========================================================================
// 解放
void CDirectXDevice::Uninit(void)
{
	//デバイスの開放
	Release(m_pD3DDevice);

	//Direct3Dオブジェクトの開放
	Release(m_pD3D);
}
