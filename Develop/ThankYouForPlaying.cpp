//==========================================================================
// ThankYouForPlaying[ThankYouForPlaying.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "ThankYouForPlaying.h"

//==========================================================================
// ����
//==========================================================================
C2DObject * CThankYouForPlaying::m_Pos; // ���W
C2DPolygon *CThankYouForPlaying::m_Poly; // �|���S��
int CThankYouForPlaying::m_Num; // �����J�E���g
int CThankYouForPlaying::m_��count; // ���l�J�E���^
bool * CThankYouForPlaying::m_End; // �I���t���O
int CThankYouForPlaying::m_Count; // �J�E���^

CThankYouForPlaying::CThankYouForPlaying()
{
}

CThankYouForPlaying::~CThankYouForPlaying()
{
}

//==========================================================================
// ������
bool CThankYouForPlaying::Init(void)
{
	const char * pTexLink[] =
	{
		"resource/texture/ColorTex.png",
		"resource/texture/ThankYouForPlaying.png",
	};
	m_Num = (int)Sizeof(pTexLink);
	New(m_Pos, m_Num);
	New(m_Poly);
	New(m_End);

	for (int i = 0; i < m_Num; i++)
	{
		m_Pos[i].Init(i);
		m_Pos[i].SetColor(0, 0, 0, 0);
	}

	m_Pos[1].SetCentralCoordinatesMood(true);
	m_Pos[1].SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
	m_Pos[1].SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 2));

	if (m_Poly->Init(pTexLink, m_Num))
	{
		return true;
	}

	m_Poly->GetTexPalam(m_Pos[0].m_ID)->Widht = CDirectXDevice::GetWindowsSize().m_Width;
	m_Poly->GetTexPalam(m_Pos[0].m_ID)->Height = CDirectXDevice::GetWindowsSize().m_Height;

	m_��count = 0;
	m_Count = 0;
	(*m_End) = false;

	return false;
}

//==========================================================================
// ���
void CThankYouForPlaying::Uninit(void)
{
	m_Poly->Uninit();

	Delete(m_Pos);
	Delete(m_Poly);
	Delete(m_End);
}

//==========================================================================
// �X�V
void CThankYouForPlaying::Update(bool bkey)
{
	if (bkey)
	{
		m_��count += 5;
		if (255 <= m_��count)
		{
			m_��count = 255;
			m_Count++;
			if (m_Count == 60)
			{
				(*m_End) = true;
			}
		}
		m_Pos[1].SetColor(255, 255, 255, m_��count);
		m_Pos[0].SetColor(0, 0, 0, m_��count);
	}
}

//==========================================================================
// �`��
void CThankYouForPlaying::Draw(void)
{
	for (int i = 0; i < m_Num; i++)
	{
		m_Poly->Draw(&m_Pos[i]);
	}
}
