//==========================================================================
// バックグラウンド[Background.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "Background.h"

//==========================================================================
// 実体
//==========================================================================
C2DObject * CBackground::m_Pos; // 座標
C2DPolygon * CBackground::m_Poly; // ポリゴン
CCamera *CBackground::m_Camera; // ワールドビューカメラ

CBackground::CBackground()
{
}

CBackground::~CBackground()
{
}

//==========================================================================
// 初期化
bool CBackground::Init(void)
{
	const char * pTexLink[] =
	{
		"resource/texture/cube.png",
	};

	D3DXVECTOR3 Eye = D3DXVECTOR3(1.5f, 2.5f, 2.5f); // 注視点
	D3DXVECTOR3 At = D3DXVECTOR3(1.5f, 2.5f, 0.0f); // カメラ座標 

	New(m_Pos, (m_NumBrockX * m_NumBrockY));
	New(m_Poly);
	New(m_Camera);

	if (m_Poly->Init(pTexLink, Sizeof(pTexLink))) { return true; }

	m_Poly->GetTexPalam(0)->Widht = m_Poly->GetTexPalam(0)->Widht / 2;
	m_Poly->GetTexPalam(0)->Height = m_Poly->GetTexPalam(0)->Height / 2;
	float fWidht = (float)m_Poly->GetTexPalam(0)->Widht;
	float fHeight = (float)m_Poly->GetTexPalam(0)->Height;

	float fPosX = 0.0f;
	float fPosY = 0.0f;

	int nCount = 0;
	for (int y = 0; y < m_NumBrockY; y++)
	{
		for (int x = 0; x < m_NumBrockX; x++)
		{
			m_Pos[nCount].Init(0);
			m_Pos[nCount].SetCentralCoordinatesMood(true);
			m_Pos[nCount].SetPos(fPosX, fPosY);
			nCount++;
			fPosX += fWidht;
		}
		fPosX = 0.0f;
		fPosY += fHeight;
	}

	// ワールドカメラ初期化
	m_Camera->Init();
	m_Camera->Init(&Eye, &At);

	return false;
}

//==========================================================================
// 解放
void CBackground::Uninit(void)
{
	m_Camera->Uninit();
	m_Poly->Uninit();

	Delete(m_Camera);
	Delete(m_Poly);
	Delete(m_Pos);
}

//==========================================================================
// 更新
void CBackground::Update(void)
{
}

//==========================================================================
// 描画
void CBackground::Draw(void)
{
	for (int i = 0; i < (m_NumBrockX * m_NumBrockY); i++)
	{
		m_Poly->Draw(&m_Pos[i]);
	}
}
