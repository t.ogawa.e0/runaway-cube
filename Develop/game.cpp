//==========================================================================
// ゲーム[game.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "GunIncludeData.h"
#include "game.h"
#include "Hit.h"
#include "field.h"
#include "player.h"
#include "enemy.h"
#include "Time.h"
#include "Reqteyl.h"
#include "DamageEffect.h"
#include "NumberOfHits.h"
#include "DestructionRate.h"
#include "WillPower.h"
#include "Start.h"
#include "EndGame.h"
#include "Fade.h"
#include "Sky.h"
#if defined(_DEBUG) || defined(DEBUG)
#include "world.h"
#endif

CGameScene::CGameScene()
{
}

CGameScene::~CGameScene()
{
}

//==========================================================================
// 初期化
bool CGameScene::Init(void)
{
	CLight light;
	CSound::SoundLabel SoundList[] =
	{
		{ "resource/sound/Jubeat_ripples_APPEND_12.wav",-1,0.3f },
	};

	light.Init({ 1,0,0 });
	if (CField::Init()) { return true; }
	if (CSky::Init()) { return true; }
	if (CPlayer::Init()) { return true; }
	if (CEnemy::Init("resource/csv/EnemyParam.csv")) { return true; }
	if (CTime::Init(120)) { return true; }
	if (CReqteyl::Init()) { return true; }
	if (CDamageEffect::Init()) { return true; }
	if (CNumberOfHits::Init()) { return true; }
	if (CDestructionRate::Init(CEnemy::LimitNumEnemy())) { return true; }
	if (CWillPower::Init(255)) { return true; }
	if (CStartScene::Init()) { return true; }
	if (CEndGame::Init()) { return true; }
#if defined(_DEBUG) || defined(DEBUG)
	if (CWorld::Init()) { return true; }
#endif

	// サウンド
	if (this->m_Sound.Init(SoundList, 1)) { return true; }
	this->m_Sound.Volume(0, 50);
	this->m_Sound.Play(0);

	return false;
}

//==========================================================================
// 解放
void CGameScene::Uninit(void)
{
	CField::Uninit();
	CSky::Uninit();
	CPlayer::Uninit();
	CEnemy::Uninit();
	CTime::Uninit();
	CReqteyl::Uninit();
	CDamageEffect::Uninit();
	CNumberOfHits::Uninit();
	CDestructionRate::Uninit();
	CWillPower::Uninit();
	CStartScene::Uninit();
	CEndGame::Uninit();
#if defined(_DEBUG) || defined(DEBUG)
	CWorld::Uninit();
#endif
	// サウンド
	this->m_Sound.Uninit();
}

//==========================================================================
// 更新
void CGameScene::Update(void)
{
	bool bEndKey = CEndGame::Update();
	if (CStartScene::Update() && !bEndKey)
	{
		CTime::Update();
		CEnemy::Create(CTime::GetTime()->GetTime());
	}
	CSky::Update();
	CField::Update();
	CPlayer::Update();
	CEnemy::Update();
	if (!bEndKey)
	{
		CHit::GameHit();
	}
	CReqteyl::Update();
	CDamageEffect::Update();
	CNumberOfHits::Update();
	CDestructionRate::Update();
	CWillPower::Update(*CDamageEffect::Getα());

	// クリア判定
	if (CEnemy::GetDesEnemyCount() == CEnemy::LimitNumEnemy())
	{
		CEndGame::GameClear();
	}

	// タイムアップ判定
	if (CTime::TimeUpKey())
	{
		CEndGame::GameTimeUp();
	}

	// ゲームオーバー判定
	if (CWillPower::GetPercentageKay())
	{
		CEndGame::GameOver();
	}

	if (*CEndGame::GetEndKey())
	{
		// フェード処理開始フラグ
		if (!CFade::GetDraw())
		{
			CFade::In();
		}
	}
}

//==========================================================================
// 描画
void CGameScene::Draw(void)
{
	CSky::Draw();
	CField::Draw();
	CPlayer::Draw();
	CEnemy::Draw();

	// 2D
	CReqteyl::Draw(CPlayer::ViewCameraMood());
	CDamageEffect::Draw();
	CTime::Draw();
	CNumberOfHits::Draw();
	CDestructionRate::Draw();
	CWillPower::Draw();
	CStartScene::Draw();
	CEndGame::Draw();
}
