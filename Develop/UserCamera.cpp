//==========================================================================
// カメラ[UserCamera.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "SceneChange.h"
#include "UserCamera.h"
#include "GunIncludeData.h"
#include "player.h"
#include "Background.h"
#if defined(_DEBUG) || defined(DEBUG)
#include "world.h"
#include "enemy.h"
#endif

//==========================================================================
// 実体
//==========================================================================
CSceneManager::SceneName CUserCamera::m_Name;
D3DXMATRIX *CUserCamera::m_MtxView;

CUserCamera::CUserCamera()
{
}

CUserCamera::~CUserCamera()
{
}

void CUserCamera::Update(void)
{
	switch (m_Name)
	{
	case CSceneManager::SceneName::TITLE:
		Title();
		break;
	case CSceneManager::SceneName::HOME:
		Home();
		break;
	case CSceneManager::SceneName::GAME:
		Game();
		break;
	case CSceneManager::SceneName::RESULT:
		Result();
		break;
	case CSceneManager::SceneName::SCREEN_SAVER:
		Screen_Saver();
		break;
	case CSceneManager::SceneName::PRACTICE:
		Practice();
		break;
	case CSceneManager::SceneName::LOAD:
		Loat();
		break;
	default:
		break;
	}
}

void CUserCamera::Title(void)
{
	m_MtxView = CBackground::GetCamera();
	CCamera::Update(m_MtxView, CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height);
}

void CUserCamera::Home(void)
{
}

void CUserCamera::Game(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	static int nCount = (int)CameraListCame::WorldCamera;

	if (CMouse::Release(CMouse::ButtonKey::Wheel))
	{
		nCount++;
	}

	switch ((CameraListCame)nCount)
	{
	case CameraListCame::PlayerCamera:
		if (CPlayer::GetCamera() != nullptr)
		{
			m_MtxView = CPlayer::GetCamera();
		}
		else
		{
			nCount++;
		}
		break;
	case CameraListCame::WorldCamera:
		if (CWorld::GetCamera() != nullptr)
		{
			m_MtxView = CWorld::GetCamera();
		}
		else
		{
			nCount++;
		}
		break;
	case CameraListCame::Enemy:
		if (CEnemy::GetCamera() != nullptr)
		{
			m_MtxView = CEnemy::GetCamera();
		}
		else
		{
			nCount++;
		}
		break;
	default:
		nCount = 0;
		break;
	}
#else
	m_MtxView = CPlayer::GetCamera();
#endif
	CCamera::Update(m_MtxView, CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height);
}

void CUserCamera::Result(void)
{
	m_MtxView = CBackground::GetCamera();
	CCamera::Update(m_MtxView, CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height);
}

void CUserCamera::Screen_Saver(void)
{
}

void CUserCamera::Practice(void)
{
	m_MtxView = CBackground::GetCamera();
	CCamera::Update(m_MtxView, CDirectXDevice::GetWindowsSize().m_Width, CDirectXDevice::GetWindowsSize().m_Height);
}

void CUserCamera::Loat(void)
{
}
