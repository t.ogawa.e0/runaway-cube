//==========================================================================
// メイン関数[main.cpp]
// author: tatuya ogawa
//==========================================================================
#include "main.h"
#include "DirectXLibrary.h"
#include "Screen.h"
#include "resource.h"

//==========================================================================
// 実体
//==========================================================================
HINSTANCE CMain::m_hInstance;
HWND CMain::m_hWnd;

//==========================================================================
//	メイン関数
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	WNDCLASSEX wcex;	// ウインドウクラス構造体
	HWND hWnd;			// ウインドウハンドル	ウィンドウのポインタ
	MSG msg;			// メッセージ構造体		押したボタンによって表示メッセージを変えるためのもの
	RECT wr, dr; // 画面サイズ
	DWORD DNewTime = 0, DOldTime = 0;//時間格納

#if defined(_DEBUG) || defined(DEBUG)
	bool bWindowTypeKey = true;
#else
	bool bWindowTypeKey = false;
#endif

	lpCmdLine;
	hPrevInstance;

	//変数の初期化
	{
		wcex.cbSize = sizeof(WNDCLASSEX);					// 構造体のサイズ
		wcex.style = CS_VREDRAW | CS_HREDRAW;				// ウインドウスタイル
		wcex.lpfnWndProc = (WNDPROC)CMain::WndProc;			// そのウインドウのメッセージを処理するコールバック関数へのポインタ
		wcex.cbClsExtra = 0;								// ウインドウクラス構造体の後ろに割り当てる補足バイト数．普通0．
		wcex.cbWndExtra = 0;								// ウインドウインスタンスの後ろに割り当てる補足バイト数．普通0．
		wcex.hInstance = hInstance;							// このクラスのためのウインドウプロシージャがあるインスタンスハンドル．
		wcex.hIcon = LoadIcon(hInstance, (LPCSTR)IDI_ICON1);// アイコンのハンドル
		wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);		// マウスカーソルのハンドル．LoadCursor(nullptr, IDC_ARROW )とか．
		wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);	// ウインドウ背景色(背景描画用ブラシのハンドル)．
		wcex.lpszMenuName = nullptr;						// デフォルトメニュー名(MAKEINTRESOURCE(メニューID))
		wcex.lpszClassName = CMain::CLASS_NAAME;			// このウインドウクラスにつける名前
		wcex.hIconSm = LoadIcon(hInstance, (LPCSTR)IDI_ICON1);// 16×16の小さいサイズのアイコン
	}

	// マウスカーソル表示設定
	ShowCursor(bWindowTypeKey);

	//	ウインドウクラスを登録します。
	RegisterClassEx(&wcex);

	if (CDirectXDevice::Init(&hInstance, nullptr, bWindowTypeKey, 70)) { return true; }

	//デスクトップサイズ習得
	GetWindowRect(GetDesktopWindow(), &dr);

	wr = dr;

	// メイン・ウインドウ作成
	AdjustWindowRect(&wr, wcex.style, TRUE);

	wr.right = CDirectXDevice::GetWindowsSize().m_Width - wr.left;
	wr.bottom = CDirectXDevice::GetWindowsSize().m_Height - wr.top;
	wr.left = 0;
	wr.top = 0;

	//	ウインドウを作成します。
	hWnd = CreateWindowEx(0, CMain::CLASS_NAAME, CMain::WINDOW_NAMW, WINDOW_STYLE, wr.left, wr.top, wr.right, wr.bottom, nullptr, nullptr, hInstance, nullptr);

	//	ウインドウを表示します。
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	if (CMain::Init(hInstance, hWnd))
	{
		ErrorMessage(hWnd, "初期化に失敗しました");
		return-1;
	}

	timeBeginPeriod(1);//時間を正確にするもの

	//	メッセージループ
	for (;;)
	{
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))//何があってもスルーする
		{
			if (msg.message == WM_QUIT)
			{
				break;
			}
			else
			{
				//メッセージ処理
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			DNewTime = timeGetTime();
			if ((DNewTime - DOldTime) >= (1000 / 60))
			{
				if (CMain::Update())
				{
					ErrorMessage(hWnd, "初期化に失敗しました");
					return-1;
				}
				 
				//描画
				CMain::Draw();

				//時間渡し
				DOldTime = DNewTime;
			}
		}
	}
	timeEndPeriod(1);
	CMain::Uninit();

	return (int)msg.wParam;
}

CMain::CMain()
{
}

CMain::~CMain()
{
}

//==========================================================================
// ウインドウプロシージャ
LRESULT CALLBACK CMain::WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	// メッセージの種類に応じて処理を分岐します。
	switch (uMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_KEYDOWN:	// キー入力
		switch (wParam)
		{
		case VK_ESCAPE:
			// [ESC]キーが押されたら
			if (MessageBox(hWnd, "終了しますか？", "終了メッセージ", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{
				DestroyWindow(hWnd);	// ウィンドウを破棄
			}
			else
			{
				return 0;	// いいえの時
			}
		}
		break;
	case WM_LBUTTONDOWN:
		SetFocus(hWnd);
		break;
	case WM_CLOSE:	// ×ボタン押した時
		if (MessageBox(hWnd, "終了しますか？", "終了メッセージ", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{
			DestroyWindow(hWnd);	// ウィンドウを破棄
		}
		else
		{
			return 0;	// いいえの時
		}
		break;
	default:
		break;
	}

	//デフォルトの処理
	return DefWindowProc(hWnd, uMsg, wParam, lParam);

}

//==========================================================================
//	初期化
bool CMain::Init(HINSTANCE hInstance, HWND hWnd)
{
	m_hInstance = hInstance;
	m_hWnd = hWnd;

	return CScreen::Init();
}

//==========================================================================
//	終了処理
void CMain::Uninit(void)
{
	CScreen::Uninit();
}

//==========================================================================
//	更新処理
bool CMain::Update(void)
{
	return CScreen::Update();
}

//==========================================================================
//	描画処理
void CMain::Draw(void)
{
	CScreen::Draw();
}
