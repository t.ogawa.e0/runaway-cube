//==========================================================================
// マウス処理[Mouse.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Mouse.h"

//==========================================================================
// 実体
//==========================================================================
LPDIRECTINPUT8 CMouse::m_pDInput;
LPDIRECTINPUTDEVICE8 CMouse::m_pDIDevice;
DIMOUSESTATE2 CMouse::m_State;
BYTE CMouse::m_StateTrigger[(int)sizeof(DIMOUSESTATE2::rgbButtons)];
BYTE CMouse::m_StateRelease[(int)sizeof(DIMOUSESTATE2::rgbButtons)];
BYTE CMouse::m_StateRepeat[(int)sizeof(DIMOUSESTATE2::rgbButtons)];
int CMouse::m_StateRepeatCnt[(int)sizeof(DIMOUSESTATE2::rgbButtons)];
POINT CMouse::m_mousePos;
HWND CMouse::m_hWnd;

CMouse::CMouse()
{
}

CMouse::~CMouse()
{
}

//==========================================================================
// 初期化
bool CMouse::Init(HINSTANCE *hInstance, HWND *hWnd)
{
	m_pDInput = nullptr;
	m_pDIDevice = nullptr;
	m_hWnd = *hWnd;
	m_mousePos.x = m_mousePos.y = (LONG)0;

	if (FAILED(DirectInput8Create(*hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_pDInput, nullptr)))
	{
		MessageBox(*hWnd, "DirectInputオブジェクトが作れませんでした", "警告", MB_OK);
		return true;
	}

	if (FAILED(m_pDInput->CreateDevice(GUID_SysMouse, &m_pDIDevice, nullptr)))
	{
		MessageBox(*hWnd, "マウスがありませんでした。", "警告", MB_OK);
		return true;
	}

	if (m_pDIDevice != nullptr)
	{
		// マウス用のデータ・フォーマットを設定
		if (FAILED(m_pDIDevice->SetDataFormat(&c_dfDIMouse2)))
		{
			MessageBox(*hWnd, "マウスのデータフォーマットを設定できませんでした。", "警告", MB_ICONWARNING);
			return true;
		}
		
		// 協調モードを設定（フォアグラウンド＆非排他モード）
		if (FAILED(m_pDIDevice->SetCooperativeLevel(*hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND)))
		{
			MessageBox(*hWnd, "マウスの協調モードを設定できませんでした。", "警告", MB_ICONWARNING);
			return true;
		}

		if (FAILED(m_pDIDevice->EnumObjects(EnumAxesCallback, (VOID*)hWnd, DIDFT_AXIS)))
		{
			MessageBox(*hWnd, "プロパティを設定できません", "警告", MB_OK);
			return true;
		}

		if (FAILED(m_pDIDevice->Poll()))
		{
			while (m_pDIDevice->Acquire() == DIERR_INPUTLOST)
			{
				m_pDIDevice->Acquire();
			}
		}
	}

	return false;
}

//==========================================================================
// 軸のコールバック
BOOL CALLBACK CMouse::EnumAxesCallback(const DIDEVICEOBJECTINSTANCE *pdidoi, VOID *pContext)
{
	DIPROPDWORD diprop;
	UNREFERENCED_PARAMETER(pContext);

	diprop.diph.dwSize = sizeof(DIPROPDWORD);
	diprop.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	diprop.diph.dwHow = DIPH_DEVICE;
	diprop.diph.dwObj = pdidoi->dwType;
	diprop.dwData = DIPROPAXISMODE_REL; // 相対値モードで設定（絶対値はDIPROPAXISMODE_ABS）    

	if (FAILED(m_pDIDevice->SetProperty(DIPROP_AXISMODE, &diprop.diph)))
	{
		return DIENUM_STOP;
	}

	return DIENUM_CONTINUE;
}

//==========================================================================
// 終了処理
void CMouse::Uninit(void)
{
	m_hWnd = nullptr;

	if (m_pDIDevice != nullptr)
	{
		m_pDIDevice->Unacquire();
		m_pDIDevice->Release();
		m_pDIDevice = nullptr;
	}

	if (m_pDInput != nullptr)
	{
		m_pDInput->Release();
		m_pDInput = nullptr;
	}
}

//==========================================================================
// 更新処理
void CMouse::Update(void)
{
	DIMOUSESTATE2 aState;

	GetCursorPos(&m_mousePos);
	ScreenToClient(m_hWnd, &m_mousePos);

	if (m_pDIDevice != nullptr)
	{
		// デバイスからデータを取得
		if (SUCCEEDED(m_pDIDevice->GetDeviceState(sizeof(aState), &aState)))
		{
			for (int i = 0; i < (int)sizeof(DIMOUSESTATE2::rgbButtons); i++)
			{
				// トリガー・リリース情報を生成
				m_StateTrigger[i] = (m_State.rgbButtons[i] ^ aState.rgbButtons[i]) & aState.rgbButtons[i];
				m_StateRelease[i] = (m_State.rgbButtons[i] ^ aState.rgbButtons[i]) & m_State.rgbButtons[i];

				// リピート情報を生成
				if (aState.rgbButtons[i])
				{
					if (m_StateRepeatCnt[i] < m_LimitCounter)
					{
						m_StateRepeatCnt[i]++;
						if (m_StateRepeatCnt[i] == 1 || m_StateRepeatCnt[i] >= m_LimitCounter)
						{// 押し始めた最初のフレーム、または一定時間経過したらリピート情報ON
							m_StateRepeat[i] = aState.rgbButtons[i];
						}
						else
						{
							m_StateRepeat[i] = 0;
						}
					}
				}
				else
				{
					m_StateRepeatCnt[i] = 0;
					m_StateRepeat[i] = 0;
				}
				// プレス情報を保存
				m_State.rgbButtons[i] = aState.rgbButtons[i];
			}
			m_State.lX = aState.lX;
			m_State.lY = aState.lY;
			m_State.lZ = aState.lZ;
		}
		else
		{
			// アクセス権を取得
			m_pDIDevice->Acquire();
		}
	}
}

//==========================================================================
// プレス
bool CMouse::Press(ButtonKey key)
{
	if (m_pDIDevice == nullptr)
	{
		return false;
	}

	return (m_State.rgbButtons[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// トリガー
bool CMouse::Trigger(ButtonKey key)
{
	if (m_pDIDevice == nullptr)
	{
		return false;
	}

	return (m_StateTrigger[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// リピート
bool CMouse::Repeat(ButtonKey key)
{
	if (m_pDIDevice == nullptr)
	{
		return false;
	}

	return (m_StateRelease[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// リリ−ス
bool CMouse::Release(ButtonKey key)
{
	if (m_pDIDevice == nullptr)
	{
		return false;
	}

	return (m_StateRelease[(int)key] & 0x80) ? true : false;
}

//==========================================================================
// マウスの速度
CMouse::CSpeed CMouse::Speed(void)
{
	CSpeed speed;

	speed.m_lX = m_State.lX;
	speed.m_lY = m_State.lY;
	speed.m_lZ = m_State.lZ;

	return speed;
}

//==========================================================================
// カーソルの座標
POINT CMouse::WIN32Cursor(void)
{
	return m_mousePos;
}

//==========================================================================
// 左クリック
SHORT CMouse::WIN32LeftClick(void)
{
	return GetAsyncKeyState(VK_LBUTTON);
}

//==========================================================================
// 右クリック
SHORT CMouse::WIN32RightClick(void)
{
	return GetAsyncKeyState(VK_RBUTTON);
}

//==========================================================================
// マウスホイールホールド
SHORT CMouse::WIN32WheelHold(void)
{
	return GetAsyncKeyState(VK_MBUTTON);
}