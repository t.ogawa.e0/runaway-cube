//==========================================================================
// 数字処理[Number.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Number_H_
#define _Number_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "2DObject.h"
#include "2DPolygon.h"

//==========================================================================
//
// class  : CNumber
// Content: 数字
//
//==========================================================================
class CNumber
{
private:
	static constexpr int m_Pusyu_Score = 10; // 繰り上げ定数
private:
	// データリスト
	class CList
	{
	public:
		int m_Digit; // 桁
		int m_Max; // 上限
		bool m_Zero; // ゼロ描画判定
		bool m_LeftAlignment; // 左寄せ判定
	};
public:
	CNumber();
	~CNumber();
	// 初期化
	// Digit 桁
	// LeftAlignment 左寄せするかしないか
	// Zero ゼロ描画判定
	void Init(int Digit, bool LeftAlignment, bool Zero);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	float Draw(C2DPolygon * pPoly, C2DObject * pPos, int NumBer);
private:
	// max
	template <typename T> inline T Max(T a, T b);

	// min
	template <typename T> inline T Min(T a, T b);
private:
	CList m_list; // リスト
};

//==========================================================================
// max
template<typename T>
inline T CNumber::Max(T a, T b)
{
	return (((a) > (b)) ? (a) : (b));
}

//==========================================================================
// min
template<typename T>
inline T CNumber::Min(T a, T b)
{
	return (((a) < (b)) ? (a) : (b));
}

#endif // !_Number_H_
