//==========================================================================
// タイトルの名前[TitleName.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _TitleName_H_
#define _TitleName_H_

//==========================================================================
//
// class  : CTitleName
// Content: タイトルの名前
//
//==========================================================================
class CTitleName : private CTemplate
{
private:
	static constexpr int m_NumBrockX = 12;
	static constexpr int m_NumBrockY = 12;
public:
	CTitleName();
	~CTitleName();
	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static bool Update(void);
	// 描画
	static void Draw(void);
	static bool*GetKey(void) { return m_Key; }
private:
	static int * m_αCountLimit;
	static int * m_αCount;
	static bool * m_αChange;
	static bool * m_Input;
	static int * m_NumFont;
	static bool * m_Key;
	static C2DObject * m_Pos; // 座標
	static C2DPolygon * m_Poly; // ポリゴン
	static CSound * m_Sound; // サウンド
};

#endif // !_TitleName_H_
