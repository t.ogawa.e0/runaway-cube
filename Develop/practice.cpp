//==========================================================================
// チュートリアル[practice.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "GunIncludeData.h"
#include "practice.h"
#include "OperationExplanation.h"
#include "enemy.h"
#include "Fade.h"
#include "PressEnter.h"
#include "Background.h"

CPracticeScene::CPracticeScene()
{
}

CPracticeScene::~CPracticeScene()
{
}

//==========================================================================
// 初期化
bool CPracticeScene::Init(void)
{
	CLight light;
	CSound::SoundLabel SoundList[] =
	{
		{ "resource/sound/Quriora.wav",-1,1.0f },
	};

	light.Init({ 1,0,0 });
	if (COperationExplanation::Init()) { return true; }
	if (CEnemy::Init("resource/csv/EnemyParamTitle.csv")) { return true; }
	CEnemy::Create(0);
	if (CPressEnter::Init()) { return true; }
	if (CBackground::Init()) { return true; }

	// サウンド
	if (this->m_Sound.Init(SoundList, 1)) { return true; }
	this->m_Sound.Volume(0, 50);
	this->m_Sound.Play(0);

	return false;
}

//==========================================================================
// 解放
void CPracticeScene::Uninit(void)
{
	COperationExplanation::Uninit();
	CEnemy::Uninit();
	CPressEnter::Uninit();
	CBackground::Uninit();

	// サウンド
	this->m_Sound.Uninit();
}

//==========================================================================
// 更新
void CPracticeScene::Update(void)
{
	COperationExplanation::Update();
	CEnemy::Update();
	CPressEnter::Update();
	CBackground::Update();

	if (*COperationExplanation::GetKey())
	{
		// フェード処理開始フラグ
		if (!CFade::GetDraw())
		{
			CFade::In();
		}
	}
}

//==========================================================================
// 描画
void CPracticeScene::Draw(void)
{
	CBackground::Draw();
	CEnemy::Draw();
	COperationExplanation::Draw();
	CPressEnter::Draw();
}
