//==========================================================================
// タイトル[title.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "title.h"
#include "GunIncludeData.h"
#include "TitleName.h"
#include "enemy.h"
#include "Fade.h"
#include "Background.h"

CTitleScene::CTitleScene()
{
}

CTitleScene::~CTitleScene()
{
}

//==========================================================================
// 初期化
bool CTitleScene::Init(void)
{
	CLight light;
	CSound::SoundLabel SoundList[] =
	{
		{ "resource/sound/Jubeat_copious_06. electro peaceful.wav",-1,1.0f },
	};

	light.Init({ 1,0,0 });
	if (CTitleName::Init()) { return true; }
	if (CBackground::Init()) { return true; }
	if (CEnemy::Init("resource/csv/EnemyParamTitle.csv")) { return true; }
	CEnemy::Create(0);

	// サウンド
	if (this->m_Sound.Init(SoundList, 1)) { return true; }
	this->m_Sound.Volume(0, 50);
	this->m_Sound.Play(0);

	return false;
}

//==========================================================================
// 解放
void CTitleScene::Uninit(void)
{
	CTitleName::Uninit();
	CEnemy::Uninit();
	CBackground::Uninit();

	// サウンド
	this->m_Sound.Uninit();
}

//==========================================================================
// 更新
void CTitleScene::Update(void)
{
	CTitleName::Update();
	CEnemy::Update();
	CBackground::Update();

	if (*CTitleName::GetKey())
	{
		// フェード処理開始フラグ
		if (!CFade::GetDraw())
		{
			CFade::In();
		}
	}
}

//==========================================================================
// 描画
void CTitleScene::Draw(void)
{
	CBackground::Draw();
	CEnemy::Draw();
	CTitleName::Draw();
}
