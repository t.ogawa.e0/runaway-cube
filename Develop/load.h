//==========================================================================
// ロード[load.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _LOAD_H_
#define _LOAD_H_

#include"SceneChange.h"

//==========================================================================
//
// class  : CLoadScene
// Content: ロードシーン
//
//==========================================================================
class CLoadScene : public CBaseScene
{
public:
	CLoadScene();
	~CLoadScene();
	// 初期化
	bool Init(void);
	// 解放
	void Uninit(void);
	// 更新
	void Update(void);
	// 描画
	void Draw(void);
};

#endif // !_LOAD_H_
