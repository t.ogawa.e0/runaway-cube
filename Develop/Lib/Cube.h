//==========================================================================
// キューブ[Cube.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Cube_H_
#define _Cube_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include "Rectangle.h"
#include "Vertex3D.h"
#include "3DObject.h"
#include "Template.h"

//==========================================================================
//
// class  : CCube
// Content: キューブ 
//
//==========================================================================
class CCube : private CRectangle, private VERTEX_3D, private CTemplate
{
private:
	static constexpr int nBite = 256; // バイト数
	static constexpr int nMaxIndex = 36; // 
	static constexpr int nNumVertex = 24; // 頂点数
	static constexpr int nNumTriangle = 12; // 三角形の数
	static constexpr int nNumUV = 6; // UVの数
	static constexpr float VCRCT = 0.5f;
private:
	struct DrawType
	{
		enum List
		{
			MtxWorld1,
			MtxWorld2,
		};
	};

	class CUV // UV
	{
	public:
		float u0;
		float v0;
		float u1;
		float v1;
	};

	// テクスチャ情報格納
	class TEXTURE
	{
	public:
		char cFaileName[nBite];	// ファイルパス
		int Direction1; // 一行の画像数
		int Direction2; // 一列の画像数
		int Pattern; // 面の数
		int Width; // 幅
		int Height; // 高さ
		int TexCutW;
		int TexCutH;
	};

	class CParam
	{
	public:
		LPDIRECT3DTEXTURE9 m_pTexture;	// テクスチャの管理
		TEXTURE *m_aTexture; // テクスチャの情報一時格納
		D3DMATERIAL9 m_aMat;
		LPDIRECT3DVERTEXBUFFER9 m_pVertexBuffer; // バッファ
		LPDIRECT3DINDEXBUFFER9 m_pIndexBuffer; // インデックスバッファ
	};
public:
	typedef DrawType::List DrawTypeName;
private: // 格納
	CParam *m_Param;
	int m_NumData;
public:
	CCube();
	~CCube();
	// 初期化 失敗時true
	bool Init(const char** pFont, int size);
	// 解放
	void Uninit(void);
	// 描画
	void Draw(C3DObject * Input, DrawTypeName DrawType = DrawTypeName::MtxWorld2);
private:
	// キューブ生成
	void SetCube(VERTEX_4 * Output, const CUV * _this);
	// UV生成
	void SetUV(const TEXTURE * Input, CUV * Output, const int nNum);
};


#endif // !_Cube_H_
