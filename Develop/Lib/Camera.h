//==========================================================================
// カメラ[Camera.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _Camera_H_
#define _Camera_H_

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
//
// class  : CCamera
// Content: カメラ
//
//==========================================================================
class CCamera // カメラ
{
public:
	enum class VectorList
	{
		VEYE = 0, // 注視点
		VAT, // カメラ座標
		VUP, // ベクター
		VECUP, // 上ベクトル
		VECFRONT, // 前ベクトル
		VECRIGHT, //  右ベクトル
	};
private:
	D3DXMATRIX m_aMtxView; // ビュー行列
	D3DXVECTOR3 m_Eye; // 注視点
	D3DXVECTOR3 m_At; // カメラ座標
	D3DXVECTOR3 m_Eye2; // 注視点
	D3DXVECTOR3 m_At2; // カメラ座標
	D3DXVECTOR3 m_Up; // ベクター
	D3DXVECTOR3 m_VecUp; // 上ベクトル
	D3DXVECTOR3 m_VecFront; // 前ベクトル
	D3DXVECTOR3 m_VecRight; //  右ベクトル
private:
	// 平行処理
	static void CameraMoveXYZ(D3DXVECTOR3 *pVec, D3DXVECTOR3 *pVecRight, const D3DXVECTOR3 *pVecUp, const D3DXVECTOR3 *pVecFront, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pSpeed);
	// X軸回転処理
	static void CameraRangX(D3DXVECTOR3 *pDirection, D3DXMATRIX *pRot, D3DXVECTOR3 *pVecRight, const D3DXVECTOR3 *pVecUp, D3DXVECTOR3 *pVecFront, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pRang);
	// Y軸回転処理
	static void CameraRangY(D3DXVECTOR3 *pDirection, D3DXMATRIX *pRot, D3DXVECTOR3 *pVecRight, const D3DXVECTOR3 *pVecUp, D3DXVECTOR3 *pVecFront, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pRang);
	// 視点との距離計算
	static void ViewPos(D3DXVECTOR3 *pVec, D3DXVECTOR3 *pVecRight, const D3DXVECTOR3 *pVecUp, const D3DXVECTOR3 *pVecFront, D3DXVECTOR3 *pOut1, D3DXVECTOR3 *pOut2, const float *pSpeed);
	// 内積
	static bool Restriction(D3DXMATRIX pRot, D3DXVECTOR3 pVecRight, const D3DXVECTOR3 pVecUp, D3DXVECTOR3 pVecFront, const float* pRang);
public:
	CCamera();
	~CCamera();

	// 初期化
	void Init(void);

	void Init(D3DXVECTOR3 * pEye, D3DXVECTOR3 * pAt);
	// 解放
	void Uninit(void);

	// 更新
	static void Update(D3DXMATRIX *MtxView, int fWidth, int fHeight);

	// ビュー行列生成
	D3DXMATRIX *CreateView(void);

	// 視点中心にX軸回転
	void RotViewX(float Rang);

	// 視点中心にY軸回転
	void RotViewY(float Rang);

	// カメラ中心にX軸回転
	void RotCameraX(float Rang);

	// カメラ中心にY軸回転
	void RotCameraY(float Rang);

	// X軸平行移動
	void MoveX(float Speed);

	// Y軸平行移動
	void MoveY(float Speed);

	// Z軸平行移動
	void MoveZ(float Speed);

	// 視点との距離変更
	void DistanceFromView(float Distance);

	// ベクターの取得
	D3DXVECTOR3 GetVECTOR(VectorList List);

	// カメラY回転情報
	float GetRestriction(void);

	// カメラ座標をセット
	void SetCameraPos(D3DXVECTOR3 * Eye, D3DXVECTOR3 * At, D3DXVECTOR3 * Up);
	// カメラ座標
	void SetAt(D3DXVECTOR3 * At);
	// 注視点
	void SetEye(D3DXVECTOR3 * Eye);
};

#endif // !_Camera_H_
