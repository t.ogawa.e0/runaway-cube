//==========================================================================
// 気力[WillPower.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _WillPower_H_
#define _WillPower_H_

//==========================================================================
//
// class  : CWillPower
// Content: 気力計算
//
//==========================================================================
class CWillPower : private CTemplate
{
private:
	static constexpr float m_LimitPercentage = 100; // %上限
public:
	CWillPower();
	~CWillPower();
	// 初期化
	static bool Init(int nα);
	// 解放
	static void Uninit(void);
	// 更新
	static void Update(int nα);
	// 描画
	static void Draw(void);

	static bool GetPercentageKay(void) { return m_0Percentage; }

	static float GetResultPercentage(void) { return m_NowPercentage; }
private:
	static bool m_0Percentage;
	static float m_1Percentage; // 1%
	static float m_NowPercentage; // 今の%
	static C2DObject * m_Pos; // 座標
	static C2DPolygon * m_Poly; // ポリゴン
	static CNumber * m_Number; // 数字
};

#endif // !_WillPower_H_
