//==========================================================================
// メイン関数[main.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _MAIN_H_	//インクルードガード
#define _MAIN_H_

#include <Windows.h>

//==========================================================================
// マクロ定義
//==========================================================================
#define WINDOW_STYLE (WS_OVERLAPPEDWINDOW& ~(WS_THICKFRAME | WS_MAXIMIZEBOX | WS_MINIMIZEBOX))

//==========================================================================
//
// class  : CMain
// Content: メイン
//
//==========================================================================
class CMain
{
public:
	static constexpr char *CLASS_NAAME = "Runaway Cube";
	static constexpr char *WINDOW_NAMW = "Runaway Cube";
public:
	CMain();
	~CMain();
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);//ウィンドウプロシージャ
	static bool Init(HINSTANCE hInstance, HWND hWnd); //初期化	BOOL false 又は true　で全画面
	static void Uninit(void);//終了処理
	static bool Update(void);//更新
	static void Draw(void);//描画
public:
	static HINSTANCE m_hInstance;
	static HWND m_hWnd;
};

#endif // !_MAIN_H_