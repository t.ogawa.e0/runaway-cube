//==========================================================================
// シーン遷移[SceneChange.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _SCENECHANGE_H_
#define _SCENECHANGE_H_


#include <Windows.h>

//==========================================================================
// マクロ定義
//==========================================================================
#ifndef ErrorMessage
#define ErrorMessage(p,c) MessageBox((p), (c), "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING)
#endif // !ErrorMessage

//==========================================================================
//
// class  : CBaseScene
// Content: ベースとなる継承用クラス
//
//==========================================================================
class CBaseScene
{
public:
	// 初期化
	virtual bool Init(void) = 0;
	// 解放
	virtual void Uninit(void) = 0;
	// 更新
	virtual void Update(void) = 0;
	// 描画
	virtual void Draw(void) = 0;
};

//==========================================================================
//
// class  : CSceneManager
// Content: 全てのシーンの管理
//
//==========================================================================
class CSceneManager
{
private:
	// シーンのリスト
	struct SceneList
	{
		// リスト
		enum List
		{
			NOME = -1, // 無し
			TITLE, // タイトル
			HOME, // ホーム
			GAME, // ゲーム
			RESULT, // リザルト
			SCREEN_SAVER, // スクリーンセーバー
			PRACTICE, // チュートリアル
			LOAD, // ロード画面
		};
	};
public:
	// シーン名
	typedef SceneList::List SceneName;
public:
	CSceneManager();
	~CSceneManager();
	// シーンの切り替え
	static bool ChangeScene(SceneName Name);
	// 解放
	static void Uninit(void);
	// 更新
	static void Update(void);
	// 描画
	static void Draw(void);
private:
	// 初期化
	static bool Init(void);
private:
	static CBaseScene* m_pScene; // 今のシーン
};


#endif // !_SCENECHANGE_H_
