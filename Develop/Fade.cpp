//==========================================================================
// フェード[Fade.cpph]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "Fade.h"

//==========================================================================
// 実体
//==========================================================================
C2DObject *CFade::m_Pos; // 座標
C2DPolygon *CFade::m_Poly; // ポリゴン
CFade::CParam *CFade::m_Param; // パラメータ

CFade::CFade()
{
}

CFade::~CFade()
{
}

//==========================================================================
// 初期化
bool CFade::Init(void)
{
	static const char * cLinkData[] =
	{
		"resource/texture/ColorTex.png",
	};

	// メモリ確保
	New(m_Pos, Sizeof(cLinkData));
	New(m_Param, Sizeof(cLinkData));
	New(m_Poly, 1);

	// テクスチャの格納
	if (m_Poly->Init(cLinkData, Sizeof(cLinkData))) { return true; }

	// 座標の初期化
	m_Pos->Init(0);
	m_Param->m_Change = false;
	m_Param->m_Key = false;
	m_Param->m_Draw = true;
	m_Param->m_In = true;
	m_Param->m_α = 255;

	// テクスチャのサイズ変更
	m_Poly->GetTexPalam(m_Pos->m_ID)->Height = CDirectXDevice::GetWindowsSize().m_Height;
	m_Poly->GetTexPalam(m_Pos->m_ID)->Widht = CDirectXDevice::GetWindowsSize().m_Width;

	//	α値をセット
	m_Pos->SetColor(0, 0, 0, m_Param->m_α);

	return false;
}

//==========================================================================
// 解放
void CFade::Uninit(void)
{
	// テクスチャの解放
	m_Poly->Uninit();

	// メモリ解放
	Delete(m_Pos);
	Delete(m_Param);
	Delete(m_Poly);
}

//==========================================================================
// 更新
bool CFade::Update(void)
{
	// 処理実行判定
	if (m_Param->m_Key)
	{
		// フェードin,aut切り替え
		if (m_Param->m_Change)
		{
			m_Param->m_α += 5;
			if (255 < m_Param->m_α)
			{
				m_Param->m_Key = false;
				m_Param->m_In = true;
				m_Param->m_α = 255;
			}
		}
		else
		{
			m_Param->m_α -= 5;
			if (m_Param->m_α < 0)
			{
				m_Param->m_Key = false;
				m_Param->m_Draw = false;
				m_Param->m_α = 0;
			}
		}
		m_Pos->SetColor(0, 0, 0, m_Param->m_α);
	}

	return m_Param->m_Draw;
}

//==========================================================================
// 描画
void CFade::Draw(void)
{
	// 描画判定が出ている時のみ
	if (m_Param->m_Draw)
	{
		m_Poly->Draw(m_Pos);
	}
}

//==========================================================================
// フェードイン
void CFade::In(void)
{
	// in用のパラメータの初期化
	m_Param->m_Key = true;
	m_Param->m_Change = true;
	m_Param->m_Draw = true;
	m_Param->m_In = false;
	m_Param->m_α = 0;
}

//==========================================================================
// フェードアウト
void CFade::Out(void)
{
	// out用のパラメータの初期化
	m_Param->m_Key = true;
	m_Param->m_Change = false;
	m_Param->m_In = false;
	m_Param->m_α = 255;
}

//==========================================================================
// フェードイン終了判定
bool CFade::FeadInEnd(void)
{
	return m_Param->m_In; // 判定
}
