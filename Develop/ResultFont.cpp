//==========================================================================
// リザルトフォント[ResultFont.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "ResultFont.h"

//==========================================================================
// 実体
//==========================================================================
C2DObject * CResultFont::m_Pos;
C2DPolygon * CResultFont::m_poly;

CResultFont::CResultFont()
{
}

CResultFont::~CResultFont()
{
}

//==========================================================================
// 初期化
bool CResultFont::Init(void)
{
	const char * pTexLink[] =
	{
		"resource/texture/Resultフォント.png",
	};
	New(m_Pos);
	New(m_poly);

	m_Pos->Init(0);
	m_Pos->SetCentralCoordinatesMood(true);
	m_Pos->SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
	m_Pos->SetY((float)(CDirectXDevice::GetWindowsSize().m_Height *0.1f));

	return m_poly->Init(pTexLink, Sizeof(pTexLink));
}

//==========================================================================
// 解放
void CResultFont::Uninit(void)
{
	m_poly->Uninit();
	Delete(m_Pos);
	Delete(m_poly);
}

//==========================================================================
// 更新
void CResultFont::Update(void)
{
}

//==========================================================================
// 描画
void CResultFont::Draw(void)
{
	m_poly->Draw(m_Pos);
}
