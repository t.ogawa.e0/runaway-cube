//==========================================================================
// リザルトフォント[ResultFont.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _ResultFont_H_
#define _ResultFont_H_

//==========================================================================
//
// class  : CResultFont
// Content: リザルトフォント
//
//==========================================================================
class CResultFont : private CTemplate
{
public:
	CResultFont();
	~CResultFont();
	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static void Update(void);
	// 描画
	static void Draw(void);
private:
	static C2DObject * m_Pos;
	static C2DPolygon * m_poly;
};

#endif // !_ResultFont_H_
