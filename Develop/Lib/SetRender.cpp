//==========================================================================
// セットレンダー[SetRender.cpp]
// author: tatuya ogawa
//==========================================================================
#include "SetRender.h"

CSetRender::CSetRender()
{
}

CSetRender::~CSetRender()
{
}

//==========================================================================
// 減算処理
void CSetRender::SetRenderREVSUBTRACT(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);			//アルファブレンディングの有効化
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA); // αソースカラーの指定
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_REVSUBTRACT); // ブレンディング処理
	pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);			//アルファテストの無効化
}

//==========================================================================
// 半透明処理
void CSetRender::SetRenderADD(LPDIRECT3DDEVICE9 pDevice)
{
	// 半加算合成
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);			//アルファブレンディングの有効化
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);			//ブレンディングオプション加算
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);		//SRCの設定
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);	//DESTの設定
	pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);			//アルファテストの無効化
}

//==========================================================================
// アルファテスト
void CSetRender::SetRenderALPHAREF_START(LPDIRECT3DDEVICE9 pDevice, int Power)
{
	// αテストによる透明領域の切り抜き
	pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);			//アルファテストの有効化
	pDevice->SetRenderState(D3DRS_ALPHAREF, Power);					//アルファ参照値
	pDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);	//アルファテスト合格基準
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);			//アルファブレンディングの無効化
}

//==========================================================================
// アルファテスト
void CSetRender::SetRenderALPHAREF_END(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE); // αブレンドを許可
	pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE); // αテスト
}

//==========================================================================
// 加算合成
void CSetRender::SetRenderSUB(LPDIRECT3DDEVICE9 pDevice)
{
	// 全加算合成
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);		//アルファブレンディングの有効化
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);		//ブレンディングオプション加算
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);	//SRCの設定
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);		//DESTの設定
	pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);		//アルファテストの無効化
}

//==========================================================================
// 描画時にZバッファを参照するか否か
void CSetRender::SetRenderZENABLE_START(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetRenderState(D3DRS_ZENABLE, FALSE);
}

//==========================================================================
// 描画時にZバッファを参照するか否か
void CSetRender::SetRenderZENABLE_END(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetRenderState(D3DRS_ZENABLE, TRUE);
}

//==========================================================================
// Zバッファを描画するか否か
void CSetRender::SetRenderZWRITEENABLE_START(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
}

//==========================================================================
// Zバッファを描画するか否か
void CSetRender::SetRenderZWRITEENABLE_END(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
}
