//==========================================================================
// 空[Sky.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "Sky.h"

//==========================================================================
// 実体
//==========================================================================
C2DObject * CSky::m_Pos;
C2DPolygon * CSky::m_Poly;

CSky::CSky()
{
}

CSky::~CSky()
{
}

//==========================================================================
// 初期化
bool CSky::Init(void)
{
	const char * pLinkData[] =
	{
		"resource/texture/sky.jpg"
	};
	New(m_Pos);
	New(m_Poly);

	m_Pos->Init(0);

	if (m_Poly->Init(pLinkData, Sizeof(pLinkData)))
	{
		return true;
	}

	m_Poly->GetTexPalam(m_Pos->m_ID)->Widht = CDirectXDevice::GetWindowsSize().m_Width;
	m_Poly->GetTexPalam(m_Pos->m_ID)->Height = CDirectXDevice::GetWindowsSize().m_Height;

	return false;
}

//==========================================================================
// 解放
void CSky::Uninit(void)
{
	m_Poly->Uninit();

	Delete(m_Pos);
	Delete(m_Poly);
}

//==========================================================================
// 更新
void CSky::Update(void)
{
}

//==========================================================================
// 描画
void CSky::Draw(void)
{
	m_Poly->Draw(m_Pos);
}
