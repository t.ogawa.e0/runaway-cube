//==========================================================================
// DXデバッグ[DXDebug.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DXDebug.h"
#include "DXDevice.h"

//==========================================================================
// 実体
//==========================================================================
#if defined(_DEBUG) || defined(DEBUG)
int CDirectXDebug::m_FontPosX;
int CDirectXDebug::m_FontPosY;
LPD3DXFONT CDirectXDebug::m_pFont;
int CDirectXDebug::m_Width;
int CDirectXDebug::m_Height;
D3DCOLOR CDirectXDebug::m_Color;
int CDirectXDebug::m_FontSize;
#endif

CDirectXDebug::CDirectXDebug()
{
}

CDirectXDebug::~CDirectXDebug()
{
}

//==========================================================================
// 初期化
void CDirectXDebug::Init(int Width, int Height)
{
#if defined(_DEBUG) || defined(DEBUG)
	static bool bLock = false;
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();

	if (bLock == false)
	{
		D3DXCreateFont
		(
			pDevice,				// デバイス
			20,						// 文字の高さ
			0,						// 文字の幅
			FW_REGULAR,				// フォントの太さ
			0,						// MIPMAPのレベル
			FALSE,					// イタリックか？
			SHIFTJIS_CHARSET,		// 文字セット
			OUT_DEFAULT_PRECIS,		// 出力精度
			DEFAULT_QUALITY,		// 出力品質
			FIXED_PITCH | FF_SCRIPT,// フォントピッチとファミリ
			TEXT("Terminal"),		// フォント名
			&m_pFont				// Direct3Dフォントへのポインタへのアドレス
		);
		m_FontSize = 18;
		bLock = true;
		m_Width = Width;
		m_Height = Height;
	}
#else
	Decoy(&Width);
	Decoy(&Height);
#endif
}

//==========================================================================
// 解放
void CDirectXDebug::Uninit(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	static bool bLock = false;

	if (bLock == false)
	{
		Release(m_pFont);
		bLock = true;
	}
#endif
}

//==========================================================================
// 座標セット
void CDirectXDebug::PosAndColor(int x, int y, D3DCOLOR Color)
{
#if defined(_DEBUG) || defined(DEBUG)
	m_FontPosX = x;
	m_FontPosY = y;
	m_Color = Color;
#else
	Decoy(&x);
	Decoy(&y);
	Decoy(&Color);
#endif
}

//==========================================================================
// 文字描画
void CDirectXDebug::DPrintf(const char * pFormat, ...)
{
#if defined(_DEBUG) || defined(DEBUG)
	va_list argp;
	RECT rect = { m_FontPosX, m_FontPosY, m_Width, m_Height };
	char strBuf[m_Bite];

	va_start(argp, pFormat);
	vsprintf_s(strBuf, m_Bite, pFormat, argp);
	va_end(argp);
	m_pFont->DrawText(nullptr, strBuf, -1, &rect, DT_LEFT, m_Color);
#else
	Decoy(&pFormat);
#endif
}
