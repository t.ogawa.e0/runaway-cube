//==========================================================================
// カメラ[camera.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _CAERA_H_
#define _CAERA_H_

//==========================================================================
//
// class  : CUserCamera
// Content: user用カメラ
//
//==========================================================================
class CUserCamera
{
private:
	struct CameCameraList
	{
		enum LIST
		{
			PlayerCamera,
			WorldCamera,
			Enemy,
		};
	};
public:
	typedef CameCameraList::LIST CameraListCame;
public:
	CUserCamera();
	~CUserCamera();

	// 初期化
	static void Set(CSceneManager::SceneName Input) { m_Name = Input; };
	static void Update(void);
	static D3DXMATRIX * GetView(void) { return m_MtxView; }
private:
	static void Title(void);
	static void Home(void);
	static void Game(void);
	static void Result(void);
	static void Screen_Saver(void);
	static void Practice(void);
	static void Loat(void);
private:
	static CSceneManager::SceneName m_Name;
	static 	D3DXMATRIX * m_MtxView;
};

#endif // !_CAERA_H_
