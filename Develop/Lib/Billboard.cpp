//==========================================================================
// ビルボード[Billboard.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Billboard.h"
#include "DXDevice.h"

//==========================================================================
// マクロ定義
//==========================================================================
#ifndef ErrorMessage
#define ErrorMessage(p,c) MessageBox((p), (c), "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING)
#endif // !ErrorMessage

CBillboard::CBillboard()
{
}

CBillboard::~CBillboard()
{
}

//==========================================================================
// 初期化 失敗時true
bool CBillboard::Init(const char* pTex)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	WORD* pIndex;

	this->m_pIndexBuffer = nullptr;
	this->m_pVertexBuffer = nullptr;
	this->m_pTexture = nullptr;
	this->m_pAnimation = nullptr;

	if (this->LoadTex(pDevice, pTex)) { return true; }

	this->m_UV.u0 = 0.0f;
	this->m_UV.v0 = 0.0f;
	this->m_UV.u1 = 1.0f;
	this->m_UV.v1 = 1.0f;

	if (this->SetVertexBuffer(pDevice)) { return true; }

	if (this->SetIndexBuffer(pDevice)) { return true; }

	this->m_pVertexBuffer->Lock(0, 0, (void**)&this->m_pPseudo, D3DLOCK_DISCARD);
	this->CreateVertexBuffer(this->m_pPseudo);
	this->m_pVertexBuffer->Unlock();	// ロック解除

	this->m_pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
	this->RectangleIndex(pIndex, 1);
	this->m_pIndexBuffer->Unlock();	// ロック解除

	return false;
}

//==========================================================================
// アニメーション用初期化 失敗時true
bool CBillboard::Init(const char* pTex, int UpdateFrame, int Pattern, int Direction)
{
	int nCount = 0;
	D3DXIMAGE_INFO dil; // 画像データの管理
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	WORD* pIndex;

	this->m_pIndexBuffer = nullptr;
	this->m_pVertexBuffer = nullptr;
	this->m_pTexture = nullptr;
	this->m_pAnimation = nullptr;

	if (this->LoadTex(pDevice, pTex)) { return true; }
	D3DXGetImageInfoFromFile(pTex, &dil);
	this->New(this->m_pAnimation);

	this->m_pAnimation->m_TexSize.Widht = dil.Width;
	this->m_pAnimation->m_TexSize.Height = dil.Height;
	this->m_pAnimation->Frame = UpdateFrame;
	this->m_pAnimation->Pattern = Pattern;
	this->m_pAnimation->Direction = Direction;
	this->m_pAnimation->m_Cut.w = (float)this->m_pAnimation->m_TexSize.Widht / this->m_pAnimation->Direction;
	for (int i = 0;; i += this->m_pAnimation->Direction)
	{
		if (this->m_pAnimation->Pattern <= i) { break; }
		nCount++;
	}
	this->m_pAnimation->m_Cut.h = (float)this->m_pAnimation->m_TexSize.Height / nCount;
	this->m_pAnimation->m_Cut.x = this->m_pAnimation->m_Cut.y = 0.0f;

	if (this->SetVertexBuffer(pDevice)) { return true; }

	if (this->SetIndexBuffer(pDevice)) { return true; }

	this->m_pVertexBuffer->Lock(0, 0, (void**)&this->m_pPseudo, D3DLOCK_DISCARD);
	this->CreateVertexBuffer(this->m_pPseudo);
	this->m_pVertexBuffer->Unlock();	// ロック解除

	this->m_pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
	this->RectangleIndex(pIndex, 1);
	this->m_pIndexBuffer->Unlock();	// ロック解除

	return false;
}

//==========================================================================
// 解放
void CBillboard::Uninit(void)
{
	this->Release(this->m_pTexture);

	// バッファ解放
	this->Release(this->m_pVertexBuffer);

	// インデックスバッファ解放
	this->Release(this->m_pIndexBuffer);

	this->Delete(this->m_pAnimation);
}

//==========================================================================
// 描画
void CBillboard::Draw(C3DObject * pInput, D3DXMATRIX *MtxView, int * AnimationCount, bool bADD)
{
	D3DXMATRIX aMtxTrans; // ワールド行列
	D3DXMATRIX aMtxWorld = *MtxView; // ワールド行列
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();

	//	D3DXMatrixInverse();
	//float fMat_12 = aMtxWorld2._12;
	//float fMat_23 = aMtxWorld2._23;
	//float fMat_21 = aMtxWorld2._21;
	//float fMat_32 = aMtxWorld2._32;
	D3DXMatrixTranspose(&aMtxWorld, &aMtxWorld);
	aMtxWorld._14 = 0.0f;
	aMtxWorld._24 = 0.0f;
	aMtxWorld._34 = 0.0f;

	//aMtxWorld2._12 = fMat_12;
	//aMtxWorld2._23 = fMat_23;
	//aMtxWorld2._21 = fMat_21;
	//aMtxWorld2._32 = fMat_32;

	//aMtxWorld2._12; aMtxWorld2._21;
	//aMtxWorld2._23; aMtxWorld2._32; // 転置しなければ垂直方向は無視

	if (AnimationCount != nullptr&&this->m_pAnimation != nullptr)
	{
		this->AnimationPalam(AnimationCount);
		this->UV();
		this->m_pVertexBuffer->Lock(0, 0, (void**)&this->m_pPseudo, D3DLOCK_DISCARD);
		this->m_pPseudo[0].Tex = D3DXVECTOR2(this->m_UV.u0, this->m_UV.v0);
		this->m_pPseudo[1].Tex = D3DXVECTOR2(this->m_UV.u1, this->m_UV.v0);
		this->m_pPseudo[2].Tex = D3DXVECTOR2(this->m_UV.u1, this->m_UV.v1);
		this->m_pPseudo[3].Tex = D3DXVECTOR2(this->m_UV.u0, this->m_UV.v1);
		this->m_pVertexBuffer->Unlock();	// ロック解除
	}

	if (pInput->Collar != nullptr)
	{
		this->m_pPseudo[0].color = D3DCOLOR_RGBA(pInput->Collar->r, pInput->Collar->g, pInput->Collar->b, pInput->Collar->α);
		this->m_pPseudo[1].color = D3DCOLOR_RGBA(pInput->Collar->r, pInput->Collar->g, pInput->Collar->b, pInput->Collar->α);
		this->m_pPseudo[2].color = D3DCOLOR_RGBA(pInput->Collar->r, pInput->Collar->g, pInput->Collar->b, pInput->Collar->α);
		this->m_pPseudo[3].color = D3DCOLOR_RGBA(pInput->Collar->r, pInput->Collar->g, pInput->Collar->b, pInput->Collar->α);
	}

	// サイズ
	pDevice->SetStreamSource(0, this->m_pVertexBuffer, 0, sizeof(VERTEX_4));	// パイプライン

	pDevice->SetIndices(this->m_pIndexBuffer);

	// FVFの設定
	pDevice->SetFVF(FVF_VERTEX_4);

	pDevice->SetTexture(0, nullptr);
	pDevice->SetTexture(0, this->m_pTexture);

	if (bADD)
	{
		this->SetRenderSUB(pDevice);
	}
	else
	{
		this->SetRenderALPHAREF_START(pDevice, 100);//アルファ画像によるブレンド
	}

	// ライト設定
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	// 各種行列の設定
	D3DXMatrixTranslation(&aMtxTrans, pInput->Info.pos.x, pInput->Info.pos.y, pInput->Info.pos.z); // 座標変更
	D3DXMatrixMultiply(&aMtxWorld, &aMtxWorld, &aMtxTrans); // 行列の合成
	pDevice->SetTransform(D3DTS_WORLD, &aMtxWorld);

	// 描画設定
	pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);

	if (bADD)
	{
		this->SetRenderADD(pDevice);
		this->SetRenderALPHAREF_END(pDevice);
	}
	else
	{
		this->SetRenderALPHAREF_END(pDevice);
	}
}

//==========================================================================
// テクスチャの読み込み
bool CBillboard::LoadTex(LPDIRECT3DDEVICE9 pDevice, const char * pTex)
{
	if (FAILED(D3DXCreateTextureFromFile(pDevice, pTex, &this->m_pTexture)))
	{
		ErrorMessage(nullptr, "テクスチャの読み込み失敗");
		return true;
	}

	return false;
}

//==========================================================================
// バーテックスバッファのセット
bool CBillboard::SetVertexBuffer(LPDIRECT3DDEVICE9 pDevice)
{
	if (FAILED(pDevice->CreateVertexBuffer(sizeof(VERTEX_4) * 4, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &this->m_pVertexBuffer, nullptr)))
	{
		ErrorMessage(nullptr, "頂点バッファが作れませんでした。");
		return true;
	}

	return false;
}

//==========================================================================
// インデックスバッファのセット
bool CBillboard::SetIndexBuffer(LPDIRECT3DDEVICE9 pDevice)
{
	if (FAILED(pDevice->CreateIndexBuffer(sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &this->m_pIndexBuffer, nullptr)))
	{
		ErrorMessage(nullptr, "インデックスバッファが作れませんでした。");
		return true;
	}

	return false;
}

//==========================================================================
// 頂点バッファの生成
void CBillboard::CreateVertexBuffer(VERTEX_4 * Output)
{
	VERTEX_4 vVertex[] =
	{
		// 手前
		{ D3DXVECTOR3(-VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(this->m_UV.u0,this->m_UV.v0) }, // 左上
		{ D3DXVECTOR3(VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(this->m_UV.u1,this->m_UV.v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(this->m_UV.u1,this->m_UV.v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(this->m_UV.u0,this->m_UV.v1) }, // 左下
	};

	for (int i = 0; i < (int)this->Sizeof(vVertex); i++)
	{
		Output[i] = vVertex[i];
	}
}

//==========================================================================
// アニメーション情報のセット
void CBillboard::AnimationPalam(int * AnimationCount)
{
	(*AnimationCount)++;
	int PattanNum = ((*AnimationCount) / this->m_pAnimation->Frame) % this->m_pAnimation->Pattern;	// フレームに１回	パターン数
	int patternV = PattanNum % this->m_pAnimation->Direction; // 横方向のパターン
	int patternH = PattanNum / this->m_pAnimation->Direction; // 縦方向のパターン

	this->m_pAnimation->m_Cut.x = patternV * this->m_pAnimation->m_Cut.w; // 切り取り座標X
	this->m_pAnimation->m_Cut.y = patternH * this->m_pAnimation->m_Cut.h; // 切り取り座標Y
}

//==========================================================================
// アニメーション切り替わり時の判定 切り替え時true 
bool CBillboard::GetPattanNum(int * AnimationCount)
{
	if (AnimationCount != nullptr)
	{
		if ((*AnimationCount) == (this->m_pAnimation->Frame*this->m_pAnimation->Pattern) - 1)
		{
			this->AnimationCountInit(AnimationCount);
			return true;
		}
	}
	return false;
}

//==========================================================================
// アニメーション用のカウンタの初期化 引数はカウンタ用変数
int *CBillboard::AnimationCountInit(int * AnimationCount)
{
	if (AnimationCount != nullptr)
	{
		(*AnimationCount) = -1;
	}

	return AnimationCount;
}

//==========================================================================
// UVの生成
void CBillboard::UV(void)
{
	// 左上
	this->m_UV.u0 = (float)this->m_pAnimation->m_Cut.x / this->m_pAnimation->m_TexSize.Widht;
	this->m_UV.v0 = (float)this->m_pAnimation->m_Cut.y / this->m_pAnimation->m_TexSize.Height;

	// 左下
	this->m_UV.u1 = (float)(this->m_pAnimation->m_Cut.x + this->m_pAnimation->m_Cut.w) / this->m_pAnimation->m_TexSize.Widht;
	this->m_UV.v1 = (float)(this->m_pAnimation->m_Cut.y + this->m_pAnimation->m_Cut.h) / this->m_pAnimation->m_TexSize.Height;
}
