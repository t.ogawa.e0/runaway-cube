//==========================================================================
// キューブ[Cube.cpp]
// author: tatuya ogawa
//==========================================================================
#include "Cube.h"
#include "DXDevice.h"

//==========================================================================
// マクロ定義
//==========================================================================
#ifndef ErrorMessage
#define ErrorMessage(p,c) MessageBox((p), (c), "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING)
#endif // !ErrorMessage

CCube::CCube()
{
}

CCube::~CCube()
{
}

//==========================================================================
// 初期化 失敗時true
bool CCube::Init(const char** pFont, int size)
{
	CUV aUv[nNumUV]; // 各面のUV
	VERTEX_4* pPseudo;// 頂点バッファのロック
	WORD* pIndex;
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	CParam * pData = nullptr;

	this->m_NumData = size;
	this->New(this->m_Param, size);

	for (int i = 0; i < this->m_NumData; i++)
	{
		D3DXIMAGE_INFO dil; // 画像データの管理

		pData = &this->m_Param[i];
		pPseudo = nullptr;// 頂点バッファのロック
		pIndex = nullptr;
		pData->m_pVertexBuffer = nullptr;
		pData->m_pIndexBuffer = nullptr;
		pData->m_pTexture = nullptr;
		pData->m_aTexture = nullptr;

		this->New(pData->m_aTexture, 1);

		pData->m_aTexture->cFaileName[0] = 0;
		strcat(pData->m_aTexture->cFaileName, pFont[i]);
		pData->m_aTexture->Direction1 = 4;
		pData->m_aTexture->Direction2 = 4;
		pData->m_aTexture->Pattern = 6;

		// 画像データの格納
		D3DXGetImageInfoFromFile(pData->m_aTexture->cFaileName, &dil);
		pData->m_aTexture->Height = dil.Height;
		pData->m_aTexture->Width = dil.Width;
		pData->m_aTexture->TexCutW = pData->m_aTexture->Width / pData->m_aTexture->Direction1;
		pData->m_aTexture->TexCutH = pData->m_aTexture->Height / pData->m_aTexture->Direction2;

		if (FAILED(D3DXCreateTextureFromFile(pDevice, pData->m_aTexture->cFaileName, &pData->m_pTexture)))
		{
			ErrorMessage(nullptr, "テクスチャの読み込み失敗");
			return true;
		}

		for (int s = 0; s < pData->m_aTexture->Pattern; s++)
		{
			this->SetUV(pData->m_aTexture, &aUv[s], s);
		}

		this->Delete(pData->m_aTexture);

		if (FAILED(pDevice->CreateVertexBuffer(sizeof(VERTEX_4) * this->nNumVertex, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &pData->m_pVertexBuffer, nullptr)))
		{
			ErrorMessage(nullptr, "頂点バッファが作れませんでした。");
			return true;
		}

		if (FAILED(pDevice->CreateIndexBuffer(sizeof(WORD) * this->nMaxIndex, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &pData->m_pIndexBuffer, nullptr)))
		{
			ErrorMessage(nullptr, "インデックスバッファが作れませんでした。");
			return true;
		}

		pData->m_pVertexBuffer->Lock(0, 0, (void**)&pPseudo, D3DLOCK_DISCARD);
		this->SetCube(pPseudo, aUv);
		pData->m_pVertexBuffer->Unlock();	// ロック解除

		pData->m_pIndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
		this->RectangleIndex(pIndex, this->nMaxIndex / this->m_NumDfaltIndex);
		pData->m_pIndexBuffer->Unlock();	// ロック解除

		// マテリアルの設定
		ZeroMemory(&pData->m_aMat, sizeof(pData->m_aMat));
		pData->m_aMat.Diffuse.r = 1.0f; // ディレクショナルライト
		pData->m_aMat.Diffuse.g = 1.0f; // ディレクショナルライト
		pData->m_aMat.Diffuse.b = 1.0f; // ディレクショナルライト
		pData->m_aMat.Diffuse.a = 1.0f; // ディレクショナルライト
		pData->m_aMat.Ambient.r = 0.5f; // アンビエントライト
		pData->m_aMat.Ambient.g = 0.5f; // アンビエントライト
		pData->m_aMat.Ambient.b = 0.5f; // アンビエントライト
		pData->m_aMat.Ambient.a = 1.0f; // アンビエントライト
	}

	return false;
}

//==========================================================================
// 解放
void CCube::Uninit(void)
{
	CParam * pData = nullptr;

	for (int i = 0; i < this->m_NumData; i++)
	{
		pData = &this->m_Param[i];

		// バッファ解放
		this->Release(pData->m_pVertexBuffer);

		// インデックスバッファ解放
		this->Release(pData->m_pIndexBuffer);

		this->Release(pData->m_pTexture);

		this->Delete(pData->m_aTexture);
	}

	this->Delete(this->m_Param);
}

//==========================================================================
// 描画
void CCube::Draw(C3DObject * Input, DrawTypeName DrawType)
{
	LPDIRECT3DDEVICE9 pDevice = CDirectXDevice::GetD3DDevice();
	D3DXMATRIX aMtxWorld; // ワールド行列
	CParam * pData = nullptr;

	pData = &this->m_Param[Input->ID];

	// サイズ
	pDevice->SetStreamSource(0, pData->m_pVertexBuffer, 0, sizeof(VERTEX_4));	// パイプライン

	pDevice->SetIndices(pData->m_pIndexBuffer);

	// FVFの設定
	pDevice->SetFVF(FVF_VERTEX_4);

	pDevice->SetTexture(0, nullptr);
	pDevice->SetTexture(0, pData->m_pTexture);

	// ライト設定
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	// マテリアルの設定
	pDevice->SetMaterial(&pData->m_aMat);

	switch (DrawType)
	{
	case CCube::DrawType::MtxWorld1:
		pDevice->SetTransform(D3DTS_WORLD, Input->CreateMtxWorld1(&aMtxWorld));
		break;
	case CCube::DrawType::MtxWorld2:
		pDevice->SetTransform(D3DTS_WORLD, Input->CreateMtxWorld2(&aMtxWorld));
		break;
	default:
		pDevice->SetTransform(D3DTS_WORLD, Input->CreateMtxWorld2(&aMtxWorld));
		break;
	}

	// 描画設定
	pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, this->nNumVertex, 0, this->nNumTriangle);

}

//==========================================================================
// キューブ生成	
void CCube::SetCube(VERTEX_4 * Output, const CUV * _this)
{
	VERTEX_4 vVertex[] =
	{
		// 手前
		{ D3DXVECTOR3(-VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u0,_this[0].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u1,_this[0].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u1,_this[0].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u0,_this[0].v1) }, // 左下

		// 奥
		{ D3DXVECTOR3(-VCRCT,-VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u0,_this[1].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,-VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u1,_this[1].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT, VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u1,_this[1].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT, VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u0,_this[1].v1) }, // 左下

		// 右
		{ D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u0,_this[2].v0) }, // 左上
		{ D3DXVECTOR3(-VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u1,_this[2].v0) }, // 右上
		{ D3DXVECTOR3(-VCRCT, VCRCT, VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u1,_this[2].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u0,_this[2].v1) }, // 左下

		// 左
		{ D3DXVECTOR3(VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u0,_this[3].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT, VCRCT, VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u1,_this[3].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u1,_this[3].v1) }, // 右下
		{ D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u0,_this[3].v1) }, // 左下

		// 上
		{ D3DXVECTOR3(-VCRCT,VCRCT, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u0,_this[4].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,VCRCT, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u1,_this[4].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,VCRCT,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u1,_this[4].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,VCRCT,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u0,_this[4].v1) }, // 左下

		// 下
		{ D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u0,_this[5].v0) }, // 左上
		{ D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u1,_this[5].v0) }, // 右上
		{ D3DXVECTOR3(VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u1,_this[5].v1) }, // 右下
		{ D3DXVECTOR3(-VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u0,_this[5].v1) }, // 左下
	};

	for (int i = 0; i < (int)(sizeof(vVertex) / sizeof(VERTEX_4)); i++) { Output[i] = vVertex[i]; }
}

//==========================================================================
// UV生成
void CCube::SetUV(const TEXTURE * Input, CUV * Output, const int nNum)
{
	int patternNum = (nNum / 1) % Input->Pattern; //2フレームに１回	%10=パターン数
	int patternV = patternNum % Input->Direction1; //横方向のパターン
	int patternH = patternNum / Input->Direction2; //縦方向のパターン
	int nTcx = patternV * Input->TexCutW; //切り取りサイズ
	int nTcy = patternH * Input->TexCutH; //切り取りサイズ

	Output->u0 = (float)nTcx / Input->Width;// 左
	Output->v0 = (float)nTcy / Input->Height;// 上
	Output->u1 = (float)(nTcx + Input->TexCutW) / Input->Width;// 右
	Output->v1 = (float)(nTcy + Input->TexCutH) / Input->Height;//下
}
