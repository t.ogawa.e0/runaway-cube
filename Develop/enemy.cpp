//==========================================================================
// エネミー[enemy.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "GunIncludeData.h"
#include "game.h"
#include "enemy.h"
#include "SceneChange.h"
#include "UserCamera.h"

#include "player.h"

//==========================================================================
// 実体
//==========================================================================
CEnemy::C_AI *CEnemy::m_pStartAddress; // 先頭アドレス
CCube * CEnemy::m_Data; // キューブのデータ
CXmodel * CEnemy::m_MdelData; // 核
CGun * CEnemy::m_enemygun; // エネミーの銃
CShadow *CEnemy::m_Shadow; // 影
int CEnemy::m_NumData; // データ数
int CEnemy::m_DesEnemyCount; // 消えたエネミーのcount
int CEnemy::m_IDCount; // IDカウンタ
int CEnemy::m_EnemyParamDataCount;
int CEnemy::m_LimitEnemyParamDataCount;
CEnemy::CEnemyParamData *CEnemy::m_ParamData;

CEnemy::CEnemy()
{
}

CEnemy::~CEnemy()
{
}

//==========================================================================
// 初期化
bool CEnemy::Init(const char * pCSVFile)
{
	const char* LinkData[] =
	{
		"resource/3DModel/核.blend.x"
	};
	const char* MosinLinkData[] =
	{
		"resource/3DModel/MosinNagantM1891-30SniperTHEEND.x"
	};
	const char* BarrettLinkData[] =
	{
		"resource/3DModel/BarrettEfectEnemy.x"
	};
	const char * EnemyTex[] =
	{
		"resource/texture/EnemyBox.png"
	};

	C3DObject Pos;

	// メモリ確保
	m_ParamData = nullptr;
	m_pStartAddress = nullptr;
	m_LimitEnemyParamDataCount = 0;
	m_DesEnemyCount = 0;
	New(m_Data);
	New(m_MdelData);
	New(m_enemygun);
	New(m_Shadow);
	CSVFileOpen(pCSVFile);

	m_EnemyParamDataCount = 0;
	m_NumData = 0;
	m_IDCount = 0;

	// モデルの確保
	if (m_MdelData->Init(LinkData, Sizeof(LinkData))) { return true; }

	// 各データの確保
	if (m_enemygun->Init(MosinLinkData)) { return true; }
	if (m_enemygun->InitBarrett(BarrettLinkData, Sizeof(BarrettLinkData)) ){ return true; }
	if (m_enemygun->InitEffect("resource/texture/explosionA-color_10F_xp.png", 2, 10, 5)) { return true; }
	if (m_Shadow->Init("resource/texture/shadow000.jpg")) { return true; }

	// 武器の描画無効
	m_enemygun->DrawJudgment(false);

	return m_Data->Init(EnemyTex, Sizeof(EnemyTex));
}

//==========================================================================
// 解放
void CEnemy::Uninit(void)
{
	C_AI *pData = m_pStartAddress; // 初期アドレスを記憶
	C_AI *pNextDelete = nullptr; // 次のアドレス

	// 解放処理
	m_MdelData->Uninit();
	m_enemygun->Uninit();
	m_Data->Uninit();
	m_Shadow->Uninit();

	Delete(m_MdelData);
	Delete(m_enemygun);
	Delete(m_Data);
	Delete(m_ParamData);
	Delete(m_Shadow);

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// 次のアドレスを記憶
		pNextDelete = pData->m_pNext;

		// 解放
		pData->Uninit();

		// メモリ解放
		Delete(pData);
		m_NumData--;

		pData = pNextDelete;

		// データ数が0の時
		if (m_NumData == 0)
		{
			m_pStartAddress = nullptr;
			m_IDCount = 0;
		}
	}
}

//==========================================================================
// 更新
void CEnemy::Update(void)
{
	// 先頭アドレスを記憶
	C_AI *pData = m_pStartAddress;

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// 更新
		pData->Update();

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}

	PinpointDelete();
	m_enemygun->Update();
}

//==========================================================================
// 描画
void CEnemy::Draw(void)
{
	// 先頭アドレスを記憶
	C_AI *pData = m_pStartAddress;

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// 描画
		pData->Draw();

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}
	
	if (m_pStartAddress != nullptr)
	{
		m_enemygun->Draw(CUserCamera::GetView());
	}
}

//==========================================================================
// 生成
void CEnemy::Create(int time)
{
	if (m_EnemyParamDataCount != m_LimitEnemyParamDataCount)
	{
		for (int i = m_EnemyParamDataCount; i < m_LimitEnemyParamDataCount; i++)
		{
			if (m_ParamData[i].m_Time == time)
			{
				C3DObject Pos;
				C_AI *pMain = nullptr; // 生成時のアドレス
				C_AI *pData = nullptr; // 次のアドレス

				m_EnemyParamDataCount++;
				m_NumData++;
				m_IDCount++;

				// メモリを確保
				New(pMain);
				pMain->m_pNext = nullptr;
				pMain->m_pBack = nullptr;
				pMain->m_ID = m_IDCount;

				Pos.Init();
				Pos.Info.pos.x = m_ParamData[i].m_x;
				Pos.Info.pos.y = m_ParamData[i].m_y;
				Pos.Info.pos.z = m_ParamData[i].m_z;

				// 初期化
				pMain->Init(&Pos, m_ParamData[i].m_Type);

				// 先頭アドレスがなかった場合
				if (m_pStartAddress == nullptr)
				{
					m_pStartAddress = pMain;
				}
				else if (m_pStartAddress != nullptr)
				{
					// 先頭アドレスを記憶
					pData = m_pStartAddress;

					// アドレスがnullではない限り検索を繰り返す
					for (;;)
					{
						// 次のアドレスがnullの場合
						if (pData->m_pNext == nullptr)
						{
							break;
						}

						// 検索をするアドレスを記憶
						pData = pData->m_pNext;
					}

					// 次のアドレスを記憶
					pData->m_pNext = pMain;

					// ひとつ前のアドレスに記憶
					pMain->m_pBack = pData;
				}
			}
		}
	}
}

void CEnemy::PinpointDelete(const int * ID)
{
	C_AI *pData = m_pStartAddress; // 先頭アドレスを記憶

	// 解放対象が見つかるまで検索を続ける
	for (;;)
	{
		// アドレスが無ければ終了
		if (pData == nullptr)
		{
			break;
		}

		// 検索IDとヒット
		if (pData->m_ID == (*ID))
		{
			if (!pData->GetDestruction())
			{
				pData->SetDestruction();
			}
			break; // 終了
		}

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}
}

//==========================================================================
// 特定のデータ消去
void CEnemy::PinpointDelete(void)
{
	C_AI *pData = m_pStartAddress; // 先頭アドレスを記憶
	C_AI *pBack = nullptr; // 前のデータ
	C_AI *pNext = nullptr; // 次のデータ

	// 解放対象が見つかるまで検索を続ける
	for (;;)
	{
		// アドレスが無ければ終了
		if (pData == nullptr)
		{
			break;
		}

		// 消去判定がヒット
		if (pData->GetDestruction() == true)
		{
			// 先頭アドレスと検索IDが同じ場合先頭アドレスを書き換える
			if (m_pStartAddress->m_ID == pData->m_ID)
			{
				m_pStartAddress = pData->m_pNext;
			}

			pBack = pData->m_pBack; // ひとつ前のアドレスを記憶
			pNext = pData->m_pNext; // 次のアドレスを記憶

			// アドレスがある場合次のアドレスをつなぎなおす
			if (pBack != nullptr)
			{
				pBack->m_pNext = pNext;
			}

			// アドレスがある場合ひとつ前のアドレスをつなぎなおす
			if (pNext != nullptr)
			{
				pNext->m_pBack = pBack;
			}

			// 解放
			pData->Uninit();

			// メモリ解放
			Delete(pData);
			m_NumData--;
			m_DesEnemyCount++;

			// データ数が0の時
			if (m_NumData == 0)
			{
				m_pStartAddress = nullptr;
				m_IDCount = 0;
			}

			break; // 終了
		}

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}
}

// csvファイル読み込み
bool CEnemy::CSVFileOpen(const char * pCSVFile)
{
	FILE *fp;
	char cText[256] = { 0, };
	int ret = 0;
	int nCount = 0;

	fp = fopen(pCSVFile, "r");
	if (fp)
	{
		// 行数カウント
		for (;;)
		{
			cText[0] = 0;
			ret = fscanf(fp, "%s", &cText);
			if (ret == EOF) { break; }
			nCount++;
		}
		fclose(fp);

		// データ数記憶
		m_LimitEnemyParamDataCount = nCount - 1;

		// メモリ確保
		New(m_ParamData, m_LimitEnemyParamDataCount);

		fp = fopen(pCSVFile, "r");
		if (fp)
		{
			// 一行読み飛ばし
			fscanf(fp, "%s", cText);
			for (int i = 0; i < m_LimitEnemyParamDataCount; i++)
			{
				fscanf(
					fp, "%[^,],%d,%f,%f,%f,%d",
					cText,
					&m_ParamData[i].m_Time,
					&m_ParamData[i].m_x,
					&m_ParamData[i].m_y,
					&m_ParamData[i].m_z,
					&m_ParamData[i].m_Type
				);
			}
			fclose(fp);
		}
	}
	else
	{
		ErrorMessage(nullptr, "ファイルが開けません");
		return true;
	}

	return false;
}

//==========================================================================
// ビューカメラのゲッター
D3DXMATRIX * CEnemy::GetCamera(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	if (m_pStartAddress == nullptr)
	{
		return nullptr;
	}
	else
	{
		return m_pStartAddress->GetCamera();
	}
#else
	return nullptr;
#endif
}

//==========================================================================
// ゲッター データがない場合null
CEnemy::C_AI * CEnemy::GetData(const int * Count)
{
	C_AI *pData = m_pStartAddress; // 先頭アドレスを記憶

	// カウンタの回数繰り返す
	for (int i = 0; i < (*Count); i++)
	{
		// アドレスが無ければ終了
		if (pData == nullptr)
		{
			break;
		}

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}

	return pData;
}

//==========================================================================
// カメラ
void CEnemy::C_AI::Camera(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	C3DObject Eye = this->m_Pos;
	C3DObject At = this->m_Pos;

	this->m_Camera->Init();
	Eye.Info.pos.y += 1;
	At = Eye;
	Eye.MoveZ(-5);
	this->m_Camera->Init(&Eye.Info.pos, &At.Info.pos);
	this->m_Camera->SetEye(&this->m_Pos.Info.pos);
	this->m_Camera->SetAt(&this->m_Pos.Info.pos);
#endif
}

//==========================================================================
// カメラの動く処理
void CEnemy::C_AI::MoveCamera(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	this->m_Camera->SetEye(&this->m_Pos.Info.pos);
	this->m_Camera->SetAt(&this->m_Pos.Info.pos);
	this->m_Camera->RotViewX(0.001f);
#endif
}

//==========================================================================
// モード
void CEnemy::C_AI::Move(void)
{
	// まだ存在しているとき
	if (this->m_Existence == true)
	{
		this->m_PosBox1.Info.pos = this->m_Pos.Info.pos;
		this->m_PosBox2.Info.pos = this->m_Pos.Info.pos;
		this->m_GunPos.Info.pos = this->m_Pos.Info.pos;

		this->m_PosBox1.Info.rot.x += 0.01f;
		this->m_PosBox1.Info.rot.z += 0.02f;
		this->m_PosBox1.Info.rot.y += 0.005f;

		this->m_PosBox2.Info.rot.x -= 0.01f;
		this->m_PosBox2.Info.rot.z -= 0.02f;
		this->m_PosBox2.Info.rot.y -= 0.005f;
	}
}

//==========================================================================
// 出現処理
void CEnemy::C_AI::Playback(void)
{
	if (this->m_Playback == true)
	{
		D3DXVECTOR3 Limit1 = D3DXVECTOR3(0.5f, 0.5f, 0.5f);
		D3DXVECTOR3 Limit2 = D3DXVECTOR3(1.0f, 1.0f, 1.0f);

		this->m_Hit = false;
		this->m_Existence = true;

		this->m_PosBox1.Scale(0.05f);

		if (Limit2.z < this->m_PosBox1.Info.sca.z)
		{
			this->m_PosBox1.Info.sca = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
			this->m_PosBox2.Scale(0.05f);

			if (Limit1.z < this->m_PosBox2.Info.sca.z)
			{
				this->m_PosBox2.Info.sca = D3DXVECTOR3(0.5f, 0.5f, 0.5f);
				this->m_Playback = false;
			}
		}
	}
}

//==========================================================================
// 消滅
void CEnemy::C_AI::Destruction(void)
{
	if (this->m_Existence == true && this->m_Hit == true)
	{
		D3DXVECTOR3 Limit = D3DXVECTOR3(0, 0, 0);

		this->m_Playback = false;

		this->m_Magnification += this->m_Magnification;

		this->m_PosBox1.Info.rot.x += 0.01f*this->m_Magnification;
		this->m_PosBox1.Info.rot.z += 0.02f*this->m_Magnification;
		this->m_PosBox1.Info.rot.y += 0.005f*this->m_Magnification;

		this->m_PosBox2.Info.rot.x -= 0.01f*this->m_Magnification;
		this->m_PosBox2.Info.rot.z -= 0.02f*this->m_Magnification;
		this->m_PosBox2.Info.rot.y -= 0.005f*this->m_Magnification;

		this->m_PosBox1.Scale(-0.05f);
		this->m_PosBox2.Scale(-0.05f);

		if (this->m_PosBox1.Info.sca.z < Limit.z)
		{
			this->m_PosBox1.Info.sca = D3DXVECTOR3(0, 0, 0);
			this->m_Destruction = true;
			this->m_Existence = false;
			this->m_Magnification = 1.0f;
		}

		if (this->m_PosBox2.Info.sca.z < Limit.z)
		{
			this->m_PosBox2.Info.sca = D3DXVECTOR3(0, 0, 0);
		}
	}
}

//==========================================================================
// エネミーの銃
void CEnemy::C_AI::EnemyGun(void)
{
	if (this->m_Existence == true)
	{
		float fSpped = 1.0f;
		m_enemygun->SetPos(&this->m_GunPos);
		if (this->m_Attak)
		{
			this->m_AttakFrame++;

			if (this->m_AttakFrame == 5)
			{
				m_enemygun->Trigger(true, &fSpped);
				this->m_AttakFrame = 0;
			}
		}
	}
}

//==========================================================================
// 初期化
void CEnemy::C_AI::Init(C3DObject * pPos, TypeList type)
{
	// 親座標の設定
	this->m_Pos = *pPos;

	// 親座標の初期化
	this->m_Camera = nullptr;
#if defined(_DEBUG) || defined(DEBUG)
	// デバッグ用メモリ確保
	this->New(this->m_Camera);
	this->m_Camera->Init();
	this->Camera();
#endif
	// 親座標の移動
	this->m_Pos.MoveY(2);
	this->m_OldPos = this->m_Pos;

	// 子座標の初期化
	this->m_PosBox1.Init();
	this->m_PosBox2.Init();
	this->m_GunPos.Init();

	// 子座標に親座標を渡す
	this->m_PosBox1 = this->m_Pos;
	this->m_PosBox2 = this->m_Pos;
	this->m_GunPos = this->m_Pos;
	this->m_GunPos.Scale(2);

	// 倍率の初期化
	this->m_Magnification = 1.0f;

	// 子Boxのスケール
	this->m_PosBox1.Info.sca = D3DXVECTOR3(0, 0, 0);
	this->m_PosBox2.Info.sca = D3DXVECTOR3(0, 0, 0);

	// 各判定の初期化
	this->m_Existence = false;
	this->m_Playback = true;
	this->m_Hit = false;
	this->m_Destruction = false;
	this->m_AttakFrame = 0;
	this->m_MoveFlame = 0;
	this->m_Type = type;
	this->m_Attak = false;
	this->m_MoveSpped = m_Spped;
	this->m_backpattern = -1;
}

//==========================================================================
// 解放
void CEnemy::C_AI::Uninit(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	this->Delete(this->m_Camera);
#endif
}

//==========================================================================
// 描画
void CEnemy::C_AI::Draw(void)
{
	// エネミーが存在しているとき
	if (this->m_Existence == true)
	{
		m_MdelData->Draw(&this->m_PosBox2);
		m_Data->Draw(&this->m_PosBox2);
		m_Data->Draw(&this->m_PosBox1);
		m_Shadow->Draw(&this->m_Pos);
	}
}

//==========================================================================
// 更新
void CEnemy::C_AI::Update(void)
{
	this->AI();
	this->Mode();
	this->Move();
	this->MoveCamera();
	this->Destruction();
	this->Playback();
	this->EnemyGun();
}

//==========================================================================
// ビューカメラのゲッター
D3DXMATRIX * CEnemy::C_AI::GetCamera(void)
{
#if defined(_DEBUG) || defined(DEBUG)
	return this->m_Camera->CreateView();
#else
	return nullptr;
#endif
}

//==========================================================================
// 移動
void CEnemy::C_AI::Mode(void)
{
	this->m_OldPos = this->m_Pos;

	switch (this->m_ModeType)
	{
	case CEnemy::C_AI::Mode::List::AmbushMode:
		break;
	case CEnemy::C_AI::Mode::List::TrackingMode:
	{
		// 追従処理は、対象の座標から動かす対象の座標を引いて出てきたベクトルを使用する
		D3DXVECTOR3 VecTor = CPlayer::GetHeadPos()->Info.pos - this->m_Pos.Info.pos;
		D3DXVec3Normalize(&VecTor, &VecTor);

		this->m_GunPos.Vec.Front = VecTor;
		this->m_GunPos.Look.Eye = -VecTor;
		this->m_Pos.Vec.Front.x = VecTor.x;
		this->m_Pos.Vec.Front.z = VecTor.z;
		this->m_Pos.MoveZ(this->m_MoveSpped);
	}
	break;
	case CEnemy::C_AI::Mode::List::AttackMood:
	{
		// 追従処理は、対象の座標から動かす対象の座標を引いて出てきたベクトルを使用する
		D3DXVECTOR3 VecTor = CPlayer::GetHeadPos()->Info.pos - this->m_Pos.Info.pos;
		D3DXVec3Normalize(&VecTor, &VecTor);

		this->m_GunPos.Vec.Front = VecTor;
		this->m_GunPos.Look.Eye = -VecTor;
		this->m_Pos.Vec.Front.x = VecTor.x;
		this->m_Pos.Vec.Front.z = VecTor.z;
	}
	break;
	default:
		break;
	}
}

//==========================================================================
// AI
void CEnemy::C_AI::AI(void)
{
	this->m_MoveFlame++;
	switch (this->m_Type)
	{
	case CEnemy::C_AI::Type::AutoAttack:
		if (this->m_MoveFlame == 50 && this->m_ModeType != Mode::List::TrackingMode)
		{
			this->m_MoveFlame = 0;
			this->m_ModeType = Mode::List::TrackingMode;
			this->m_Attak = false;
		}
		else if (this->m_MoveFlame == 150 && this->m_ModeType != Mode::List::AttackMood)
		{
			this->m_MoveFlame = 0;
			this->m_Attak = true;
			this->m_ModeType = Mode::List::AttackMood;
		}
		break;
	case CEnemy::C_AI::Type::Ambush:
		if (CHitDetermination::Ball(&this->m_Pos, CPlayer::GetHeadPos(), 25))
		{
			this->m_ModeType = Mode::List::TrackingMode;
			this->m_Attak = true;
			this->m_MoveFlame = 0;
		}
		else
		{
			this->m_ModeType = Mode::List::AmbushMode;
			this->m_Attak = false;
			this->m_MoveFlame = 0;
		}
		break;
	case CEnemy::C_AI::Type::not:
		this->m_ModeType = Mode::List::AmbushMode;
		this->m_Attak = false;
		this->m_MoveFlame = 0;
		break;
	default:
		this->m_MoveFlame = 0;
		break;
	}
}

//==========================================================================
// 判断
void CEnemy::C_AI::Judgment(void)
{
}
