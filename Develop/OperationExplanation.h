//==========================================================================
// 操作説明[OperationExplanation.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _OperationExplanation_H_
#define _OperationExplanation_H_

//==========================================================================
//
// class  : COperationExplanation
// Content: 操作説明
//
//==========================================================================
class COperationExplanation : private CTemplate
{
private:
	static constexpr int m_NumBrockX = 12;
	static constexpr int m_NumBrockY = 12;
public:
	COperationExplanation();
	~COperationExplanation();
	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static bool Update(void);
	// 描画
	static void Draw(void);
	static bool*GetKey(void) { return m_Key; }
private:
	static C2DObject * m_Pos;
	static C2DPolygon * m_Poly;
	static bool * m_Key;
	static int * m_NumFont;
	static int * m_Input;
	static CSound * m_Sound; // サウンド
};
#endif // !_OperationExplanation_H_
