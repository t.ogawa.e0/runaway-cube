//==========================================================================
// 当たり判定[Hit.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _HIT_DETERMINATION_H_
#define _HIT_DETERMINATION_H_

//==========================================================================
//
// class  : CHit
// Content: 当たり判定
//
//==========================================================================
class CHit
{
public:
	CHit();
	~CHit();
	// ゲームの当たり判定
	static void GameHit(void);
private:
	// プレイヤーの当たり判定
	static void PlayerWallHit(void);
	// アイテムの当たり判定処理
	static void ItemHit(void);
	// 弾と壁の当たり判定処理
	static void GunHit(CGun * pGun);
	// エネミーの当たり判定
	static void EnemyHitPlayerGun(CGun * pGun);
	// エネミーの当たり判定
	static void PlayerHitEnemyGun(CGun * pGun);
	// エネミー同士の当たり判定
	static void EnemyHitEnemy(void);
};

#endif // !_HIT_DETERMINATION_H_
