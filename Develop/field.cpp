//==========================================================================
// フィールド[field.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "field.h"

//==========================================================================
// 実体
//==========================================================================
CMeshfield *CField::m_FieldData; // フィールド構成データ
C3DObject *CField::m_FieldPos; // フィールド座標
CMeshfield *CField::m_WallData; // 壁データ
C3DObject *CField::m_WallPos; // 壁座標
int CField::m_NumWall;

CField::CField()
{
}

CField::~CField()
{
}

//==========================================================================
// 初期化
bool CField::Init(void)
{
	int nCount = 0;
	CMeshfield::LINK aFieldLink[] =
	{
		{ "resource/texture/Asphalt.jpg" ,m_WallX,m_WallZ },
	};

	CMeshfield::LINK aWallLink[] =
	{
		{ "resource/texture/Brick.png",m_WallZ,m_WallY },
		{ "resource/texture/Brick.png",m_WallX,m_WallY },
		{ "resource/texture/Brick.png",m_WallX,m_WallY },
		{ "resource/texture/Brick.png",m_WallZ,m_WallY },
	};

	// 壁の数を格納
	m_NumWall = Sizeof(aWallLink);

	// 地面データ
	New(m_FieldData);
	New(m_FieldPos, Sizeof(aFieldLink));

	// 壁のデータ
	New(m_WallData);
	New(m_WallPos, Sizeof(aWallLink));

	// 各座標初期化
	m_FieldPos->Init();
	m_FieldPos->Info.pos.z += m_MoveZPos;
	for (int i = 0; i < m_NumWall; i++)
	{
		m_WallPos[i].Init();
	}

	// 壁のデータモデルデータを確保
	if (m_WallData->Init(aWallLink, Sizeof(aWallLink))) { return true; }

	// 奥
	m_WallPos[nCount].Info.rot.x = m_WallPos[nCount].GetAngle()._ANGLE_270;
	m_WallPos[nCount].Info.pos.z = m_WallData->GetMeshData(nCount).NumMeshX*0.5f;
	m_WallPos[nCount].Info.pos.y = m_WallY / 2;
	m_WallPos[nCount].Info.pos.z += m_MoveZPos;

	// 右
	nCount++;
	m_WallPos[nCount].Info.rot.x = m_WallPos[nCount].GetAngle()._ANGLE_090;
	m_WallPos[nCount].Info.rot.y = m_WallPos[nCount].GetAngle()._ANGLE_270;
	m_WallPos[nCount].Info.pos.z = 0.0f;
	m_WallPos[nCount].Info.pos.x = m_WallData->GetMeshData(nCount).NumMeshX*0.5f;
	m_WallPos[nCount].Info.pos.y = m_WallY / 2;
	m_WallPos[nCount].Info.pos.z += m_MoveZPos;

	// 左
	nCount++;
	m_WallPos[nCount].Info.rot.x = m_WallPos[nCount].GetAngle()._ANGLE_270;
	m_WallPos[nCount].Info.rot.y = m_WallPos[nCount].GetAngle()._ANGLE_270;
	m_WallPos[nCount].Info.pos.z = 0.0f;
	m_WallPos[nCount].Info.pos.x = -m_WallData->GetMeshData(nCount).NumMeshX*0.5f;
	m_WallPos[nCount].Info.pos.y = m_WallY / 2;
	m_WallPos[nCount].Info.pos.z += m_MoveZPos;

	// 手前
	nCount++;
	m_WallPos[nCount].Info.rot.x = m_WallPos[nCount].GetAngle()._ANGLE_090;
	m_WallPos[nCount].Info.pos.z = -m_WallData->GetMeshData(nCount).NumMeshX*0.5f;
	m_WallPos[nCount].Info.pos.y = m_WallY / 2;
	m_WallPos[nCount].Info.pos.z += m_MoveZPos;

	return m_FieldData->Init(aFieldLink, Sizeof(aFieldLink));
}

//==========================================================================
// 解放
void CField::Uninit(void)
{
	if (m_FieldData != nullptr)
	{
		m_FieldData->Uninit();
		Delete(m_FieldData);
	}

	if (m_WallData != nullptr)
	{
		m_WallData->Uninit();
		Delete(m_WallData);
	}
	
	Delete(m_FieldPos);
	Delete(m_WallPos);
}

//==========================================================================
// 更新
void CField::Update(void)
{
}

//==========================================================================
// 描画
void CField::Draw(void)
{
	m_FieldData->Draw(m_FieldPos);
	for (int i = 0; i < m_NumWall; i++)
	{
		m_WallData->Draw(&m_WallPos[i]);
	}
}
