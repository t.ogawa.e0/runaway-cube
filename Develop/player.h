//==========================================================================
// プレイヤー[player.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _PLAYER_H_
#define _PLAYER_H_

//==========================================================================
//
// class  : CPlayer
// Content: プレイヤー
//
//==========================================================================
class CPlayer : private CTemplate
{
private:
	//==========================================================================
	//
	// class  : CPlayerData
	// Content: プレイヤーデーター
	//
	//==========================================================================
	class CPlayerData
	{
	public:
		C3DObject m_Pos; // 座標
		C3DObject m_FPSPos; // 座標
		C3DObject m_OldPos; // 座標
		C3DObject m_Body; // ボディー座標
		C3DObject m_Head; // 頭の座標
		C3DObject m_MosinPos; // モシンナガンの座標
		C3DObject m_MosinRot; // モシンナガン回転情報
		CCamera m_TPSCamera; // TPS視点カメラ
		CCamera m_FPSCamera; // FPS視点カメラ
		CCamera *m_CurrentCamera; // 現在のカメラ
		CSound m_Sound; // 音
		bool m_ViewCameraKey; // ビューカメラの切り替え
	};
public:
	CPlayer();
	~CPlayer();
	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static void Update(void);
	// 描画
	static void Draw(void);
	// 座標ゲッター
	static C3DObject *GetPos(void) { return &m_Player->m_Pos; }
	// 古い座標ゲッター
	static C3DObject *GetOldPos(void) { return &m_Player->m_OldPos; }
	// 体のゲッター
	static C3DObject *GetBodyPos(void) { return &m_Player->m_Body; }
	// 頭のゲッター
	static C3DObject *GetHeadPos(void) { return &m_Player->m_Head; }
	// 速度ゲッター
	static float GetSpeed(void) { return m_MoveSpeed; }
	// モシンのデータのゲッター
	static CGun* GetMosinData(void) { return m_MosinData; }
	// ビューカメラのゲッター
	static D3DXMATRIX *GetCamera(void) { return m_Player->m_CurrentCamera->CreateView(); }
	// TPS視点カメラ
	static CCamera*TPSCamera(void) { return &m_Player->m_TPSCamera; }
	// FPS視点カメラ
	static CCamera*FPSCamera(void) { return &m_Player->m_FPSCamera; }
	// プレイヤーのカメラモード
	static bool * ViewCameraMood(void) { return &m_Player->m_ViewCameraKey; }
private:
	// プレイヤーの動く処理
	static void Move(void);
	// プレイヤーのカメラ
	static void SetCamera(void);
	// カメラの動く処理
	static void MoveCamera(void);
	// ビューカメラ
	static void ViewCamera(void);
	// 視点カメラ
	static void LookCamera(void);
	// プレイヤーのカメラ
	static void PlayerCamera(void);
	// 攻撃
	static void Attack(void);
	// カメラ切り替え
	static void ChangeCanera(void);

	static void MosinUpdate(void);
private: // 
	static CPlayerData *m_Player; // プレイヤーのデータ
	static CXmodel *m_ModelData; // モデルデータ
	static CShadow *m_Shadow; // 影
	static CGun *m_MosinData; // 銃のデータ
	static int * m_FrameSkip;
private:
	static constexpr float m_MoveSpeed = 0.1f; // 移動速度
	static constexpr float m_RollingPower = 0.1f; // 左右移動力
	static constexpr float m_ViewRollingPower = 0.005f; // カメラ回転パワー
	static constexpr float m_MosinBarrettSpped = 1.5f; // 銃の弾速
};

#endif // !_PLAYER_H_
