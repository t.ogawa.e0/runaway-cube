//==========================================================================
// 操作説明[OperationExplanation.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "OperationExplanation.h"

//==========================================================================
// 実体
//==========================================================================
C2DObject * COperationExplanation::m_Pos;
C2DPolygon * COperationExplanation::m_Poly;
int * COperationExplanation::m_NumFont;
int * COperationExplanation::m_Input;
bool * COperationExplanation::m_Key;
CSound * COperationExplanation::m_Sound; // サウンド

COperationExplanation::COperationExplanation()
{
}

COperationExplanation::~COperationExplanation()
{
}

//==========================================================================
// 初期化
bool COperationExplanation::Init(void)
{
	const char * pTexLink[] =
	{
		"resource/texture/操作説明3.png",
		"resource/texture/操作説明2.png",
		"resource/texture/操作説明1.png",
	};

	CSound::SoundLabel pSundList[] =
	{
		{ "resource/sound/RPG_SE[232].wav",0,1.0f }
	};

	New(m_Pos, (int)Sizeof(pTexLink));
	New(m_Poly);
	New(m_NumFont);
	New(m_Input);
	New(m_Key);
	New(m_Sound);

	(*m_Input) = 0;
	(*m_Key) = false;

	(*m_NumFont) = (int)Sizeof(pTexLink);
	for (int i = 0; i < (*m_NumFont); i++)
	{
		m_Pos[i].Init(i);
		m_Pos[i].SetCentralCoordinatesMood(true);
		m_Pos[i].SetX((float)(CDirectXDevice::GetWindowsSize().m_Width / 2));
		m_Pos[i].SetY((float)(CDirectXDevice::GetWindowsSize().m_Height / 2));
	}

	if (m_Sound->Init(pSundList, Sizeof(pSundList)))
	{
		return true;
	}

	return m_Poly->Init(pTexLink, Sizeof(pTexLink));
}

//==========================================================================
// 解放
void COperationExplanation::Uninit(void)
{
	m_Poly->Uninit();
	m_Sound->Uninit();

	Delete(m_Pos);
	Delete(m_Poly);
	Delete(m_NumFont);
	Delete(m_Input);
	Delete(m_Key);
	Delete(m_Sound);
}

//==========================================================================
// 更新
bool COperationExplanation::Update(void)
{
	if ((CKeyboard::Trigger(CKeyboard::KeyList::KEY_RETURN) && (*m_Input) != 3))
	{
		if ((*m_Input) != 2)
		{
			m_Pos[2 - (*m_Input)].SetColor(0, 0, 0, 0);
		}
		m_Sound->Play(0);
		(*m_Input)++;
	}

	if ((CMouse::Trigger(CMouse::ButtonKey::Left) || CMouse::Trigger(CMouse::ButtonKey::Right)) && (*m_Input) != 3)
	{
		if ((*m_Input) != 2)
		{
			m_Pos[2 - (*m_Input)].SetColor(0, 0, 0, 0);
		}
		m_Sound->Play(0);
		(*m_Input)++;
	}

	if ((CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton1)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton2)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton3)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::RightButton4)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::L1Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::R1Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::L2Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::R2Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::SHAREButton)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::SOPTIONSButton)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::L3Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::R3Button)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::PSButton)
		|| CController::CPS4::CButton::Trigger(CController::CPS4::CButton::Ckey::TouchPad)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUp)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUpRight)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonRight)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUnderRight)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUnder)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUnderLeft)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonLeft)
		|| CController::CPS4::CDirection::Trigger(CController::CPS4::CDirection::Ckey::LeftButtonUpLeft)
		) && (*m_Input) != 3)
	{
		if ((*m_Input) != 2)
		{
			m_Pos[2 - (*m_Input)].SetColor(0, 0, 0, 0);
		}
		m_Sound->Play(0);
		(*m_Input)++;
	}


	if ((*m_Input) == 3)
	{
		(*m_Key) = true;
	}

	return (*m_Key);
}

//==========================================================================
// 描画
void COperationExplanation::Draw(void)
{
	for (int i = 0; i < (*m_NumFont); i++)
	{
		m_Poly->Draw(&m_Pos[i]);
	}
}

