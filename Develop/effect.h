//==========================================================================
// エフェクト[effect.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _EXPLOSION_H_
#define _EXPLOSION_H_

//==========================================================================
//
// class  : CEffect
// Content: エフェクト
//
//==========================================================================
class CEffect : private CTemplate
{
private:
	struct Type
	{
		enum List
		{
			α, // α値タイプ
			Anim, // アニメーションタイプ
		};
	};
	// エフのェクトデータ管理
	class CData
	{
	public:
		int m_ID; // 管理ID
		int *m_AnimCounter; // アニメーションカウンタ
		C3DObject m_Pos; // 座標
		CData *m_pNext; // 次のアドレス
		CData *m_pBack; // 前のアドレス
	};
private:
	typedef Type::List EffectType; // タイプリスト
public:
	CEffect();
	~CEffect();
	// 初期化
	 bool Init(const char * InputExplosion, int UpdateFrame, int Pattern, int Direction);

	 bool Init(const char * InputExplosion);
	// 解放
	 void Uninit(void);
	// 更新
	 void Update(void);
	// 描画
	 void Draw(D3DXMATRIX *MtxView);
	 // エフェクトセット
	 void SetEfect(const C3DObject * Input);
private:
	// 特定のデータ消去
	void PinpointDelete(const int * ID);
private:
	CBillboard *m_EffectData; // エフェクト格納
	CData *m_pStartAddress; // 先頭アドレス
	Type::List m_EfectType; // エフェクトタイプ
	int m_NumData; // データ数
	int m_IDCount; // IDカウンタ
	bool m_Add; // 加算
};

#endif // !_EXPLOSION_H_
