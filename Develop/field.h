//==========================================================================
// フィールド[field.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _FIELD_H_
#define _FIELD_H_

//==========================================================================
//
// class  : CField
// Content: フィールド
//
//==========================================================================
class CField : private CTemplate
{
public:
	CField();
	~CField();

	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static void Update(void);
	// 描画
	static void Draw(void);
	// 壁の座標
	static C3DObject *WallPos(int Count) { return &m_WallPos[Count]; }

	static int PosY(void) { return m_WallY; }
	// 壁の数
	static int *NumWall(void) { return &m_NumWall; }
private:
	static CMeshfield *m_FieldData; // フィールド構成データ
	static C3DObject *m_FieldPos; // フィールド座標
	static CMeshfield *m_WallData; // 壁データ
	static C3DObject *m_WallPos; // 壁座標
	static int m_NumWall; // 壁の数
private:
	static constexpr float m_MoveZPos = 0.0f;
	static constexpr int m_WallY = 90;
	static constexpr int m_WallX = 90;
	static constexpr int m_WallZ = 90;
};

#endif // !_FIELD_H_
