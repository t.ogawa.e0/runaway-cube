//==========================================================================
// ThankYouForPlaying[ThankYouForPlaying.h]
// author: tatuya ogawa
//==========================================================================
#ifndef _ThankYouForPlaying_H_
#define _ThankYouForPlaying_H_

//==========================================================================
//
// class  : CThankYouForPlaying
// Content: ThankYouForPlaying
//
//==========================================================================
class CThankYouForPlaying : private CTemplate
{
public:
	CThankYouForPlaying();
	~CThankYouForPlaying();
	// 初期化
	static bool Init(void);
	// 解放
	static void Uninit(void);
	// 更新
	static void Update(bool bkey);
	// 描画
	static void Draw(void);
	// 終了キーのゲッター
	static bool *GetKey(void) { return m_End; }
private:
	static C2DObject * m_Pos; // 座標
	static C2DPolygon * m_Poly; // ポリゴン
	static int m_Num; // 枚数カウント
	static int m_αcount; // α値カウンタ
	static bool * m_End; // 終了フラグ
	static int m_Count; // カウンタ
};

#endif // !_ThankYouForPlaying_H_
