//==========================================================================
// バレット[barrett.cpp]
// author: tatuya ogawa
//==========================================================================
#include "DirectXLibrary.h"
#include "effect.h"
#include "barrett.h"

CBarrett::CBarrett()
{
}

CBarrett::~CBarrett()
{
}

//==========================================================================
// 初期化
bool CBarrett::Init(const char ** pFile, int size)
{
	// アドレスの初期化
	this->m_pBarrett = nullptr;
	this->m_EffectData = nullptr;
	this->m_pStartAddress = nullptr;

	// メモリ確保
	this->New(this->m_pBarrett);

	// 管理用パラメータの初期化
	this->m_IDCount = 0;
	this->m_NumData = 0;

	//格納
	return this->m_pBarrett->Init(pFile, size);
}

//==========================================================================
// 使用するエフェクトの初期化
bool CBarrett::SetEffect(const char * InputExplosion, int UpdateFrame, int Pattern, int Direction)
{
	// データ自体がないとき
	if (this->m_pBarrett == nullptr)
	{
		ErrorMessage(nullptr, "弾がセットされていないため、エフェクトを作れませんでした");
		return true;
	}

	// メモリ確保
	this->New(this->m_EffectData);

	// エフェクトの確保
	return this->m_EffectData->Init(InputExplosion, UpdateFrame, Pattern, Direction);
}

//==========================================================================
// 解放
void CBarrett::Uninit(void)
{
	CData *pData = this->m_pStartAddress; // 初期アドレスを記憶
	CData *pNextDelete = nullptr; // 次のアドレス

	// モデルデータの解放
	if (this->m_pBarrett != nullptr)
	{
		this->m_pBarrett->Uninit();
		this->Delete(this->m_pBarrett);
	}
	
	// エフェクトの解放
	if (this->m_EffectData != nullptr)
	{
		this->m_EffectData->Uninit();
		this->Delete(this->m_EffectData);
	}

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// 次のアドレスを記憶
		pNextDelete = pData->m_pNext;

		// メモリ解放
		this->Delete(pData);
		this->m_NumData--;

		pData = pNextDelete;

		// データ数が0の時
		if (this->m_NumData == 0)
		{
			this->m_pStartAddress = nullptr;
			this->m_IDCount = 0;
		}
	}
}

//==========================================================================
// 更新
void CBarrett::Update(void)
{
	// 先頭アドレスを記憶
	CData *pData = this->m_pStartAddress;

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// 古い座標に格納
		pData->m_OldPos = pData->m_Pos;

		// 弾移動処理
		pData->m_Pos.MoveZ(pData->m_Speed);

		// 弾オブジェクト座標
		pData->m_ObjPos.Info.pos = pData->m_Pos.Info.pos;

		// 弾の回転
		pData->m_ObjPos.Info.rot.z += 0.5f;

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}

	// エフェクトの更新
	if (this->m_EffectData != nullptr)
	{
		this->m_EffectData->Update();
	}
}

//==========================================================================
// 描画
void CBarrett::Draw(D3DXMATRIX *MtxView)
{
	// 先頭アドレスを記憶
	CData *pData = this->m_pStartAddress;

	// アドレスがnullではない限り繰り返す
	for (;;)
	{
		// アドレスがnullだった場合終了
		if (pData == nullptr)
		{
			break;
		}

		// 描画
		this->m_pBarrett->Draw(&pData->m_ObjPos);

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}

	// エフェクトの描画
	if (this->m_EffectData != nullptr)
	{
		this->m_EffectData->Draw(MtxView);
	}
}

//==========================================================================
// 弾生成
void CBarrett::Create(const C3DObject *Input, const float *Spped)
{
	CData *pMain = nullptr; // 生成時のアドレス
	CData *pData = nullptr; // 次のアドレス
	this->m_NumData++;
	this->m_IDCount++;

	// メモリを確保
	this->New(pMain);
	pMain->m_pNext = nullptr;
	pMain->m_pBack = nullptr;
	pMain->m_ID = this->m_IDCount;
	pMain->m_Pos = *Input;
	pMain->m_Pos.Info.sca = D3DXVECTOR3(0.05f, 0.05f, 0.05f);
	pMain->m_OldPos = pMain->m_Pos;
	pMain->m_ObjPos = pMain->m_Pos;
	pMain->m_Speed = (*Spped);

	// 先頭アドレスがなかった場合
	if (this->m_pStartAddress == nullptr)
	{
		this->m_pStartAddress = pMain;
	}
	else if (this->m_pStartAddress != nullptr)
	{
		// 先頭アドレスを記憶
		pData = this->m_pStartAddress;

		// アドレスがnullではない限り検索を繰り返す
		for (;;)
		{
			// 次のアドレスがnullの場合
			if (pData->m_pNext == nullptr)
			{
				break;
			}

			// 検索をするアドレスを記憶
			pData = pData->m_pNext;
		}

		// 次のアドレスを記憶
		pData->m_pNext = pMain;

		// ひとつ前のアドレスに記憶
		pMain->m_pBack = pData;
	}
}

//==========================================================================
// 特定のデータ消去
void CBarrett::PinpointDelete(const int * ID)
{
	CData *pData = this->m_pStartAddress; // 先頭アドレスを記憶
	CData *pBack = nullptr; // 前のデータ
	CData *pNext = nullptr; // 次のデータ

	// 解放対象が見つかるまで検索を続ける
	for (;;)
	{
		// アドレスが無ければ終了
		if (pData == nullptr)
		{
			break;
		}

		// 検索IDとヒット
		if (pData->m_ID == (*ID))
		{
			if (this->m_EffectData != nullptr)
			{
				this->m_EffectData->SetEfect(&pData->m_Pos);
			}

			// 先頭アドレスと検索IDが同じ場合先頭アドレスを書き換える
			if (this->m_pStartAddress->m_ID == pData->m_ID)
			{
				this->m_pStartAddress = pData->m_pNext;
			}

			pBack = pData->m_pBack; // ひとつ前のアドレスを記憶
			pNext = pData->m_pNext; // 次のアドレスを記憶

			// アドレスがある場合次のアドレスをつなぎなおす
			if (pBack != nullptr)
			{
				pBack->m_pNext = pNext;
			}

			// アドレスがある場合ひとつ前のアドレスをつなぎなおす
			if (pNext != nullptr)
			{
				pNext->m_pBack = pBack;
			}

			// メモリ解放
			this->Delete(pData);
			this->m_NumData--;

			// データ数が0の時
			if (this->m_NumData == 0)
			{
				this->m_pStartAddress = nullptr;
				this->m_IDCount = 0;
			}

			break; // 終了
		}

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}
}

//==========================================================================
// 弾データのゲッター データがない場合null
CBarrett::CData * CBarrett::GetData(const int * Count)
{
	CData *pData = this->m_pStartAddress; // 先頭アドレスを記憶

	// カウンタの回数繰り返す
	for (int i = 0; i < (*Count); i++)
	{
		// アドレスが無ければ終了
		if (pData == nullptr)
		{
			break;
		}

		// 次のアドレスを記憶
		pData = pData->m_pNext;
	}

	return pData;
}
